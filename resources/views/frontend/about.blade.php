@extends('frontend.layouts.app')
@section('page-name', 'About')
@section('about', 'active')
@section('content')
    <section class="about_page pt-5 pb-5">
        <div class="container">
            <div class="row">
                @if (!empty($about))
                    <div class="col-md-12">
                        <div class="css_title text-center pb-5">
                            <img src="{{ asset('frontend/images/small.png') }}" alt="">
                            <h1 class="title">{{ $about->title }}</h1>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-12">

                        <div class="desc_class1">
                            {!! $about->description !!}
                        </div>


                    </div>

                    <div class="col-lg-3 col-md-12">
                        <div class="about_image">
                            @if ($about->image)
                                <img src="{{ $about->getImg($about->image) }}" alt="" class="img-fluid">
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
