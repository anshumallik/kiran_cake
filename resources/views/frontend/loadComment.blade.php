@foreach($product_ratings as $product_rating)
    <div class="review-box clearfix">
        <div class="rev-content">
            <div class="rev-header clearfix">
                <h4>{{$product_rating->name}}</h4>
                <div class="rating-css">
                    @for($i=0; $i<5; ++$i)
                        @php
                            echo '<span class="fa fa-star',
                            (round($product_rating->rating,1)==$i+.1?'-o':''),
                            (round($product_rating->rating,1)==$i+.2?'-o':''),
                            (round($product_rating->rating,1)==$i+.3?'-o':''),
                            (round($product_rating->rating,1)==$i+.4?'-o':''),
                            (round($product_rating->rating,1)==$i+.5?'-half':''),
                            (round($product_rating->rating,1)==$i+.6?'-half':''),
                            (round($product_rating->rating,1)==$i+.7?'-half':''),
                            (round($product_rating->rating,1)==$i+.8?'-half':''),
                            (round($product_rating->rating,1)==$i+.9?'-half':''),
                            (round($product_rating->rating,1)<=$i?'-o':''),
                            '" aria-hidden="true"></span>';
                            echo "\n";
                        @endphp
                    @endfor
                    @php
                        $totalRating = round($product_rating->rating, 1);
                            echo "($totalRating)";
                            echo "\n";
                    @endphp
                </div>
            </div>
            <div class="rev-text">{{$product_rating->review}}</div>
        </div>
    </div>
@endforeach
<button class="loadMore" onclick="loadMore();">Load More</button>