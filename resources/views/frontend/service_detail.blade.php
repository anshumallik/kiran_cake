@extends('frontend.layouts.app')
@section('page-name', 'Service')
@section('service', 'active')
@section('content')
    <section class="about_page pt-5 pb-5">
        <div class="container">
            <div class="row">
                @if (!empty($service))
                    <div class="col-md-12">
                        <div class="css_title text-center pb-5">
                            <img src="{{ asset('frontend/images/small.png') }}" alt="">
                            <h1 class="title">{{ $service->title }}</h1>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-12">
                        @if ($service->description)
                            <div class="desc_class1">
                                {!! $service->description !!}
                            </div>
                        @else
                            No data available
                        @endif

                    </div>

                    <div class="col-lg-3 col-md-12">
                        <div class="service_image">
                            @if ($service->image)
                                <img src="{{ $service->getImg($service->image) }}" alt="" class="img-fluid">
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
