@extends('frontend.layouts.app')
@section('page-name', 'Home')
@section('home', 'active')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">

                        <div class="carousel-inner">
                            @if (count($sliders) > 0)
                                @foreach ($sliders as $slider)
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        @if ($slider->link)
                                            <a href="{{ $slider->link }}" target="_blank">
                                                <img src="{{ $slider->getImg($slider->image) }}" class="d-block w-100"
                                                    alt="...">
                                            </a>
                                        @else
                                            <img src="{{ $slider->getImg($slider->image) }}" class="d-block w-100"
                                                alt="...">
                                        @endif

                                    </div>
                                @endforeach
                            @else
                                <div class="carousel-item active">
                                    <img src="{{ asset('frontend/images/banner.png') }}" class="d-block w-100" alt="...">

                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('frontend/images/banner12.jpg') }}" class="d-block w-100"
                                        alt="...">

                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('frontend/images/back.jpg') }}" class="d-block w-100" alt="...">

                                </div>
                            @endif

                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if (count($featured_products) > 0)
        <section class="pt-5 featured_product">
            <div class="container">
                <div class="css_title text-center pb-5">
                    <img src="{{ asset('frontend/images/icon.png') }}" alt="">
                    <h1 class="title">Featured Product</h1>
                </div>
                <div class="row card-area">
                    @foreach ($featured_products as $product)
                        <div class="col-md-6 col-lg-2 mycard">
                            <div class="card mb-5">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="{{ route('frontend.product_detail', $product->slug) }}">
                                            <img class="pic-1" src="{{ $product->getImg($product->image) }}">
                                            <img class="pic-2" src="{{ $product->getImg($product->image) }}">
                                        </a>
                                        <ul class="social">
                                            <li><a href="{{ route('frontend.product_detail', $product->slug) }}"
                                                    data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                                            <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                            </li>
                                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#cartModal"
                                                    data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>

                                        @if (count($product->product_ratings) > 0)
                                            <span class="product-new-label"><i class="fa fa-star-o"><a href=""
                                                        class="rating">
                                                        @php
                                                            $totalRating = round($product->avgRating(), 1);
                                                            echo "$totalRating";
                                                            echo "\n";
                                                        @endphp</a></i>
                                            </span>
                                        @endif
                                        @if ($product->discount)
                                            <span class="product-discount-label1">- {{ $product->discount }} %</span>
                                        @endif
                                    </div>
                                    <div class="product-content">

                                        <h3 class="title"><a
                                                href="{{ route('frontend.product_detail', $product->slug) }}">{!! substr($product->name, 0, 50) !!}</a>
                                        </h3>
                                        <div class="price">

                                            @if ($product->discount)
                                                <?php
                                                $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                ?>
                                                Rs.{{ $discount_amount }}

                                                <span>Rs.{{ $product->price }}</span>
                                            @else
                                                Rs.{{ $product->price }}
                                            @endif
                                        </div>
                                        @if (count($product->product_ratings) > 0)
                                            <div class="rating rating_star">
                                                <div class="stars">
                                                    @for ($i = 0; $i < 5; ++$i)
                                                        @php
                                                            echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>';
                                                            echo "\n";
                                                        @endphp
                                                    @endfor
                                                    <span class="review-no">
                                                        @php
                                                            $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                ->where('status', 1)
                                                                ->count();
                                                            
                                                            if ($totalProductReview > 0) {
                                                                $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 5)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                // dd($totalFiveRating);
                                                                $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 4)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 3)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 2)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 1)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                            } else {
                                                                $fivePercent = 0;
                                                                $fourPercent = 0;
                                                                $threePercent = 0;
                                                                $twoPercent = 0;
                                                                $onePercent = 0;
                                                            }
                                                        @endphp
                                                        @if ($totalProductReview <= 9)
                                                            ({{ $totalProductReview }})
                                                        @else
                                                            ({{ $totalProductReview }})
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModal1Title"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-check mr-2"></i>
                                Product successfully added to your shopping cart</h5>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>
                            </button>
                        </div>
                        <div class="modal-body">

                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if (count($trending_products) > 0)
        <section class="pt-5 featured_product">
            <div class="container">
                <div class="css_title text-center pb-5">
                    <img src="{{ asset('frontend/images/small.png') }}" alt="">
                    <h1 class="title">Trending Products</h1>
                </div>
                <div class="row card-area">
                    @foreach ($trending_products as $product)
                        <div class="col-md-6 col-lg-2 mycard">
                            <div class="card mb-5">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="{{ route('frontend.product_detail', $product->slug) }}">
                                            <img class="pic-1" src="{{ $product->getImg($product->image) }}">
                                            <img class="pic-2" src="{{ $product->getImg($product->image) }}">
                                        </a>
                                        @auth
                                        <ul class="social">
                                            <li><a href="{{ route('frontend.product_detail', $product->slug) }}"
                                                    data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                                            <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
                                            </li>
                                            <li><a href="#" data-bs-toggle="modal" data-id="{{ $product->id }}"
                                                    data-price="{{ $product->price }}" data-bs-target="#cartModal"
                                                    data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul>
                                        @endauth
                                        @if (count($product->product_ratings) > 0)
                                            <span class="product-new-label"><i class="fa fa-star-o"><a href=""
                                                        class="rating">
                                                        @php
                                                            $totalRating = round($product->avgRating(), 1);
                                                            echo "$totalRating";
                                                            echo "\n";
                                                        @endphp</a></i>
                                            </span>
                                        @endif
                                        @if ($product->discount)
                                            <span class="product-discount-label1">- {{ $product->discount }} %</span>
                                        @endif
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a
                                                href="{{ route('frontend.product_detail', $product->slug) }}">{!! substr($product->name, 0, 50) !!}</a>
                                        </h3>
                                        <div class="price">

                                            @if ($product->discount)
                                                <?php
                                                $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                ?>
                                                Rs.{{ $discount_amount }}

                                                <span>Rs.{{ $product->price }}</span>
                                            @else
                                                Rs.{{ $product->price }}
                                            @endif
                                        </div>
                                        @if (count($product->product_ratings) > 0)
                                            <div class="rating rating_star">
                                                <div class="stars">
                                                    @for ($i = 0; $i < 5; ++$i)
                                                        @php
                                                            echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>';
                                                            echo "\n";
                                                        @endphp
                                                    @endfor
                                                    <span class="review-no">
                                                        @php
                                                            $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                ->where('status', 1)
                                                                ->count();
                                                            
                                                            if ($totalProductReview > 0) {
                                                                $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 5)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                // dd($totalFiveRating);
                                                                $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 4)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 3)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 2)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('rating', 1)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                            } else {
                                                                $fivePercent = 0;
                                                                $fourPercent = 0;
                                                                $threePercent = 0;
                                                                $twoPercent = 0;
                                                                $onePercent = 0;
                                                            }
                                                        @endphp
                                                        @if ($totalProductReview <= 9)
                                                            ({{ $totalProductReview }})
                                                        @else
                                                            ({{ $totalProductReview }})
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>


        </section>
    @endif
    @if (count($best_selling_products) > 0)
        <section class="pb-5">
            <div class="container">
                <div class="css_title text-center pb-5">
                    <img src="{{ asset('frontend/images/icon.png') }}" alt="">

                    <h1 class="title">Best selling Products</h1>

                </div>
                <div class="row card-area new">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">
                            @foreach ($best_selling_products as $product)
                                <div class="card">
                                    <div class="product-grid4">
                                        <div class="product-image4">
                                            <a href="{{ route('frontend.product_detail', $product->slug) }}">
                                                <img class="pic-1"
                                                    src="{{ $product->getImg($product->image) }}">
                                                <img class="pic-2"
                                                    src="{{ $product->getImg($product->image) }}">
                                            </a>
                                            <ul class="social">
                                                <li><a href="{{ route('frontend.product_detail', $product->slug) }}"
                                                        data-tip="Quick View"><i class="fa fa-search"></i></a>
                                                </li>
                                                <li><a href="#" data-tip="Add to Wishlist"><i
                                                            class="fa fa-heart-o"></i></a>
                                                </li>
                                                <li><a href="#" data-bs-toggle="modal" data-bs-target="#cartModal1"
                                                        data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>
                                            @if (count($product->product_ratings) > 0)
                                                <span class="product-new-label"><i class="fa fa-star-o"><a href=""
                                                            class="rating">
                                                            @php
                                                                $totalRating = round($product->avgRating(), 1);
                                                                echo "$totalRating";
                                                                echo "\n";
                                                            @endphp</a></i>
                                                </span>
                                            @endif
                                            @if ($product->discount)
                                                <span class="product-discount-label1">- {{ $product->discount }}
                                                    %</span>
                                            @endif
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a
                                                    href="{{ route('frontend.product_detail', $product->slug) }}">{!! substr($product->name, 0, 50) !!}</a>
                                            </h3>
                                            <div class="price">

                                                @if ($product->discount)
                                                    <?php
                                                    $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                    ?>
                                                    Rs.{{ $discount_amount }}

                                                    <span>Rs.{{ $product->price }}</span>
                                                @else
                                                    Rs.{{ $product->price }}
                                                @endif
                                            </div>
                                            @if (count($product->product_ratings) > 0)
                                                <div class="rating rating_star">
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        <span class="review-no">
                                                            @php
                                                                $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                
                                                                if ($totalProductReview > 0) {
                                                                    $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 5)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    // dd($totalFiveRating);
                                                                    $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                    $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 4)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                    $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 3)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                    $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 2)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                    $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 1)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                                } else {
                                                                    $fivePercent = 0;
                                                                    $fourPercent = 0;
                                                                    $threePercent = 0;
                                                                    $twoPercent = 0;
                                                                    $onePercent = 0;
                                                                }
                                                            @endphp
                                                            @if ($totalProductReview <= 9)
                                                                ({{ $totalProductReview }})
                                                            @else
                                                                ({{ $totalProductReview }})
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        </div>

                    </div>
                </div>
            </div>
        </section>
    @endif

    @if (count($latest_products) > 0)
        <section class="pb-5 pt-5">
            <div class="container">
                <div class="css_title text-center pb-5">
                    <img src="{{ asset('frontend/images/icon.png') }}" alt="">
                    {{-- <h5 class="title_small pt-3">Ice Creams</h5> --}}
                    <h1 class="title">Latest Collection</h1>

                </div>
                <div class="row card-area new">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">
                            @foreach ($latest_products as $product)
                                <div class="card">
                                    <div class="product-grid4">
                                        <div class="product-image4">
                                            <a href="{{ route('frontend.product_detail', $product->slug) }}">
                                                <img class="pic-1"
                                                    src="{{ $product->getImg($product->image) }}">
                                                <img class="pic-2"
                                                    src="{{ $product->getImg($product->image) }}">
                                            </a>
                                            <ul class="social">
                                                <li><a href="{{ route('frontend.product_detail', $product->slug) }}"
                                                        data-tip="Quick View"><i class="fa fa-search"></i></a>
                                                </li>
                                                <li><a href="#" data-tip="Add to Wishlist"><i
                                                            class="fa fa-heart-o"></i></a>
                                                </li>
                                                <li><a href="#" data-bs-toggle="modal" data-bs-target="#cartModal1"
                                                        data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>
                                            @if (count($product->product_ratings) > 0)
                                                <span class="product-new-label"><i class="fa fa-star-o"><a href=""
                                                            class="rating">
                                                            @php
                                                                $totalRating = round($product->avgRating(), 1);
                                                                echo "$totalRating";
                                                                echo "\n";
                                                            @endphp</a></i>
                                                </span>
                                            @endif
                                            <span class="product-discount-label1">New</span>
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a
                                                    href="{{ route('frontend.product_detail', $product->slug) }}">{!! substr($product->name, 0, 50) !!}</a>
                                            </h3>
                                            <div class="price">

                                                @if ($product->discount)
                                                    <?php
                                                    $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                    ?>
                                                    Rs.{{ $discount_amount }}

                                                    <span>Rs.{{ $product->price }}</span>
                                                @else
                                                    Rs.{{ $product->price }}
                                                @endif
                                            </div>
                                            @if (count($product->product_ratings) > 0)
                                                <div class="rating rating_star">
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        <span class="review-no">
                                                            @php
                                                                $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                
                                                                if ($totalProductReview > 0) {
                                                                    $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 5)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    // dd($totalFiveRating);
                                                                    $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                    $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 4)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                    $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 3)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                    $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 2)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                    $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $product->id)
                                                                        ->where('rating', 1)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                                } else {
                                                                    $fivePercent = 0;
                                                                    $fourPercent = 0;
                                                                    $threePercent = 0;
                                                                    $twoPercent = 0;
                                                                    $onePercent = 0;
                                                                }
                                                            @endphp
                                                            @if ($totalProductReview <= 9)
                                                                ({{ $totalProductReview }})
                                                            @else
                                                                ({{ $totalProductReview }})
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="pt-5 pb-5 vertical-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    @foreach ($services as $service)
                        <div class="card margin-bottom rounded-0">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="icon_side my-auto">
                                        <a href="{{ route('frontend.service_detail', $service->slug) }}"><img
                                                src="{{ $service->getImg($service->image) }}" alt="" height="40"></a>
                                    </div>
                                    <div class="ms-3 my-auto detail_ver">
                                        <a href="{{ route('frontend.service_detail', $service->slug) }}">
                                            <h5 class="mb-0">{{ $service->title }}</h5>
                                        </a>
                                        @if ($service->description)
                                            <div class="detail_service mb-0 mt-1">
                                                {!! substr($service->description, 0, 100) !!}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="col-md-6 my-auto">
                    @if ($company_data->company_banner)
                        <div class="style_img text-center">
                            <img src="{{ $company_data->companyBanner($company_data->company_banner) }}" alt="cake"
                                width="" class="img-fluid">
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @if (count($brands) > 0)
        <section class="pb-5 pt-5 partner_logo">
            <div class="container">
                <div class="css_title text-center pb-5">
                    <h1 class="title">Our Brands</h1>
                    <p class="pt-2">A product can be quickly outdated, a successful brand is timeless.</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <section class="customer-logos slider">
                            @foreach ($brands as $brand)
                                <div class="slide">
                                    @if ($brand->url)
                                        <a href="{{ $brand->url }}" target="_blank">
                                            <img class="slider-image" src="{{ $brand->getImg($brand->logo) }}" alt="">
                                        </a>
                                    @else
                                        <img class="slider-image" src="{{ $brand->getImg($brand->logo) }}" alt="">
                                    @endif
                                </div>
                            @endforeach
                        </section>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection
@section('script')
    <script>
        @if (Auth::check())
            $(document).ready(function() {
            $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $("#cartModal").on('show.bs.modal', function(e) {
            var this_item = e.relatedTarget;
            var product_id = $(this_item).data('id');
            var product_price = $(this_item).data('price');
            $.ajax({
            url: "{{ route('frontend.get_product_modal') }}",
            type: 'POST',
            data: {
            'product_id': product_id
            },
            success: function(data) {
            console.log(data);
            $(".modal-body").html(data);
        
            },
            complete: function(){
            $.ajax({
            url: "{{ route('frontend.addItemsToCart') }}",
            type: 'post',
            data: {
            'qty': 1,
            'user_id': "{{ Auth::user()->id }}",
            'product_id': product_id,
            'price': product_price,
            },
            // dataType: 'json',
            success: function(data) {
            if (data.alertmsg) {
            Swal.fire(data.alertmsg);
            }
        
            // if (data.loginRoute) {
            // location.href = data.loginRoute;
            // }
            else if (data.db_error) {
            $(".alert-warning").css('display', 'block');
            $(".db_error").html(data.db_error);
            toastr.warning(data.db_error);
            } else if (data.errors) {
        
            } else if (!data.errors && !data.db_error) {
        
        
            // toastr.success(data.msg);
        
        
            }
        
            }
            });
            }
            });
            });
            });
        @endif
    </script>
@endsection
