@extends('frontend.layouts.app')
@section('page-name', 'Login')
@section('login', 'active')
@section('content')
    <section class="login-page pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-12 mx-auto">
                    <div class="row">
                        <div class="col-md-12 col-lg-8">
                            <h4 class="title_data">Welcome to {{ $company_data->company_name }}, Please Login </h4>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="float-end float-media">
                                <h5 class="font-small mb-0">New member? <a href="{{ route('frontend.register') }}">Register
                                    </a>here</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-4 rounded-0">
                        <div class="card-body">
                            @if (Session::has('error-message'))
                                <span class="text-danger"
                                    style="font-size: 80%;"><strong>{{ Session::get('error-message') }}</strong></span>
                            @endif
                            <form action="{{ route('login') }}" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-login">

                                            @csrf
                                            <div class="form-group mb-3">
                                                <label for="">Email Address<span class="text-danger">*</span></label>
                                                <input type="email" placeholder="Please enter your email"
                                                    class="form-control @error('email') is-invalid @enderror" name="email">
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="">Password <span class="text-danger">*</span></label>
                                                <input type="password" id="password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    placeholder="Password" name="password" autocomplete="current-password">
                                                <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <span class="float-end forget_password mt-3 mb-3"><a
                                                    href="{{ route('frontend.forget_password') }}">Forgot Password
                                                    ?</a></span>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="login mb-2">
                                            <button class="btn btn-login w-100 rounded-0" type="submit">Login</button>
                                        </div>
                                        <span>Or,login with</span>
                                        <div class="flex items-center justify-end mb-3 mt-3">
                                            <a class="btn btn-facebook w-100 rounded-0"
                                                href="{{ route('frontend.redirectToFacebook') }}">
                                                <i class="fa fa-facebook me-3"></i>Facebook
                                            </a>
                                        </div>
                                        {{-- <div class="mb-3 mt-2">
                                            <button class="btn btn-facebook w-100 rounded-0"><i
                                                    class="fa fa-facebook me-3"></i>Facebook</button>
                                        </div> --}}
                                        {{-- <div>
                                            <button class="btn btn-google w-100 rounded-0"><i
                                                    class="fa fa-google-plus me-3"></i>Google</button>
                                        </div> --}}
                                        <div class="flex items-center justify-end mt-3">
                                            <a class="btn btn-google w-100 rounded-0"
                                                href="{{ route('frontend.redirectToGoogle') }}">
                                                <i class="fa fa-google-plus me-3"></i>Google
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
