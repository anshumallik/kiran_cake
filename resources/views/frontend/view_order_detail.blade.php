 @extends('frontend.layouts.app')
 @section('page-name', 'Order Detail')
 @section('order-detail', 'active')
 @section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                 <table class="table">
         <thead>
             <tr>
                 <th scope="col">S.N</th>
                 <th scope="col">Product</th>
                 <th scope="col">Quantity</th>
                 <th scope="col">Price</th>
                 <th scope="col">Subtotal</th>
                
             </tr>
         </thead>
         <tbody>

             <?php $id = 1; ?>
             @foreach ($order_items as $order_item)
                 <tr>
                     <th>{{ $id++ }}</th>
                     <td>{{ $order_item->product->name }}</td>
                     <td>
                         {{ $order_item->quantity }}
                     </td>
                     <td>
                         {{ $order_item->price }}
                     </td>
                     <td>
                         {{ $order_item->subtotal }}
                     </td>
                 </tr>
             @endforeach
         </tbody>
     </table>
            </div>
        </div>
    </div>
 @endsection
