@extends('frontend.layouts.app')
@section('page-name', 'Privacy Privacy')
@section('privacy-policy', 'active')
@section('content')
    <section class="about_page pt-5 pb-5">
        <div class="container">
            <div class="row">
                @if (!empty($privacy_policy))
                    <div class="col-md-12">
                        <div class="css_title text-center pb-5">
                            <img src="{{ asset('frontend/images/small.png') }}" alt="">
                            <h1 class="title">{{ $privacy_policy->title }}</h1>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="desc_class1">
                            {!! $privacy_policy->description !!}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
