@extends('frontend.layouts.app')
@section('page-name', 'Product Detail')
@section('product_detail', 'active')
@section('content')
    <section class="product w-100 pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-12">
                    <div class="row">

                        <div class="col-md-6 col-lg-8">
                            <div class="product_title_side">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{ route('frontend.index') }}">Home</a></li>
                                        <li class="breadcrumb-item"><a
                                                href="{{ route('frontend.product', $product->category->slug) }}">
                                                {{ $product->category->title }}</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-4">
                            <a href="{{ route('frontend.shop') }}" class="btn-explore float-end">Explore All</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product_detail pt-3">
                <div class="card rounded-0 border-0">
                    <div class="row">
                        <div class="preview col-md-12 col-lg-6">
                            <div class="preview-pic tab-content">

                                <div class="tab-pane active" id="pic-{{ $product->slug }}">
                                    <article class="demo-area">
                                        <img class="demo-trigger" src="{{ $product->getImg($product->image) }}"
                                            data-zoom="{{ $product->getImg($product->image) }}">

                                    </article>
                                </div>
                                @foreach ($product->product_images->where('status', 1) as $productImage)
                                    <div class="tab-pane" id="pic-{{ $productImage->id }}">
                                        <article class="demo-area">
                                            <img class="demo-trigger"
                                                src="{{ $productImage->getImg($productImage->image) }}"
                                                data-zoom="{{ $productImage->getImg($productImage->image) }}">

                                        </article>
                                    </div>
                                @endforeach

                            </div>
                            <ul class="preview-thumbnail nav nav-tabs">
                                <li><a data-bs-target="#pic-{{ $product->slug }}" data-bs-toggle="tab"
                                        class="active"><img src="{{ $product->getImg($product->image) }}" /></a>
                                </li>
                                @foreach ($product->product_images->where('status', 1) as $productImage)
                                    <li><a data-bs-target="#pic-{{ $productImage->id }}" data-bs-toggle="tab"><img
                                                src="{{ $productImage->getImg($productImage->image) }}" /></a></li>
                                @endforeach

                            </ul>

                        </div>
                        <aside class="col-md-12 col-lg-6 detail">
                            <form action="{{ route('frontend.addItemsToCart') }}" method="POST" id="add_to_cart_form">
                                @csrf
                                <input type="hidden" id="price" value="{{ $product->price }}" name="price">
                                <input type="hidden" value="{{ $product->id }}" name="product_id">
                                <input type="hidden" id="user_id" value="{{ auth()->user() ? auth()->user()->id : '' }}"
                                    name="user_id">
                                <input type="hidden" value="1" id="qty" name="qty">
                                <article class="card-body">
                                    <div class="row">
                                        @if ($product->qty > 0)
                                            <div class="col-sm-3">
                                                <dl class="param param-inline rounded-0">
                                                    <dd>
                                                        <select class="form-control form-control-sm"
                                                            onchange="onQtyChange($(this));" style="width:90px;"
                                                            id="product_qty">
                                                            @php
                                                                $qty = $product->qty;
                                                            @endphp
                                                            @for ($i = 1; $i <= $qty; $i++)
                                                                <option name="qty" value="{{ $i }}">
                                                                    {{ $i }}&nbsp;{{ $product->unit->title }}
                                                                </option>
                                                            @endfor

                                                        </select>
                                                    </dd>
                                                </dl>
                                            </div>
                                        @endif
                                        <div class="col-sm-9">
                                            @auth
                                                @if ($product->qty > 0)
                                                    <button id="add_to_cart_button" onclick="addToCart(event);" type="submit"
                                                        class="btn btn-clear text-uppercase mb-3 me-3 ms-2"><i
                                                            class="fa fa-shopping-cart me-1"></i> Add to Cart</button>
                                                @endif
                                            @else
                                                @if ($product->qty > 0)
                                                    <a class="btn btn-clear text-uppercase mb-3 me-3 ms-2"
                                                        data-bs-toggle="modal" data-bs-target="#LoginModal"><i
                                                            class="fa fa-shopping-cart me-1"></i> Add to
                                                        Cart</a>
                                                @endif
                                            @endauth
                                        </div>

                                    </div>
                                    <h3 class="title-product mb-3">{{ $product->name }}
                                    </h3>
                                    <div class="row">
                                        <div class="col-md-12  mb-2">
                                            <span class="stock"><i class="fa fa-check me-2 text-success"></i>
                                                @if ($product->qty > 0)
                                                    Available
                                                @else
                                                    Out of
                                                    Stock
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                    <p class="price-detail-wrap">
                                        <span class="price h3 text-warning">
                                            <span class="currency product_qty_value">
                                                1
                                            </span>
                                            {{ $product->unit->title }}
                                            <input type="hidden" class="productPrices" value="">
                                            <input type="hidden" value="{{ $product->price }}" class="priceMultiply">
                                            <input type="hidden" value="{{ $product->discount }}"
                                                class="product_discount">
                                            <span class="num float-end productPrice">

                                                @if ($product->discount)
                                                    <?php
                                                    
                                                    $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                    ?>
                                                    <span class="discount-style">Rs.{{ $product->price }}</span>
                                                    Rs.{{ $discount_amount }}
                                                @else
                                                    Rs.{{ $product->price }}
                                                @endif
                                            </span>
                                        </span>
                                    </p>
                                    @if ($product->short_description)
                                        <dl class="item-property">
                                            <dt>Description:</dt>
                                            <dd>
                                                {!! $product->short_description !!}
                                            </dd>
                                        </dl>
                                    @endif
                                    <div class="row pt-3">
                                        <div class="col-md-6">
                                            <h6>Product Rating</h6>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="rating rating_star">
                                                @if ($product->avgRating() != null)
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                    round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                    round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                    </div>
                                                    <span class="review-no">

                                                        @if ($totalProductReview <= 9)
                                                            {{ $totalProductReview }}
                                                        @else
                                                            ({{ $totalProductReview }})
                                                        @endif Reviews
                                                    </span>
                                                @else
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                    round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                    round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        &nbsp; No Ratings
                                                    </div>

                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <dl class="item-property">
                                                <dt>Properties:</dt>
                                            </dl>
                                        </div>

                                        <div class="col-md-12 d-flex props">
                                            <strong>
                                                Category
                                            </strong>
                                            <div class="ms-auto">
                                                <p> {{ $product->category->title }}</p>
                                            </div>
                                        </div>
                                        @if ($product->product_type_id != null)
                                            <div class="col-md-12 d-flex props">
                                                <strong>
                                                    Type
                                                </strong>
                                                <div class="ms-auto">
                                                    <p>{{ $product->producttype->title }}</p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    {{-- here add social share --}}
                                    <div class="row mt-2">
                                        <div class="col-md-12">
                                            <dl class="item-property">
                                                <dt>Share On:</dt>
                                            </dl>
                                        </div>
                                        <div class="col-md-12">
                                            <ul class="social_product_detail">
                                                <li><a href="" target="_blank" class="facebook"><i
                                                            class="fa fa-facebook-f"></i></a></li>
                                                <li><a href="" target="_blank" class="twitter "><i
                                                            class="fa fa-twitter"></i></a></li>
                                                <li><a href="" target="_blank" class="youtube "><i
                                                            class="fa fa-linkedin"></i></a></li>
                                                <li><a href="" target="_blank" class="instagram "><i
                                                            class="fa fa-instagram"></i></a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </article>
                            </form>
                            <div class="modal fade login-page" id="LoginModal" tabindex="-1">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">
                                                Welcome! Please Login to continue.</h4>
                                            <button type="button" class="close" data-bs-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true"><i class="fa fa-times-circle-o"
                                                        aria-hidden="true"></i></span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5 class="font-small mb-4 float-end">New member? <a
                                                            href="{{ route('frontend.register') }}">Register
                                                        </a>here</h5>
                                                </div>
                                            </div>
                                            @if (Session::has('error-message'))
                                                <span class="text-danger"
                                                    style="font-size: 80%;"><strong>{{ Session::get('error-message') }}</strong></span>
                                            @endif
                                            <form action="{{ route('login') }}" method="POST">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-login">

                                                            @csrf
                                                            <div class="form-group mb-3">
                                                                <label for="" class="mb-3">Email Address<span
                                                                        class="text-danger">*</span></label>
                                                                <input type="email" placeholder="Please enter your email"
                                                                    class="form-control @error('email') is-invalid @enderror"
                                                                    name="email">
                                                                @error('email')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="mb-3">Password <span
                                                                        class="text-danger">*</span></label>
                                                                <input type="password" id="password"
                                                                    class="form-control @error('password') is-invalid @enderror"
                                                                    placeholder="Password" name="password"
                                                                    autocomplete="current-password">
                                                                <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                                @error('password')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                            <span class="float-end forget_password mt-3 mb-3"><a
                                                                    href="{{ route('frontend.forget_password') }}">Forgot
                                                                    Password
                                                                    ?</a></span>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="login mb-4">
                                                            <button class="btn btn-login w-100 rounded-0"
                                                                type="submit">Login</button>
                                                        </div>
                                                        <span>Or,login with</span>
                                                        <div class="flex items-center justify-end mb-3 mt-4">
                                                            <a class="btn btn-facebook w-100 rounded-0"
                                                                href="{{ route('frontend.redirectToFacebook') }}">
                                                                <i class="fa fa-facebook me-3"></i>Facebook
                                                            </a>
                                                        </div>

                                                        <div class="flex items-center justify-end">
                                                            <a class="btn btn-google w-100 rounded-0"
                                                                href="{{ route('frontend.redirectToGoogle') }}">
                                                                <i class="fa fa-google-plus me-3"></i>Google
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </aside>
                    </div>
                </div>
                @if ($product->description)
                    <hr>
                    <div class="product_desc">
                        <h5>About {{ $product->name }}</h5>
                        {!! $product->description !!}
                    </div>
                @endif
            </div>
        </div>
    </section>
    @if (count($products_from_similar_categories) > 0)
        <section class="pb-5">
            <div class="container">
                <div class="css_title pb-2 d-flex">
                    <div>
                        <h5 class="title_more">More {{ $product->category->title }}</h5>
                    </div>
                    <div class="ms-auto"><a href="" class="">Show All <i
                                class="fa fa-angle-double-right"></i></a></div>
                </div>
                <div class="row card-area new">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">

                            @foreach ($products_from_similar_categories as $data)
                                <div class="card">
                                    <div class="product-grid4">
                                        <div class="product-image4">
                                            <a href="{{ route('frontend.product_detail', $data->slug) }}">
                                                <img class="pic-1" src="{{ $data->getImg($data->image) }}">
                                                <img class="pic-2" src="{{ $data->getImg($data->image) }}">
                                            </a>
                                            <ul class="social">
                                                <li><a href="{{ route('frontend.product_detail', $data->slug) }}"
                                                        data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                                                <li><a href="#" data-tip="Add to Wishlist"><i
                                                            class="fa fa-heart-o"></i></a>
                                                </li>
                                                <li><a href="#" data-tip="Add to Cart"><i
                                                            class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>

                                            @if ($data->discount)
                                                <span class="product-discount-label1">- {{ $data->discount }} %</span>
                                            @endif
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a
                                                    href="{{ route('frontend.product_detail', $data->slug) }}">{!! substr($data->name, 0, 50) !!}</a>
                                            </h3>
                                            <div class="price">

                                                @if ($data->discount)
                                                    <?php
                                                    $discount_amount = $data->price - ($data->discount / 100) * $data->price;
                                                    ?>
                                                    Rs.{{ $discount_amount }}

                                                    <span>Rs.{{ $data->price }}</span>
                                                @else
                                                    Rs.{{ $data->price }}
                                                @endif
                                            </div>
                                            @if ($data->avgRating() != null)
                                                <div class="rating rating_star">
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($data->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($data->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                    round($data->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                    round($data->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                    round($data->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                    round($data->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        <span class="review-no">
                                                            @php
                                                                $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                
                                                                if ($totalProductReview > 0) {
                                                                    $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 5)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    // dd($totalFiveRating);
                                                                    $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                    $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 4)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                    $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 3)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                    $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 2)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                    $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 1)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                                } else {
                                                                    $fivePercent = 0;
                                                                    $fourPercent = 0;
                                                                    $threePercent = 0;
                                                                    $twoPercent = 0;
                                                                    $onePercent = 0;
                                                                }
                                                            @endphp
                                                            @if ($totalProductReview <= 9)
                                                                ({{ $totalProductReview }})
                                                            @else
                                                                ({{ $totalProductReview }})
                                                            @endif
                                                        </span>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        </section>
    @endif
    <section class="pb-5">
        <div class="container">
            <div class="css_title pb-2 d-flex">
                <div>
                    <h5 class="title_more">You Might Also Like <i class="fa fa-thumbs-o-up"></i></h5>
                </div>
                <div class="ms-auto"><a href="" class="">Show All <i
                            class="fa fa-angle-double-right"></i></a></div>
            </div>
            <div class="row card-area new">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        @if (count($more_products) > 0)
                            @foreach ($more_products as $data)
                                <div class="card">
                                    <div class="product-grid4">
                                        <div class="product-image4">
                                            <a href="{{ route('frontend.product_detail', $data->slug) }}">
                                                <img class="pic-1" src="{{ $data->getImg($data->image) }}">
                                                <img class="pic-2" src="{{ $data->getImg($data->image) }}">
                                            </a>
                                            <ul class="social">
                                                <li><a href="{{ route('frontend.product_detail', $data->slug) }}"
                                                        data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                                                <li><a href="#" data-tip="Add to Wishlist"><i
                                                            class="fa fa-heart-o"></i></a>
                                                </li>
                                                <li><a href="#" data-tip="Add to Cart"><i
                                                            class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>

                                            @if ($data->discount)
                                                <span class="product-discount-label1">- {{ $data->discount }}
                                                    %</span>
                                            @endif
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a
                                                    href="{{ route('frontend.product_detail', $data->slug) }}">{!! substr($data->name, 0, 50) !!}</a>
                                            </h3>
                                            <div class="price">

                                                @if ($data->discount)
                                                    <?php
                                                    $discount_amount = $data->price - ($data->discount / 100) * $data->price;
                                                    ?>
                                                    Rs.{{ $discount_amount }}

                                                    <span>Rs.{{ $data->price }}</span>
                                                @else
                                                    Rs.{{ $data->price }}
                                                @endif
                                            </div>
                                            @if ($data->avgRating() != null)
                                                <div class="rating rating_star">
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($data->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($data->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                    round($data->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                    round($data->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                    round($data->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                    round($data->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                    round($data->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        <span class="review-no">
                                                            @php
                                                                $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                
                                                                if ($totalProductReview > 0) {
                                                                    $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 5)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    // dd($totalFiveRating);
                                                                    $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                    $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 4)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                    $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 3)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                    $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 2)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                    $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $data->id)
                                                                        ->where('rating', 1)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                                } else {
                                                                    $fivePercent = 0;
                                                                    $fourPercent = 0;
                                                                    $threePercent = 0;
                                                                    $twoPercent = 0;
                                                                    $onePercent = 0;
                                                                }
                                                            @endphp
                                                            @if ($totalProductReview <= 9)
                                                                ({{ $totalProductReview }})
                                                            @else
                                                                ({{ $totalProductReview }})
                                                            @endif
                                                        </span>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>\
    @auth
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="add-review">
                            @if (count($product_ratings) > 0)
                                <h5 class="mb-5">Product Reviews</h5>
                                <div class="comment">
                                    @foreach ($product_ratings as $product_rating)
                                        <div class="review-box clearfix">
                                            <div class="rev-content">
                                                <div class="rev-header clearfix">
                                                    <h4>{{ $product_rating->name }}</h4>
                                                    <div class="rating-css">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($product_rating->rating, 1) == $i + 0.1 ? '-o' : '', round($product_rating->rating, 1) == $i + 0.2 ? '-o' : '', round($product_rating->rating, 1) == $i + 0.3 ? '-o' : '', round($product_rating->rating, 1) == $i + 0.4 ? '-o' : '', round($product_rating->rating, 1) == $i + 0.5 ? '-half' : '', round($product_rating->rating, 1) == $i + 0.6 ? '-half' : '', round($product_rating->rating, 1) == $i + 0.7 ? '-half' : '', round($product_rating->rating, 1) == $i + 0.8 ? '-half' : '', round($product_rating->rating, 1) == $i + 0.9 ? '-half' : '', round($product_rating->rating, 1) <= $i ? '-o' : '', '" aria-hidden="true"></span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        @php
                                                            $totalRating = round($product_rating->rating, 1);
                                                            echo "($totalRating)";
                                                            echo "\n";
                                                        @endphp
                                                    </div>
                                                </div>
                                                <div class="rev-text">{{ $product_rating->review }}</div>
                                            </div>
                                        </div>
                                    @endforeach

                                    {{-- {{ $product_ratings->links() }} --}}
                                    <button class="loadMore" onclick="loadMore();">Load More</button>

                                </div>
                            @else
                                <div class="text-center pt-4">
                                    <img src="{{ asset('frontend/images/no-rev.png') }}" alt="" class="img-rev">
                                    <p>This product have no reviews.
                                        <br>
                                        Let others know what you think and be the first to write a review.
                                    </p>
                                </div>
                            @endif
                            @php
                                $totalRating = round($product->avgRating(), 1);
                                
                            @endphp
                            @if ($totalRating != null)
                                <div class="text-center rating_count mb-5">
                                    <h1>

                                        @php
                                            $totalRating = round($product->avgRating(), 1);
                                            echo "$totalRating";
                                            echo "\n";
                                        @endphp
                                    </h1>
                                    <div class="rating-star">
                                        <div class="stars">
                                            @for ($i = 0; $i < 5; ++$i)
                                                @php
                                                    echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                    echo "\n";
                                                @endphp
                                            @endfor

                                        </div>
                                        <span class="avg">
                                            Average based on 1 product ratings
                                        </span>
                                    </div>
                                </div>
                            @endif
                            <h5 class="font-weight-bold mb-3">Write a review</h5>
                            <form id="rating_form" action="{{ route('frontend.storeProductRating') }}" method="POST">
                                @csrf
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <p class="db_error"></p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <input type="hidden" id="product_slug" value="{{ $product->slug }}">
                                <input type="hidden" id="product_id" value="{{ $product->id }}" name="product_id">
                                <input type="hidden" id="product_category_id" name="product_category_id"
                                    value="{{ $product->product_category_id }}">
                                <div class="row clearfix">
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label><strong>Rating <span class="text-danger">*</span></strong> </label>
                                        <div class="starRating">
                                            <input id="rating5" type="radio" name="rating" value="5">
                                            <label for="rating5">5</label>
                                            <input id="rating4" type="radio" name="rating" value="4">
                                            <label for="rating4">4</label>
                                            <input id="rating3" type="radio" name="rating" value="3">
                                            <label for="rating3">3</label>
                                            <input id="rating2" type="radio" name="rating" value="2">
                                            <label for="rating2">2</label>
                                            <input id="rating1" type="radio" name="rating" value="1">
                                            <label for="rating1">1</label>
                                        </div>

                                        <span class="require text-danger rating"></span>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                        <label><strong>Name <span class="text-danger">*</span></strong></label>
                                        <input type="text" name="name" id="name" placeholder="Full Name" required="">
                                        <span class="require text-danger name"></span>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                        <label><strong>Email <span class="text-danger">*</span></strong></label>
                                        <input type="email" name="email" id="email" placeholder="Email" required="">
                                        <span class="require text-danger email"></span>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label><strong>Your Review <span class="text-danger">*</span></strong></label>
                                        <textarea class="darma" name="review" id="review" placeholder="Write Your Review..."></textarea>
                                        <span class="require text-danger review"></span>
                                    </div>
                                    <div class="form-group text-center col-md-12 col-sm-12 col-xs-12">
                                        <button id="rating_button" onclick="submitForm(event);" type="submit"
                                            class="btn-clear"><span class="txt">Add Review</span></button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    @endauth


@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/drift-zoom/1.3.1/Drift.min.js"></script>
    <script>
        var driftAll = document.querySelectorAll('.demo-trigger');
        var pane = document.querySelector('.detail');
        $(driftAll).each(function(i, el) {


            let drift = new Drift(
                el, {
                    zoomFactor: 3,
                    paneContainer: pane,
                    inlinePane: false,
                    handleTouch: true

                }
            );
        });
    </script>
    <script>
        $(document).ready(function() {

            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function() {
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function(e) {
                    if (e < onStar) {
                        $(this).addClass('hover');
                    } else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function() {
                $(this).parent().children('li.star').each(function(e) {
                    $(this).removeClass('hover');
                });
            });


            /* 2. Action to perform on click */
            $('#stars li').on('click', function() {
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                var msg = "";
                if (ratingValue > 1) {
                    msg = "Thanks! You rated this " + ratingValue + " stars.";
                } else {
                    msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
                }
                responseMessage(msg);

            });


        });
    </script>
    <script>
        function addToCart(e) {
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#add_to_cart_form").attr("action");

            $.ajax({
                url: url,
                type: 'post',
                data: $("#add_to_cart_form").serialize(),
                success: function(data) {
                    if (data.alertmsg) {
                        Swal.fire(data.alertmsg);
                    }

                    // if (data.loginRoute) {
                    //     location.href = data.loginRoute;
                    // } 
                    else if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {

                        $("#add_to_cart_form")[0].reset();
                        $("#add_to_cart").html(data.cart_count);
                        toastr.success(data.msg);


                    }

                }
            });
        }
    </script>
    <script>
        $(document).ready(function() {
            $(".alert-warning").css('display', 'none');
        })

        function submitForm(e) {
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#rating_form").attr("action");

            $.ajax({
                url: url,
                type: 'post',
                data: $("#rating_form").serialize(),
                success: function(data) {
                    if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {
                        $("#rating_form")[0].reset();
                        toastr.success(data.msg);
                    }

                }
            });
        }
    </script>
    <script>
        let slug = $("#product_slug").val();

        var pageNumber = 1;
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: '/product-detail/' + slug + '?page=' + pageNumber,
                success: function(data) {
                    pageNumber += 1;
                    if (data.html ==
                        '<button class="loadMore" onclick="loadMore();">Load More</button>') {
                        console.log("No data");
                        $(".loadMore").hide();
                    } else {}
                },
                error: function(data) {

                },
            })
        });

        function loadMore() {
            $(".loadMore").html("Loading...");
            $.ajax({
                type: 'GET',
                url: '/product-detail/' + slug + '?page=' + pageNumber,
                success: function(data) {
                    pageNumber += 1;
                    if (data.html == '<button class="loadMore" onclick="loadMore();">Load More</button>') {
                        console.log("No data");
                        $('.loadMore').hide();
                    } else {
                        $('.loadMore').hide();
                        $('.comment').append(data.html);
                    }
                },
                error: function(data) {

                },
            });
        }
    </script>
    <script>
        function onQtyChange(qty) {

            $("#qty").val(qty.val());
            let qty_value = qty.val();
            $(".product_qty_value").text(qty_value);
            let price = $(".priceMultiply").val();

            let discountpercent = $(".product_discount").val();
            console.log(discountpercent);
            let discount = (discountpercent / 100) * price;
            if (discount > 0) {
                newPrice = qty_value * (price - discount);
            } else {
                newPrice = qty_value * price;
            }

            $('.productPrice').text(newPrice);
            $('.productPrices').val(newPrice);

        }
    </script>
@endsection
