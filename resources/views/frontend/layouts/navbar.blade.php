<?php
$nav_product_categories = App\Models\ProductCategory::where('status', 1)
    ->with('product_types')
    ->orderBy('title')
    ->take(6)
    ->get();
  
if(Auth::check()){
$cart_count = App\Models\Cart::where('user_id', Auth::user()->id)->count();
} else {
    $cart_count = 0;
}

?>
<div class="main_header">
    <div class="header-top">
        <div class="container clearfix">
            <div class="row">
                <div class="col-md-6 col-lg-8 col-sm-6 col-12">
                    <ul class="top-nav float-start">
                        <li class="margin-r"><a href="" class="" target=" _blank"><i
                                    class="fa fa-phone me-1"></i>
                                {{ $company_data->company_phone }}</a>
                        </li>
                        @if ($company_data->company_email)
                            <li class="margin-r"><a href="" class="" target=" _blank"><i
                                        class="fa fa-envelope me-1"></i>
                                    {{ $company_data->company_email }}</a></li>
                        @endif
                        <li class="margin-r"><a href="" class="" target=" _blank"><i
                                    class="fa fa-map-marker me-1"></i>{{ $company_data->company_address }}</a></li>
                    </ul>

                </div>
                <div class="right-block clearfix col-md-6 col-lg-4 col-sm-6 col-12">
                    <ul class="top-nav float-end">
                        @if ($social_data->insta_url)
                            <li><a href="{{ $social_data->insta_url }}" class="" target=" _blank"><i
                                        class="fa fa-instagram ms-4"></i></a>
                            </li>
                        @endif
                        @if ($social_data->facebook_url)
                            <li><a href="{{ $social_data->facebook_url }}" class="" target=" _blank"><i
                                        class="fa fa-facebook ms-4"></i></a>
                            </li>
                        @endif
                        @if ($social_data->youtube_url)
                            <li><a href="{{ $social_data->youtube_url }}" class="" target=" _blank"><i
                                        class="fa fa-youtube ms-4"></i></a>
                            </li>
                        @endif
                        @if ($social_data->twitter_url)
                            <li><a href="{{ $social_data->twitter_url }}" class="" target=" _blank"><i
                                        class="fa fa-twitter ms-4"></i></a>
                            </li>
                        @endif
                        @if ($social_data->linkedin_url)
                            <li><a href="{{ $social_data->linkedin_url }}" class="" target=" _blank"><i
                                        class="fa fa-linkedin ms-4"></i></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-4 my-auto">
                    <div class="search align-items-center right_side_search">
                        <form class="input-group search_css search-form" method="get" id="formSearch"
                            action="{{ route('frontend.search') }}">
                            <input type="text" name="query" class="form-control search_input_Css" placeholder="Search"
                                aria-label="search" id="searchForm" required>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary search_button"><i
                                        class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 logo text-center">
                    <a href="{{ route('frontend.index') }}">
                        <img src="{{ $company_data->companyImage($company_data->company_logo) }}"
                            class="img-responsive" alt="" height="" width=""></a>
                </div>
                <div class="col-md-12 col-lg-4 my-auto">
                    <div class="cart ms-2 float-end relative-style">
                        {{-- if user is not login --}}
                        @auth

                            <a class="font-login_signup">{{ auth()->user()->name }}</a>
                            {{-- go to profile is user is login else go to login --}}
                            <a class="cart_a" id="user_profile_icon">
                                <i class="fa fa-user-circle ms-4"></i>
                                <div class="card user-profile-style user-card rounded-0" id="user_profile">
                                    <div class="card-body">
                                        <div class="user_name_style">
                                            <a href="{{ route('frontend.user_profile') }}">
                                                <h3>{{ auth()->user()->name }}</h3>
                                            </a>
                                        </div>
                                        <ul>
                                            <li><a href="{{ route('frontend.user_profile') }}"><i
                                                        class="fa fa-user-circle me-3 profile-icon"></i>My Details</a></li>
                                            <li><a href="{{ route('frontend.user_profile') }}"><i
                                                        class="fa fa-shopping-bag me-3 profile-icon"></i>Order History</a>
                                            </li>
                                            <li>
                                                <form action="{{ route('logout') }}" method="POST">
                                                    @csrf
                                                    <button type="submit" class="logout_button"><i
                                                            class="fa fa-power-off me-3 profile-icon"></i>Logout</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </a>
                            {{-- <a class="cart_a" href=""><i
                                    class="fa fa-user-circle ms-4"></i>

                            </a> --}}
                        @else
                            <a href="{{ route('frontend.login') }}" class="me-3 font-login_signup">Login</a>
                            <a href="{{ route('frontend.register') }}" class="font-login_signup">Signup</a>

                            <a class="cart_a" href="{{ route('frontend.login') }}"><i
                                    class="fa fa-user-circle ms-4"></i>

                            </a> {{-- if user is login in --}}
                        @endauth


                        {{-- if login show this --}}

                        @auth
                            <a class="cart_a" href="{{ route('frontend.cart') }}" aria-expanded="false"><i
                                    class="fa fa-shopping-cart ms-4"></i>
                                <span id="add_to_cart" class="badge badge-danger" style="position:absolute;">

                                    {{ $cart_count }}

                                </span>
                            </a>
                        @else
                            <a class="cart_a" href="{{ route('frontend.login') }}" aria-expanded="false"><i
                                    class="fa fa-shopping-cart ms-4"></i>
                                <span id="add_to_cart" class="badge badge-danger" style="position:absolute;">

                                    {{ $cart_count }}

                                </span>
                            </a>
                        @endauth


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top_section" id="main_menus">
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-expand-lg w-100 justify-content-end text-center mynav">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon d-flex align-items-center justify-content-center"><i
                                class="fa fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse navbarClass" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto w-100">
                            <li class="nav-item flex-fill @yield('home')">
                                <a class="nav-link" href="{{ route('frontend.index') }}"><i
                                        class="fa fa-home"></i></a>
                            </li>


                            <li class="nav-item flex-fill @yield('shop')">
                                <a class="nav-link" href="{{ route('frontend.shop') }}">Explore All</a>
                            </li>
                            @if (count($nav_product_categories) > 0)
                                @foreach ($nav_product_categories as $nav_product_category)
                                    @if (!$nav_product_category->product_types->isEmpty())
                                        <li class="nav-item dropdown position-static flex-fill @yield('product')">
                                            <a class="nav-link dropdown-toggle" data-toggle="dropdown"
                                                href="{{ route('frontend.product', $nav_product_category->slug) }}"
                                                id="navbarDropdown"
                                                role="button">{{ $nav_product_category->title }}</a>
                                            <ul class="dropdown-menu megamenu menu-items">
                                                <li>
                                                    <ul>
                                                        <h6 class="list-header">BY TYPE</h6>
                                                        @foreach ($nav_product_category->product_types->where('status', 1) as $product_type)
                                                            <li><a
                                                                    href="{{ route('frontend.product', ['category_slug' => $nav_product_category->slug,'type_slug' => $product_type->slug]) }}">{{ $product_type->title }}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    @else
                                        <li class="nav-item flex-fill">
                                            <a class="nav-link"
                                                href="{{ route('frontend.product', $nav_product_category->slug) }}">{{ $nav_product_category->title }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<nav class="card rounded-0 text-center fixed-top mobilenav pe-3">
    <div class="row">
        <div class="mobile_btn flex-fill">
            <a class="sidemenu btn  btn-link  waves-effect waves-light" onclick="openNav()">
                <i class="fa fa-bars fa-2x"></i>
            </a>
            <div id="mySidenav" class="sidenav text-center">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <ul class="navbar-nav mx-auto w-100" id="menu_show">
                    <li class="nav-item flex-fill @yield('home')">
                        <a class="nav-link" href="{{ route('frontend.index') }}"><i
                                class="fa fa-home"></i></a>
                    </li>


                    <li class="nav-item flex-fill @yield('shop')">
                        <a class="nav-link" href="{{ route('frontend.shop') }}">Explore All</a>
                    </li>
                    @if (count($nav_product_categories) > 0)
                        @foreach ($nav_product_categories as $nav_product_category)
                            @if (!$nav_product_category->product_types->isEmpty())
                                <li class="nav-item dropdown position-static flex-fill @yield('product')">
                                    <a class="nav-link dropdown-toggle"
                                        onclick="toogle('myDropdown{{ $nav_product_category->id }}')">{{ $nav_product_category->title }}</a>
                                    <ul class="dropdown-menu res-megamenu megamenu"
                                        id="myDropdown{{ $nav_product_category->id }}">
                                        <div class="row">
                                            <li class="col-md-3">
                                                <ul>
                                                    <h6 class="list-header">BY STYLE</h6>
                                                    @foreach ($nav_product_category->product_types->where('status', 1) as $product_type)
                                                        <li><a
                                                                href="{{ route('frontend.product', $product_type->slug) }}">{{ $product_type->title }}</a>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </li>

                                        </div>
                                    </ul>
                                </li>
                            @else
                                <li class="nav-item flex-fill">
                                    <a class="nav-link"
                                        href="{{ route('frontend.product', $nav_product_category->slug) }}">{{ $nav_product_category->title }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif

                </ul>
            </div>

        </div>
        <div class="mobile_btn flex-fill">
            <a href="{{ route('frontend.index') }}" class="btn  btn-link  waves-effect waves-light">
                <i class="fa fa-home fa-2x"></i>
            </a>
        </div>
        <div class="mobile_btn flex-fill">
            <a href="{{ route('frontend.user_profile') }}" class="btn  btn-link  waves-effect waves-light">
                <i class="fa fa-user fa-2x"></i>
            </a>
        </div>

        <div class="mobile_btn flex-fill">
            <a href="{{ route('frontend.cart') }}" class="btn  btn-link  waves-effect waves-light"> <i
                    class="fa fa-shopping-cart fa-2x"></i>
                <span class="badge badge-danger" style="position:absolute;">0
                </span>
            </a>
        </div>
    </div>
</nav>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    function toogle(ulID) {
        console.log(ulID);
        $("#" + ulID).toggleClass('res-megamenu');
        if ($("#" + ulID).hasClass('res-megamenu')) {
            $("#" + ulID).hide();
        } else {
            $("#" + ulID).show();
        }
    }
    // $(".dropbtn").on("click", function() {
    //     console.log($(this, 'ul:first-child'));
    //     $("#myDropdown").toggleClass('res-megamenu');
    //     if ($("#myDropdown").hasClass("res-megamenu")) {
    //         $("#myDropdown").hide();
    //     } else {
    //         $("#myDropdown").show();
    //     }
    // });

    $("#user_profile_icon").on('click', function() {
        $("#user_profile").toggleClass('user-card');
        if ($("#user_profile").hasClass('user-card')) {
            $("#user_profile").hide();
        } else {
            $("#user_profile").show();
        }

    });


    function openNav() {
        document.getElementById("mySidenav").style.width = "350px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
