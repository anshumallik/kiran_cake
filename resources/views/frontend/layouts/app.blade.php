<!DOCTYPE html>
<?php
$company_data = App\Models\CompanyData::first();
$social_data = App\Models\Social::orderBy('id', 'desc')
    ->where('status', 1)
    ->first();
$cart_count = Auth::check() && Auth::user()->user_type ? App\Models\Cart::where('user_id', auth()->user()->id)->count() : 0;
?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">
    <title>{{ $company_data->company_name }} | @yield('page-name')</title>
    <link rel="icon" href="{{ $company_data->companyImage($company_data->company_logo) }}" type="image/x-icon"
        sizes="16x16">
    @include('frontend.layouts.style')
</head>

<body>
    @include('frontend.layouts.navbar')
    @yield('content')
    <div style="height: auto;"></div>
    @include('frontend.layouts.script')
    @include('frontend.layouts.footer')

</body>

</html>
