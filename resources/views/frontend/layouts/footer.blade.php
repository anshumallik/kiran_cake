<a href="#" id="button" style="display: block;">

</a>
<div class="footer_above_image">
    <footer class="footer">
        <div class="container bottom_border">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <h5 class="heading col_white pt2">Contact us</h5>
                    <ul class="footer_ul">
                        <li><a href=""><i class="fa fa-map-marker me-2"></i> {{ $company_data->company_address }}</a>
                        </li>
                        <li><a href=""><i class="fa fa-phone me-2"></i>{{ $company_data->company_phone }}
                                @if ($company_data->company_phone1)
                                    , {{ $company_data->company_phone1 }}
                                @endif

                            </a></li>
                        @if ($company_data->company_email)
                            <li><a href=""><i class="fa fa fa-envelope me-2"></i>{{ $company_data->company_email }}</a>
                            </li>
                        @endif
                    </ul>
                    <ul class="social_footer_ul">
                        @if ($social_data->facebook_url)
                            <li><a href="{{ $social_data->facebook_url }}"><i class="fa fa-facebook-f"></i></a></li>
                        @endif
                        @if ($social_data->twitter_url)
                            <li><a href="{{ $social_data->twitter_url }}"><i class="fa fa-twitter"></i></a></li>
                        @endif
                        @if ($social_data->linkedin_url)
                            <li><a href="{{ $social_data->linkedin_url }}"><i class="fa fa-linkedin"></i></a></li>
                        @endif
                        @if ($social_data->insta_url)
                            <li><a href="{{ $social_data->insta_url }}"><i class="fa fa-instagram"></i></a></li>
                        @endif
                        @if ($social_data->youtube_url)
                            <li><a href="{{ $social_data->youtube_url }}"><i class="fa fa-youtube"></i></a></li>
                        @endif
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="heading col_white pt2">Your Account</h5>
                    <ul class="footer_ul">
                        <li><a href="{{ route('frontend.user_profile') }}">Personal info</a></li>
                        <li><a href="{{ route('frontend.user_profile') }}">Orders</a></li>
                        <li><a href="{{ route('frontend.user_profile') }}">Addresses</a></li>
                    </ul>

                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="heading col_white pt2">Products</h5>
                    <ul class="footer_ul">
                        <li><a href=""> New products</a></li>
                        <li><a href="">Best sales</a></li>
                        <li><a href="{{ route('frontend.contact') }}"> Sitemap</a></li>

                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="heading col_white pt2"> Our Company</h5>
                    <ul class="footer_ul">
                       
                        <li><a href="{{ route('frontend.about') }}"> About us</a></li>

                        <li><a href="{{ route('frontend.contact') }}"> Contact us</a></li>
                        <li><a href="{{ route('frontend.privacy_policy') }}"> Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container copyright">
            <div class="row">
                <div class="col-md-12 p-0">
                    <p class="pt-3">Copyright <?php echo Date('Y'); ?> {{ $company_data->company_name }} | Designed by <a
                            href="https://paailatechnologies.com/">Paailatechnologies</a></p>
                </div>

            </div>
        </div>
    </footer>
</div>



<script>
    var btn = $('#button');

    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, '300');
    });
</script>
