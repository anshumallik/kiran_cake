<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script> --}}
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('frontend/js/index.js') }}"></script>
<script src="{{ asset('js/client.min.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
<?php
$date = date('Y-m-d');
$time = date('h:i:s');
?>
<script>
    // script fot total visit
    $(document).ready(() => {
        let client = new ClientJS();
        let fingerprint = client.getFingerprint();
        let userAgent = client.getUserAgent();
        let browser = client.getBrowser();
        let browser_version = client.getBrowserVersion();
        let os_name = client.getOS();
        let os_ver = client.getOSVersion();
        let timez = client.getTimeZone();
        let date = '<?php echo $date; ?>';
        let time = '<?php echo $time; ?>';
        console.log(timez);
        $.ajax({
            url: "{{ route('frontend.storeVisit') }}",
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'fingerprint': fingerprint,
                'useragent': userAgent,
                'browser': browser,
                'br_ver': browser_version,
                'os': os_name,
                'os_ver': os_ver,
                'timezone': timez,
            },
            success: function(response) {
                console.log(response.message);
            },
            error: function() {
                console.log('Failure');
            },
        });
    });
    // end of total vist script
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-container",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    @if (Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch (type) {
        case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
    
        case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
    
        case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
    
        case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
        }
    @endif
</script>
@yield('script')
