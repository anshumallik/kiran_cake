<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/footer.css') }}">
<link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
<style>
    .toast-top-container {
        position: absolute;
        top: 65px;
        width: 280px;
        right: 40px;
        height: auto;
    }

    .bgColor {
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url({{ asset('frontend/images/aboutbanner.jpg') }});

    }

    #button {
        height: 100px;
        width: 100px;
        background-image: url({{ asset('frontend/images/top_to_btm.png') }});

    }

    .footer_above_image:before {
        background-repeat: no-repeat;
        content: "";
        float: left;
        width: 100%;
        height: 73px;
        background-image: url({{ asset('frontend/images/footerimage.png') }});

    }

    .starRating:not(old)>label {
        display: block;
        float: right;
        position: relative;
        background: url({{ asset('frontend/images/star.png') }});
        background-size: contain;
    }

    .starRating:not(old)>label:before {
        content: "";
        display: block;
        width: 1.5em;
        height: 1.5em;
        background: url({{ asset('frontend/images/star3.png') }});
        background-size: contain;
        opacity: 0;
        transition: opacity 0.2s linear;
    }

</style>
@yield('style')
