@extends('frontend.layouts.app')
@section('page-name', 'Contact Us')
@section('contact', 'active')
@section('content')
    <section class="contact-section pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="css_title text-center pb-5">
                        <h1 class="title">Contact Us</h1>
                        <p>Do not hesitate to contact us if you have any further questions</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-column">
                        <div class="content">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-box">
                                        <i class="fa fa-home"></i>
                                    </div>

                                </div>
                                <div class="col-md-10 my-auto">
                                    <ul class="ps-3">
                                        <li>{{ $company_data->company_address }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($company_data->company_email)
                    <div class="info-column col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-column">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon-box">
                                            <i class="fa fa-envelope-open"></i>
                                        </div>

                                    </div>
                                    <div class="col-md-10 my-auto">
                                        <ul class="ps-3">
                                            <li>{{ $company_data->company_email }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-column">
                        <div class="content">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="icon-box">
                                        <i class="fa fa-phone"></i>
                                    </div>

                                </div>
                                <div class="col-md-10 my-auto">
                                    <ul class="ps-3">
                                        <li>{{ $company_data->company_phone }}
                                            @if ($company_data->company_phone1)
                                                , {{ $company_data->company_phone1 }}
                                            @endif
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-form-section pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact-form">
                                <div class="row">
                                    <form id="contact-form" action="{{ route('frontend.storecontact') }}" method="POST"
                                        class="col-md-6">
                                        @csrf

                                        <div class="padding-form">
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <p class="db_error"></p>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <div class="form-group mb-3">
                                                <input type="text" placeholder="Name" class="form-control" name="name">
                                                <span class="require text-danger name"></span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <input type="text" placeholder="Phone" class="form-control" name="phone">
                                                <span class="require text-danger phone"></span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <input type="email" placeholder="Email" class="form-control" name="email">
                                                <span class="require text-danger email"></span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <textarea name="message" id="" cols="30" rows="8" class="form-control" placeholder="Your Message"></textarea>
                                                <span class="require text-danger message"></span>
                                            </div>
                                            <div class="form-froup mb-3">

                                                <div class="recaptcha" id="g-recaptcha1"></div>
                                                <span class="require text-danger g-recaptcha-response"></span>
                                            </div>
                                            <button id="submitBtn" type="submit" onclick="submitForm(event);"
                                                class="btn btn-primary rounded-0 w-100">Submit</button>

                                        </div>

                                    </form>
                                    <div class="map-outer col-md-6">
                                        <div class="map-inner">
                                            <iframe
                                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBCqNb6VMs3jVnL-3rfDR7H4norB1w5Gz0&amp;q=Biratnagar,Nepal"
                                                width="100%" height="600" frameborder="0" style="border:0"
                                                allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
@section('script')

    <script>
        $(document).ready(function(){
            $(".alert-warning").css('display','none');
        })
        function submitForm(e) {
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#contact-form").attr("action");
            $.ajax({
                url: url,
                type: 'post',
                data: $("#contact-form").serialize(),
                success: function(data) {
                    if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {
                        $("#contact-form")[0].reset();
                        toastr.success(data.msg);
                    }

                }
            });
        }
    </script>

    <script>
        setTimeout(function() {

            $('.recaptcha').each(function() {
                grecaptcha.render(this.id, {
                    'sitekey': '6LdVkwkUAAAAACeeETRX--v9Js0vWyjQOTIZxxeB',
                    "theme": "light"
                });
            });

        }, 2000);
    </script>
@endsection
