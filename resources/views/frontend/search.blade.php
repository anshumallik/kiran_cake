@extends('frontend.layouts.app')
@section('page-name', 'Search Product')
@section('content')
    <section class="search pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search_page">
                        <h4>Search Result For : {{ request('query') }}</h4>
                        @if ($searchResults->count() > 0)
                            <h5 class="mt-4">{{ $searchResults->count() }} results found
                                for {{ request('query') }}
                                !</h5>
                        @else
                            <div class="no-result">
                                <i class="fa fa-frown-o"></i>
                                <h3>Oops! No results found
                                </h3>
                                <p>Sorry. We couldn't find what your are looking for. Please try with different
                                    query.
                                </p>
                            </div>
                        @endif
                    </div>
                    <div class="row card-area pt-4">
                        @foreach ($searchResults->groupByType() as $type => $modelSearchResults)
                            @foreach ($modelSearchResults as $searchResult)
                                <div class="col-md-6 col-lg-2">
                                    <div class="card mb-3">
                                        <div class="product-grid4">
                                            <div class="product-image4">
                                                <a href="{{ $searchResult->url }}">
                                                    <img class="pic-1"
                                                        src="{{ asset('images/product/' . $searchResult->searchable['image']) }}">
                                                    <img class="pic-2"
                                                        src="{{ asset('images/product/' . $searchResult->searchable['image']) }}">
                                                </a>
                                                <ul class="social">
                                                    <li><a href="{{ $searchResult->url }}" data-tip="Quick View"><i
                                                                class="fa fa-search"></i></a>
                                                    </li>
                                                    <li><a href="#" data-tip="Add to Wishlist"><i
                                                                class="fa fa-heart-o"></i></a>
                                                    </li>
                                                    <li><a href="#" data-tip="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                </ul>


                                            </div>
                                            <div class="product-content">
                                                <h3 class="title"><a
                                                        href="{{ $searchResult->url }}">{{ $searchResult->title }}</a>
                                                </h3>
                                                <div class="price">

                                                    Rs. {{ $searchResult->searchable['price'] }}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
