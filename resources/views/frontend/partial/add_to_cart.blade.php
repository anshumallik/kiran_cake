<div class="row">
    <div class="col-md-6 divide-right">
        <div class="row">
            <div class="col-md-5">
                <img class="product-image img-fluid"
                    src="{{ asset('frontend/images/c3.jpg') }}" alt="">
            </div>
            <div class="col-md-7">
                <h6 class="h6 product-name font-weight-bold">{{ $product->name }}
                </h6>
                <p><strong>Type:</strong> {{ $product->producttype->title }}</p>
                <p><strong>Volume:</strong> 1 {{ $product->unit->title }}</p>
                <p><strong>Quantity:</strong> 1</p>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="cart-content">
            <p class="cart-products-count">There is 1 item in your cart.
            </p>
            <p><strong>Total products:</strong> Rs. xyz</p>
            <p><strong>Total shipping:</strong>Free </p>
            <p><strong>Taxes</strong> Rs. xyz</p>
            <p><strong>Total:</strong> Rs. xyz (tax excl.)</p>
            <div class="cart-content-btn">
                <a href="{{ route('frontend.shop') }}"
                    class="btn btn-shopping rounded-0 mr-2">Continue
                    shopping</a>
                <a href="{{ route('frontend.checkout') }}"
                    class="btn btn-checkout rounded-0"><i class="fa fa-check mr-1"></i>Proceed
                    to
                    checkout</a>
            </div>
        </div>
    </div>
</div>