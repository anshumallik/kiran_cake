@extends('frontend.layouts.app')
@section('page-name', 'Shop')
@section('shop', 'active')
@section('content')
    <section class="pt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            @if (count($sliders) > 0)
                                @foreach ($sliders as $slider)
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        @if ($slider->link)
                                            <a href="{{ $slider->link }}" target="_blank">
                                                <img src="{{ $slider->getImg($slider->image) }}" class="d-block w-100"
                                                    alt="...">
                                            </a>
                                        @else
                                            <img src="{{ $slider->getImg($slider->image) }}" class="d-block w-100"
                                                alt="...">
                                        @endif

                                    </div>
                                @endforeach
                            @else
                                <div class="carousel-item active">
                                    <img src="{{ asset('frontend/images/banner.png') }}" class="d-block w-100" alt="...">

                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('frontend/images/banner12.jpg') }}" class="d-block w-100"
                                        alt="...">

                                </div>
                                <div class="carousel-item">
                                    <img src="{{ asset('frontend/images/back.jpg') }}" class="d-block w-100" alt="...">

                                </div>
                            @endif

                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pb-5 pt-5">
        <div class="container">
            <div class="css_title pb-4 d-flex">
                <div>
                    <h5 class="title_more">Featured Products</h5>
                </div>

            </div>
            <div class="row card-area new">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        @if (count($featured_products) > 0)
                            @foreach ($featured_products as $featured_product)
                                <div class="card">
                                    <div class="product-grid4">
                                        <div class="product-image4">
                                            <a href="{{ route('frontend.product_detail', $featured_product->slug) }}">
                                                <img class="pic-1"
                                                    src="{{ $featured_product->getImg($featured_product->image) }}">
                                                <img class="pic-2"
                                                    src="{{ $featured_product->getImg($featured_product->image) }}">
                                            </a>
                                            <ul class="social">
                                                <li><a href="{{ route('frontend.product_detail', $featured_product->slug) }}"
                                                        data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                                                <li><a href="#" data-tip="Add to Wishlist"><i
                                                            class="fa fa-heart-o"></i></a>
                                                </li>
                                                <li><a href="#" data-tip="Add to Cart" data-bs-toggle="modal"
                                                        data-bs-target="#cartModal2"> <i
                                                            class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>
                                            @if (count($featured_product->product_ratings) > 0)
                                                <span class="product-new-label"><i class="fa fa-star-o"><a href=""
                                                            class="rating">
                                                            @php
                                                                $totalRating = round($featured_product->avgRating(), 1);
                                                                echo "$totalRating";
                                                                echo "\n";
                                                            @endphp</a></i>
                                                </span>
                                            @endif
                                            @if ($featured_product->discount)
                                                <span class="product-discount-label1">- {{ $featured_product->discount }}
                                                    %</span>
                                            @endif
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a
                                                    href="{{ route('frontend.product_detail', $featured_product->slug) }}">{!! substr($featured_product->name, 0, 50) !!}</a>
                                            </h3>
                                            <div class="price">

                                                @if ($featured_product->discount)
                                                    <?php
                                                    $discount_amount = $featured_product->price - ($featured_product->discount / 100) * $featured_product->price;
                                                    ?>
                                                    Rs.{{ $discount_amount }}

                                                    <span>Rs.{{ $featured_product->price }}</span>
                                                @else
                                                    Rs.{{ $featured_product->price }}
                                                @endif
                                            </div>
                                            @if ($featured_product->avgRating() != null)
                                                <div class="rating rating_star">
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($featured_product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($featured_product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($featured_product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($featured_product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($featured_product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($featured_product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($featured_product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($featured_product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($featured_product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($featured_product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        <span class="review-no">
                                                            @php
                                                                $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $featured_product->id)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                
                                                                if ($totalProductReview > 0) {
                                                                    $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $featured_product->id)
                                                                        ->where('rating', 5)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    // dd($totalFiveRating);
                                                                    $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                    $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $featured_product->id)
                                                                        ->where('rating', 4)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                    $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $featured_product->id)
                                                                        ->where('rating', 3)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                    $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $featured_product->id)
                                                                        ->where('rating', 2)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                    $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $featured_product->id)
                                                                        ->where('rating', 1)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                                } else {
                                                                    $fivePercent = 0;
                                                                    $fourPercent = 0;
                                                                    $threePercent = 0;
                                                                    $twoPercent = 0;
                                                                    $onePercent = 0;
                                                                }
                                                            @endphp
                                                            @if ($totalProductReview <= 9)
                                                                ({{ $totalProductReview }})
                                                            @else
                                                                ({{ $totalProductReview }})
                                                            @endif
                                                        </span>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="pb-5">
        <div class="container">
            <div class="css_title pb-4 d-flex">
                <div>
                    <h5 class="title_more">New Arrivals</h5>
                </div>

            </div>
            <div class="row card-area new">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        @if (count($latest_products) > 0)
                            @foreach ($latest_products as $latest_product)
                                <div class="card">
                                    <div class="product-grid4">
                                        <div class="product-image4">
                                            <a href="{{ route('frontend.product_detail', $featured_product->slug) }}">
                                                <img class="pic-1"
                                                    src="{{ $latest_product->getImg($latest_product->image) }}">
                                                <img class="pic-2"
                                                    src="{{ $latest_product->getImg($latest_product->image) }}">
                                            </a>
                                            <ul class="social">
                                                <li><a href="{{ route('frontend.product_detail', $latest_product->slug) }}"
                                                        data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                                                <li><a href="#" data-tip="Add to Wishlist"><i
                                                            class="fa fa-heart-o"></i></a>
                                                </li>
                                                <li><a href="#" data-tip="Add to Cart" data-bs-toggle="modal"
                                                        data-bs-target="#cartModal2"> <i
                                                            class="fa fa-shopping-cart"></i></a>
                                                </li>
                                            </ul>
                                            @if (count($latest_product->product_ratings) > 0)
                                                <span class="product-new-label"><i class="fa fa-star-o"><a href=""
                                                            class="rating">
                                                            @php
                                                                $totalRating = round($latest_product->avgRating(), 1);
                                                                echo "$totalRating";
                                                                echo "\n";
                                                            @endphp</a></i>
                                                </span>
                                            @endif
                                            <span class="product-discount-label1">New</span>
                                        </div>
                                        <div class="product-content">
                                            <h3 class="title"><a
                                                    href="{{ route('frontend.product_detail', $latest_product->slug) }}">{!! substr($latest_product->name, 0, 50) !!}</a>
                                            </h3>
                                            <div class="price">

                                                @if ($latest_product->discount)
                                                    <?php
                                                    $discount_amount = $latest_product->price - ($latest_product->discount / 100) * $latest_product->price;
                                                    
                                                    ?>
                                                    Rs.{{ $discount_amount }}

                                                    <span>Rs.{{ $latest_product->price }}</span>
                                                @else
                                                    Rs.{{ $latest_product->price }}
                                                @endif
                                            </div>
                                            @if ($latest_product->avgRating() != null)
                                                <div class="rating rating_star">
                                                    <div class="stars">
                                                        @for ($i = 0; $i < 5; ++$i)
                                                            @php
                                                                echo '<span class="fa fa-star', round($latest_product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($latest_product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($latest_product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($latest_product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($latest_product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($latest_product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($latest_product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($latest_product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($latest_product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($latest_product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           </span>';
                                                                echo "\n";
                                                            @endphp
                                                        @endfor
                                                        <span class="review-no">
                                                            @php
                                                                $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $latest_product->id)
                                                                    ->where('status', 1)
                                                                    ->count();
                                                                
                                                                if ($totalProductReview > 0) {
                                                                    $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $latest_product->id)
                                                                        ->where('rating', 5)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    // dd($totalFiveRating);
                                                                    $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                                    $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $latest_product->id)
                                                                        ->where('rating', 4)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                                    $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $latest_product->id)
                                                                        ->where('rating', 3)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                                    $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $latest_product->id)
                                                                        ->where('rating', 2)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                                    $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $latest_product->id)
                                                                        ->where('rating', 1)
                                                                        ->where('status', 1)
                                                                        ->count();
                                                                    $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                                } else {
                                                                    $fivePercent = 0;
                                                                    $fourPercent = 0;
                                                                    $threePercent = 0;
                                                                    $twoPercent = 0;
                                                                    $onePercent = 0;
                                                                }
                                                            @endphp
                                                            @if ($totalProductReview <= 9)
                                                                ({{ $totalProductReview }})
                                                            @else
                                                                ({{ $totalProductReview }})
                                                            @endif
                                                        </span>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </section>


    <section class="pb-5">
        <div class="container">
            <div class="css_title pb-4 d-flex">
                <div>
                    <h5 class="title_more">Trending Products</h5>
                </div>
            </div>
            <div class="row card-area new">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        @if (count($trending_products) > 0)
                            @foreach ($trending_products as $trending_product)
                                <div class="pairing_img">
                                    <a href="{{ route('frontend.product_detail', $trending_product->slug) }}">
                                        <img src="{{ $trending_product->getImg($trending_product->image) }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="top_rating_title">
                    <h2>Sale Product</h2>
                </div>
                @if (count($best_selling_products) > 0)
                    @foreach ($best_selling_products as $best_selling_product)
                        <div class="col-md-4">
                            <div class="d-flex mb-5">
                                <div class="img-style">
                                    <a href="{{ route('frontend.product_detail', $best_selling_product->slug) }}">
                                        <img src="{{ $best_selling_product->getImg($best_selling_product->image) }}"
                                            alt="">
                                    </a>
                                </div>
                                <div class="p-3 product-d">
                                    <h3 class="product-t">
                                        <a href="{{ route('frontend.product_detail', $best_selling_product->slug) }}">
                                            {!! substr($best_selling_product->name, 0, 40) !!}</a>
                                    </h3>
                                    <div class="price-p">

                                        @if ($best_selling_product->discount)
                                            <?php
                                            $discount_amount = $best_selling_product->price - ($best_selling_product->discount / 100) * $best_selling_product->price;
                                            ?>
                                            <span>Rs.{{ $best_selling_product->price }}</span>&nbsp;Rs.{{ $discount_amount }}
                                        @else
                                            Rs.{{ $best_selling_product->price }}
                                        @endif

                                    </div>

                                    @if (count($best_selling_product->product_ratings) > 0)
                                        <div class="rating rating_star">
                                            <div class="stars">
                                                @for ($i = 0; $i < 5; ++$i)
                                                    @php
                                                        echo '<span class="fa fa-star', round($best_selling_product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($best_selling_product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                round($best_selling_product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                round($best_selling_product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                 round($best_selling_product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                  round($best_selling_product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                   round($best_selling_product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                   round($best_selling_product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                   round($best_selling_product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                   round($best_selling_product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               </span>';
                                                        echo "\n";
                                                    @endphp
                                                @endfor
                                                <span class="review-no">
                                                    @php
                                                        $totalProductReview = App\Models\ProductRating::where('product_ratings.product_id', $best_selling_product->id)
                                                            ->where('status', 1)
                                                            ->count();
                                                        
                                                        if ($totalProductReview > 0) {
                                                            $totalFiveRating = App\Models\ProductRating::where('product_ratings.product_id', $best_selling_product->id)
                                                                ->where('rating', 5)
                                                                ->where('status', 1)
                                                                ->count();
                                                            // dd($totalFiveRating);
                                                            $fivePercent = round(($totalFiveRating * 100) / $totalProductReview, 1);
                                                            $totalFourRating = App\Models\ProductRating::where('product_ratings.product_id', $best_selling_product->id)
                                                                ->where('rating', 4)
                                                                ->where('status', 1)
                                                                ->count();
                                                            $fourPercent = round(($totalFourRating * 100) / $totalProductReview, 1);
                                                            $totalThreeRating = App\Models\ProductRating::where('product_ratings.product_id', $best_selling_product->id)
                                                                ->where('rating', 3)
                                                                ->where('status', 1)
                                                                ->count();
                                                            $threePercent = round(($totalThreeRating * 100) / $totalProductReview, 1);
                                                            $totalTwoRating = App\Models\ProductRating::where('product_ratings.product_id', $best_selling_product->id)
                                                                ->where('rating', 2)
                                                                ->where('status', 1)
                                                                ->count();
                                                            $twoPercent = round(($totalTwoRating * 100) / $totalProductReview, 1);
                                                            $totalOneRating = App\Models\ProductRating::where('product_ratings.product_id', $best_selling_product->id)
                                                                ->where('rating', 1)
                                                                ->where('status', 1)
                                                                ->count();
                                                            $onePercent = round(($totalOneRating * 100) / $totalProductReview, 1);
                                                        } else {
                                                            $fivePercent = 0;
                                                            $fourPercent = 0;
                                                            $threePercent = 0;
                                                            $twoPercent = 0;
                                                            $onePercent = 0;
                                                        }
                                                    @endphp
                                                    @if ($totalProductReview <= 9)
                                                        ({{ $totalProductReview }})
                                                    @else
                                                        ({{ $totalProductReview }})
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="icons mt-3">
                                        <ul>
                                            <li><a href="" class="active"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li><a href=""><i class="fa fa-heart-o"></i></a></li>
                                            <li><a href=""><i class="fa fa-search"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection
