@extends('frontend.layouts.app')
@section('page-name', 'Forget Password')
@section('forget-password', 'active')
@section('content')
    <section class="login-page pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h4 class="title_data">Forgot your password?</h4>
                    <div class="card mt-4 rounded-0">
                        <div class="card-body">
                            <div class="row">
                                <span class="mb-3 font-forget">Please enter the account that you want to reset the
                                    password.</span>
                                <div class="col-md-6">
                                    <div class="form-login">
                                        <form action="{{ route('frontend.updateUserForgetPassword') }}" method="POST">
                                            @csrf
                                            @if (Session::has('msg'))
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    {{ Session::get('msg') }}
                                                    <a type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </a>
                                                </div>
                                            @endif
                                            <div class="form-group mb-3">
                                                <input type="email" placeholder="Please enter your email"
                                                    class="form-control" name="email" value="{{ old('email') }}">
                                                @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                            <div class="form-group mb-3">

                                                <input type="password" id="password" class="form-control"
                                                    placeholder="Please enter your new password" name="password">
                                                <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                @error('password')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-3">

                                                <input type="password" id="password_confirmation" class="form-control"
                                                    placeholder="Please retype your password" name="password_confirmation">
                                                <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                @if ($errors->has('password_confirmation'))
                                                    <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                                @endif
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-primary rounded-0 w-100">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
