@extends('frontend.layouts.app')
@section('page-name', 'Cart')
@section('cart', 'active')
@section('content')
    <section class="cart-page pt-5 pb-5">
        <div class="container">
            <div class="row">
                @if (count($cart_items) > 0)
                    <div class="col-lg-8 col-md-12">
                        @php
                            $total = 0;
                            $qty = 0;
                        @endphp
                        @foreach ($cart_items as $cart_item)
                            <div class="row card-box mb-3 product_data">
                                <div class="col-lg-2 col-12">
                                    <div class="product_image">
                                        <a href="">
                                            <img src="{{ $cart_item->products->getImg($cart_item->products->image) }}"
                                                alt="" class="img-fluid">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-12">
                                    <span class="product-discount-label">
                                        {{-- <i class="fa fa-trash" id="delteItem"></i> --}}
                                        <button class="delete-cart-item"><i class="fa fa-trash"></i></button>
                                    </span>
                                    <div class="cart-content">
            
                                        <a href="{{ route('frontend.product_detail', ['slug' => $cart_item->products->slug]) }}">
                                            <h4>{{ $cart_item->products->name }}</h4>
                                        </a>
                                        <hr>

                                        <div class="d-flex">
                                            <div class="price_cart">
                                                <input type="hidden" class="prod_id"
                                                    value="{{ $cart_item->product_id }}">
                                                <h5 class="mb-0">

                                                    @if ($cart_item->products->discount)
                                                        <?php
                                                        $discount_amount = $cart_item->products->price - ($cart_item->products->discount / 100) * $cart_item->products->price;
                                                        ?>
                                                        Rs.{{ $discount_amount }}
                                                        <span>Rs.{{ $cart_item->products->price }}</span>
                                                    @else
                                                        Rs.{{ $cart_item->products->price }}
                                                    @endif
                                                </h5>
                                                <h6 class="mt-2"> Category
                                                    :&nbsp;{{ $cart_item->products->category->title }}
                                                    @if ($cart_item->products->producttype)
                                                        ,&nbsp;Type :&nbsp;
                                                        {{ $cart_item->products->producttype->title }}
                                                    @endif
                                                </h6>
                                            </div>
                                            <div class="ms-auto sidebar_inc_dec_btn">
                                                {{-- @if ($cart_item->products->qty >= $cart_item->qty) --}}
                                                    <button class="value-button changeQuantity decrement-btn">-</button>
                                                    <input type="text" class="number qty-input"
                                                        data-qty="{{ $cart_item->products->qty }}" data-prod_id = "{{ $cart_item->products->product_id }}"
                                                        value="{{ $cart_item->qty }}" readonly/>
                                                    <button class="value-button changeQuantity increment-btn">+</button>
                                                    <?php
                                                    $discount_amount = $cart_item->products->price - ($cart_item->products->discount / 100) * $cart_item->products->price;
                                                    ?>
                                                    @php
                                                        $total += $cart_item->qty * $discount_amount;
                                                        $qty += $cart_item->qty;
                                                    @endphp
                                                {{-- @else
                                                    Out of stock --}}
                                                {{-- @endif --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="col-lg-4 col-md-12 order-summary">
                        <div class="card rounded-0">
                            <div class="card-body">
                                <h5 class="summary-heading">Order Summary</h5>
                                <div class="row">
                                    <div class="col-lg-8 col-md-6">
                                        <p>Subtotal ({{ $qty }} items)</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <p class="float-end">Rs. {{ $total }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-6">
                                        <p>Shipping Fee</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <p class="float-end">Rs.100</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-6">
                                        <p class="fw-bold">Total</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <p class="float-end total_amount">Rs. {{ $total }}</p>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-lg-6">
                                        <a href="{{ route('frontend.shop') }}">
                                            <button class="btn btn-shopping w-100 rounded-0">Shop More</button>
                                        </a>
                                    </div>
                                    <div class="col-lg-6">
                                        <a href="{{ route('frontend.checkout') }}">
                                            <button class="btn btn-checkout  w-100 rounded-0">Checkout</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="text-center my-auto">
                        <p>There are no items in this cart</p>
                        <a href="{{ route('frontend.shop') }}">
                            <button class="btn btn-shopping rounded-0">Continue Shopping</button>
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $('.increment-btn').click(function(e) {
            e.preventDefault();
            var inc_value = $(this).closest('.product_data').find('.qty-input').val();
            var actual_qty = $(this).closest('.product_data').find('.qty-input').data('qty');
var prod_id = $(this).closest('.product_data').find('.prod_id').val();
            var value = parseInt(inc_value, 10);
            // console.log(value);
            value = isNaN(value) ? 0 : value;
            if (value < actual_qty) {
                value++;
                $(this).closest('.product_data').find('.qty-input').val(value);
                updateCartItem(e, prod_id, value);
            } else {
                Swal.fire(

                    'Only ' + actual_qty + ' quantity left in stock',

                )
                
            }
        });
        $('.decrement-btn').click(function(e) {
            e.preventDefault();
            var dec_value = $(this).closest('.product_data').find('.qty-input').val();
            var prod_id = $(this).closest('.product_data').find('.prod_id').val();
            console.log(prod_id);
            var value = parseInt(dec_value, 10);
            value = isNaN(value) ? 0 : value;
            if (value > 1) {
                value--;
                $(this).closest('.product_data').find('.qty-input').val(value);
                updateCartItem(e, prod_id, value);
            }
        });

        function updateCartItem(e, prod_id, prod_qty){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // var prod_id = $(this).closest('.product_data').find('.prod_id').val();
            // var qty = $(this).closest('.product_data').find('.qty-input').val();
            // console.log(qty);

            // var actual_qty = $(this).closest('.product_data').find('.qty-input').data('qty');
            // console.log(actual_qty)
            $.ajax({
                method: "POST",
                url: "update-cart-item",
                data: {
                    'prod_id': prod_id,
                    'prod_qty': prod_qty
                },
                success: function(data) {
                    window.location.reload();
                   
                    if (data.msg) {
                        toastr.success(data.msg);
                    }



                }
            });
        }

        // $('.changeQuantity').click(function(e) {
        //     e.preventDefault();
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     var prod_id = $(this).closest('.product_data').find('.prod_id').val();
        //     var qty = $(this).closest('.product_data').find('.qty-input').val();
        //     console.log(qty);

        //     var actual_qty = $(this).closest('.product_data').find('.qty-input').data('qty');
        //     console.log(actual_qty)
        //     $.ajax({
        //         method: "POST",
        //         url: "update-cart-item",
        //         data: {
        //             'prod_id': prod_id,
        //             'prod_qty': qty
        //         },
        //         success: function(data) {
        //             window.location.reload();
        //             // if (data.alertmsg) {
        //             //     Swal.fire(
        //             //         '',
        //             //         data.alertmsg,
        //             //         'success'
        //             //     )

        //             // }
        //             if (data.msg) {
        //                 toastr.success(data.msg);
        //             }



        //         }
        //     });
        // })
    </script>
    <script>
        function deleteItem(product_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "delete-cart-item",
                data: {
                    'prod_id': product_id,
                },
                success: function(data) {
                    Swal.fire(
                        'Deleted!',
                        data.alertmsg,
                        'success'
                    )
                    window.location.reload();
                }
            });
        }
        $('.delete-cart-item').click(function(e) {
            e.preventDefault();


            var prod_id = $(this).closest('.product_data').find('.prod_id').val();
            Swal.fire({
                title: 'Are you sure?',
                text: "Item(s) will be deleted from cart . This action can not be reverted",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Yes'
            }).then((result) => {

                if (result.isConfirmed) {
                    deleteItem(prod_id);


                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss == 'cancel'
                ) {
                    Swal.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'success'
                    )
                }


            });

        });
    </script>
@endsection
