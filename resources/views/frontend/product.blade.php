@extends('frontend.layouts.app')
@section('page-name', 'Product')
@section('product', 'active')
@section('content')
    <section class="product_page pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <aside class="single_sidebar_widget search_widget mt-5">
                        <div class="">
                            <div class="card rounded-0 search_product mb-3">
                                <div class="input-group">
                                    <input type="text" name="query" class="form-control" id="searchForm"
                                        placeholder="Search Something" onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Search Something'">
                                    <div class="input-group-append">
                                        <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0 search_product search_detail">
                                <div class="d-flex title-search">
                                    <div class="my-auto">
                                        <h5 class="mb-0"><i class="fa fa-sliders me-2"></i> Filter</h5>
                                    </div>
                                    <div class="ms-auto my-auto">
                                        <a href="" class="btn btn-clear"><i class="fa fa-times"></i> Clear All</a>
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush pt-1 pb-2">
                                    <h6 class="title_sidebar">
                                        Price Range
                                    </h6>
                                    <li class="list-group-item">
                                        <div class="d-flex">
                                            <div>

                                                <a
                                                    href="{{ route('frontend.product', ['category_slug' => request()->category_slug, 'price_range' => 1 - 500]) }}"><label>
                                                        <input name='price_range' class="me-2" type="checkbox"
                                                            data-route="{{ route('frontend.product', ['category_slug' => request()->category_slug, 'price_range' => '1-500']) }}"
                                                            value='1-500'  {{ request()->price_range == '1-500' ? 'checked' : '' }}/>
                                                        Under Rs. 500</label>
                                                </a>
                                                <a
                                                    href="{{ route('frontend.product', ['category_slug' => request()->category_slug, 'price_range' => 500 - 1000]) }}"><label><input
                                                            name='price_range' class="me-2" type="checkbox"
                                                            value='500-1000'
                                                            data-route="{{ route('frontend.product', ['category_slug' => request()->category_slug, 'price_range' => '500-1000']) }}" {{ request()->price_range == '500-1000' ? 'checked' : '' }} />
                                                        Rs. 500 to Rs. 1000</label>
                                                </a>
                                                <a
                                                    href="{{ route('frontend.product', ['category_slug' => request()->category_slug, 'price_range' => 1000 - 1500]) }}"><label><input
                                                            name='price_range' class="me-2" type="checkbox"
                                                            value='1000-1500'
                                                            data-route="{{ route('frontend.product', ['category_slug' => request()->category_slug, 'price_range' => '1000-1500']) }}" {{ request()->price_range == '1000-1500' ? 'checked' : '' }}/>
                                                        Rs. 1000 to Rs.1500</label>
                                                </a>
                                            </div>
                                            <div class="ms-auto">
                                                <a href="">(26)</a>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                                @if (count($types) > 0)
                                    <ul class="list-group list-group-flush pt-1 pb-2">
                                        <h6 class="title_sidebar">
                                            Type
                                        </h6>

                                        <form>
                                            @foreach ($types as $type)
                                                <li class="list-group-item">
                                                    <div class="d-flex">
                                                        <div>
                                                            <a href="#"><label>
                                                                    <input name='type_slug' data-route="{{ route('frontend.product',['category_slug' => request()->category_slug, 'type_slug' => $type->slug]) }}" class="me-2 type" type="checkbox"
                                                                        value='{{ $type->id }}' id="type" {{ request()->type_slug == $type->slug ? 'checked' : '' }}/>
                                                                    {{ $type->title }}
                                                                </label>
                                                            </a>
                                                        </div>
                                                        <div class="ms-auto">
                                                            <a
                                                                href="">({{ App\Models\Product::where('product_type_id', $type->id)->count() }})</a>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </form>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </aside>
                </div>
                <div class="col-12 col-lg-8 col-md-6 product-content mt-5">
                    <div class="card up-data rounded-0 p-3">
                        <div class="d-flex">
                            <div class="list_product_items my-auto">

                                <i class="fa fa-th me-2 active" id="grid_style" onclick="gridView()"></i>
                                <i class="fa fa-list" id="list_style" onclick="listView()"></i>
                            </div>
                            <div class="side_select ms-auto">
                                <div class="d-flex">
                                    <div class="me-3 my-auto">
                                        <label for="">Sort By:</label>
                                    </div>
                                    <form id="sortProductForm" name="sortProductForm">
                                        <div class="form-group my-auto">
                                            <select class="form-control" name="sort" id="sort" onchange="sort_by()">
                                                <option class="option-css" value="">Best Match</option>
                                                <option class="option-css" value="name_a_to_z"
                                                    @if (isset($_GET['sort']) && $_GET['sort'] == 'name_a_to_z') selected @endif>Name A to Z</option>
                                                <option class="option-css" value="name_z_to_a"
                                                    @if (isset($_GET['sort']) && $_GET['sort'] == 'name_z_to_a') selected @endif>Name Z to A</option>
                                                <option class="option-css" value="price_low_to_high"
                                                    @if (isset($_GET['sort']) && $_GET['sort'] == 'price_low_to_high') selected @endif>Price low to high
                                                </option>
                                                <option class="option-css" value="price_high_to_low"
                                                    @if (isset($_GET['sort']) && $_GET['sort'] == 'price_high_to_low') selected @endif>Price high to low
                                                </option>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product_title_side mt-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('frontend.index') }}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    <a href="{{ route('frontend.product', Request::segment(2)) }}">
                                        {{ Request::segment(2) ? ucfirst(Request::segment(2)) : '' }}</a>
                                </li>
                                @if (Request::segment(3) != null)
                                    <li class="breadcrumb-item active" aria-current="page">
                                        {{ Request::segment(3) ? ucfirst(Request::segment(3)) : '' }}</li>
                                @endif
                            </ol>
                        </nav>
                    </div>
                    <div class="row card-area" id="list">
                        @if (count($products) > 0)
                            @foreach ($products as $product)
                                <div class="col-md-6 col-lg-4">
                                    <div class="card mb-3">
                                        <div class="product-grid4">
                                            <div class="product-image4">
                                                <a href="{{ route('frontend.product_detail', $product->slug) }}">
                                                    <img class="pic-1"
                                                        src="{{ $product->getImg($product->image) }}">
                                                    <img class="pic-2"
                                                        src="{{ $product->getImg($product->image) }}">
                                                </a>
                                                <ul class="social">
                                                    <li><a href="{{ route('frontend.product_detail', $product->slug) }}"
                                                            data-tip="Quick View"><i class="fa fa-search"></i></a>
                                                    </li>
                                                    <li><a href="#" data-tip="Add to Wishlist"><i
                                                                class="fa fa-heart-o"></i></a>
                                                    </li>
                                                    <li><a href="#" data-tip="Add to Cart"><i
                                                                class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                </ul>

                                                @if ($product->discount)
                                                    <span class="product-discount-label1">- {{ $product->discount }}
                                                        %</span>
                                                @endif
                                            </div>
                                            <div class="product-content">
                                                <h3 class="title"><a
                                                        href="{{ route('frontend.product_detail', $product->slug) }}">{!! substr($product->name, 0, 50) !!}</a>
                                                </h3>
                                                <div class="price">

                                                    @if ($product->discount)
                                                        <?php
                                                        $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                        ?>
                                                        Rs.{{ $discount_amount }}

                                                        <span>Rs.{{ $product->price }}</span>
                                                    @else
                                                        Rs.{{ $product->price }}
                                                    @endif
                                                </div>
                                                @if ($product->avgRating() != null)
                                                    <div class="rating rating_star">
                                                        <div class="stars">
                                                            @for ($i = 0; $i < 5; ++$i)
                                                                @php
                                                                    echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                    round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                    round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>';
                                                                    echo "\n";
                                                                @endphp
                                                            @endfor
                                                            <span class="review-no">

                                                                @if ($totalProductReview <= 9)
                                                                    ({{ $totalProductReview }})
                                                                @else
                                                                    ({{ $totalProductReview }})
                                                                @endif
                                                            </span>
                                                        </div>

                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="float-end">
                                        @if (isset($_GET['sort']) && !empty($_GET['sort']))
                                            {{ $products->appends(['sort' => $_GET['sort']])->links() }}
                                        @else
                                            {{ $products->links() }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-md-12">
                                No item available
                            </div>
                        @endif
                    </div>

                    <div class="row grid-style" id="grid" style="display: none">
                        <div class="col-lg-12 col-md-6 card-area">
                            @if (count($products) > 0)
                                @foreach ($products as $product)
                                    <div class="card rounded-0 grid_area mb-3">
                                        <div class="card-body row">
                                            <div class="col-md-3 my-auto">
                                                <a href="{{ route('frontend.product_detail', $product->slug) }}"><img
                                                        src="{{ $product->getImg($product->image) }}" alt=""
                                                        class="img-fluid"></a>
                                            </div>

                                            <div class="col-md-9 grid my-auto">
                                                <a href="{{ route('frontend.product_detail', $product->slug) }}">
                                                    <h6>{{ $product->name }}</h6>
                                                </a>
                                                <div class="price mb-3">
                                                    @if ($product->discount)
                                                        <?php
                                                        $discount_amount = $product->price - ($product->discount / 100) * $product->price;
                                                        ?>
                                                        Rs.{{ $discount_amount }}

                                                        <span>Rs.{{ $product->price }}</span>
                                                    @else
                                                        Rs.{{ $product->price }}
                                                    @endif
                                                </div>

                                                @if ($product->avgRating() != null)
                                                    <div class="rating rating_star">
                                                        <div class="stars">
                                                            @for ($i = 0; $i < 5; ++$i)
                                                                @php
                                                                    echo '<span class="fa fa-star', round($product->avgRating(), 1) == $i + 0.1 ? '-o' : 
                                                '', round($product->avgRating(), 1) == $i + 0.2 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.3 ? '-o' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.4 ? '-o' : '',
                                                    round($product->avgRating(), 1) == $i + 0.5 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.6 ? '-half' : '',
                                                    round($product->avgRating(), 1) == $i + 0.7 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.8 ? '-half' : '', 
                                                    round($product->avgRating(), 1) == $i + 0.9 ? '-half' : '', 
                                                    round($product->avgRating(), 1) <= $i ? '-o' : '', '">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   </span>';
                                                                    echo "\n";
                                                                @endphp
                                                            @endfor
                                                            <span class="review-no">

                                                                @if ($totalProductReview <= 9)
                                                                    ({{ $totalProductReview }})
                                                                @else
                                                                    ({{ $totalProductReview }})
                                                                @endif
                                                            </span>
                                                        </div>

                                                    </div>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="float-end">
                                {{ $products->links() }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- <form id="productFilter">
        <input type="hidden" id="sort" name="sort">
    </form> --}}
@endsection
@section('script')
    <script>
        function gridView() {
            $("#list").show();
            $("#grid").hide();
        }

        function listView() {
            $("#list").hide();
            $("#grid").show();
        }

        $('#grid_style').click(function(e) {
            e.preventDefault();
            $('#list_style').removeClass('active');
            $('#grid_style').addClass('active');
        });
        $('#list_style').click(function(e) {
            e.preventDefault();
            $('#grid_style').removeClass('active');
            $('#list_style').addClass('active');
        });
    </script>
    <script>
        // function sort_by() {
        //     var sort_by_value = $('#sort_by_value').val(); -----select ko id
        //     $("#sort").val(sort_by_value);
        //     $("#productFilter").submit();
        // }
        $(document).ready(function() {
            $("#sort").on('change', function() {
                this.form.submit();
            });

            $("input[name=price_range]").on('click', function() {
                var route = $(this).data('route');
                location.href = route;
            });

            $("input[name=type_slug]").on('click', function() {
                var route = $(this).data('route');
                location.href = route;
            });
        });
    </script>
@endsection
