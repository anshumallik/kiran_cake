@extends('frontend.layouts.app')
@section('page-name', 'Cart')
@section('cart', 'active')
@section('content')
    <section class="checkout-page pt-5 pb-5">
        <div class="container">
            <form action="{{ route('frontend.storeOrder') }}" method="POST" id="checkout-form">
                @csrf
                <div class="row mb-5">
                    <div class="col-lg-8 col-md-12">
                        <div class="card rounded-0">
                            <div class="card-body1">
                                <h5 class="summary-heading">Delivery Information</h5>
                                <div class="form-login">
                                    <div class="row">
                                        <input type="hidden" id="user_id"
                                            value="{{ auth()->user() ? auth()->user()->id : '' }}" name="user_id">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group mb-3">
                                                <label for="">Full name<span class="text-danger">*</span></label>
                                                <input type="text" placeholder="Please enter your full name"
                                                    class="form-control" name="name">
                                                <span class="require text-danger name"></span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="">Phone Number <span class="text-danger">*</span></label>
                                                <input type="text" placeholder="Please Enter your phone number"
                                                    class="form-control" name="phone">
                                                <span class="require text-danger phone"></span>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group mb-3">
                                                <label for="">Province <span class="text-danger">*</span></label>
                                                <select name="province_id" id="province_id" class="form-control select2"
                                                    onchange="getCity();">
                                                    <option value="">Please choose your Region</option>
                                                    @foreach ($provinces as $province)
                                                        <option data-id='{{ $province->id }}'
                                                            value="{{ $province->id }}">
                                                            {{ $province->title }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="require text-danger province_id"></span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="">City <span class="text-danger">*</span></label>
                                                <select name="city_id" id="city_id" class="form-control select2">
                                                    <option value="">Please choose your city</option>
                                                </select>
                                                <span class="require text-danger city_id"></span>
                                            </div>

                                            <div class="form-group mb-4">
                                                <label for="">Address <span class="text-danger">*</span></label>
                                                <input type="text" placeholder="For example House# Street#"
                                                    class="form-control" name="address">
                                                <span class="require text-danger address"></span>
                                            </div>
                                            <div>
                                                <button class="btn btn-checkout  w-100 rounded-0"
                                                    onclick="storeCheckoutData(event);" type="submit">Checkout</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $total = 0;
                        $qty = 0;
                        foreach ($cart_items as $cart_item) {
                            $discount_amount = $cart_item->products->price - ($cart_item->products->discount / 100) * $cart_item->products->price;
                            $total += $cart_item->qty * $discount_amount;
                            $qty += $cart_item->qty;
                        }
                    @endphp
                    <div class="col-lg-4 col-md-12 order-summary">
                        <div class="card rounded-0">
                            <input type="hidden" name="total_price" value="{{ $total }}">
                            <div class="card-body">
                                <h5 class="summary-heading">Order Summary</h5>
                                <div class="row">
                                    <div class="col-lg-8 col-md-6">
                                        <p>Subtotal ({{ $qty }} items)</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <p class="float-end">Rs. {{ $total }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-6">
                                        <p>Shiping Fee</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <p class="float-end">Rs.100</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-6">
                                        <p class="fw-bold">Total</p>
                                    </div>
                                    <div class="col-lg-4 col-md-6">
                                        <p class="float-end total_amount">Rs. {{ $total }}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @if (count($cart_items) > 0)
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="card rounded-0">
                            <div class="card-body">

                                @foreach ($cart_items as $cart_item)
                                    <div class="row  card-box1 margin-card-item product_data">
                                        <div class="col-lg-2 col-12">
                                            <div class="product_image">
                                                <img src="{{ $cart_item->products->getImg($cart_item->products->image) }}"
                                                    alt="" class="img-fluid">
                                            </div>
                                        </div>
                                        <div class="col-lg-10 col-12">
                                            <span class="product-discount-label">
                                                <button class="delete-cart-item"><i class="fa fa-trash"></i></button>
                                            </span>
                                            <div class="cart-content">
                                                <h4>{{ $cart_item->products->name }}</h4>
                                                <hr>
                                                <div class="d-flex">
                                                    <div class="price_cart">
                                                        <input type="hidden" class="prod_id"
                                                            value="{{ $cart_item->product_id }}">
                                                        <h5 class="mb-0">

                                                            @if ($cart_item->products->discount)
                                                                <?php
                                                                $discount_amount = $cart_item->products->price - ($cart_item->products->discount / 100) * $cart_item->products->price;
                                                                ?>
                                                                Rs.{{ $discount_amount }}
                                                                <span>Rs.{{ $cart_item->products->price }}</span>
                                                            @else
                                                                Rs.{{ $cart_item->products->price }}
                                                            @endif
                                                        </h5>
                                                        <h6 class="mt-2"> Category
                                                            :&nbsp;{{ $cart_item->products->category->title }}
                                                            @if ($cart_item->products->producttype)
                                                                ,&nbsp;Type :&nbsp;
                                                                {{ $cart_item->products->producttype->title }}
                                                            @endif
                                                        </h6>
                                                    </div>
                                                    <div class="ms-auto sidebar_inc_dec_btn">

                                                        <span class="font-qty">Qty : {{ $cart_item->qty }}</span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach


                            </div>
                        </div>
                    </div>

                </div>
            @endif
        </div>
    </section>
@endsection
@section('script')
    <script>
        function deleteItem(product_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "delete-cart-item",
                data: {
                    'prod_id': product_id,
                },
                success: function(data) {
                    Swal.fire(
                        'Deleted!',
                        data.alertmsg,
                        'success'
                    )
                    window.location.reload();
                }
            });
        }
        $('.delete-cart-item').click(function(e) {
            e.preventDefault();


            var prod_id = $(this).closest('.product_data').find('.prod_id').val();
            Swal.fire({
                title: 'Are you sure?',
                text: "Item(s) will be deleted from cart . This action can not be reverted",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Yes'
            }).then((result) => {

                if (result.isConfirmed) {
                    deleteItem(prod_id);


                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss == 'cancel'
                ) {
                    Swal.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'success'
                    )
                }


            });

        });
    </script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });

        function getCity() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var id = $("#province_id").val();
            $.ajax({
                url: "{{ route('frontend.getcity') }}",
                data: {
                    'province_id': id
                },
                type: "POST",
                dataType: "json",
                success: function(res) {

                    if (res) {
                        var options = "";
                        options += "<option value=''>Please Choose your city</option>";

                        $.each(res, function(key, name) {
                            valueData = key;
                            options += "<option value=" + valueData + ">" + name + "</option>";
                        });
                        $("#city_id").html(options);
                    } else {

                        $("#city_id").html("<option value=''>Please Choose your city</option>");
                    }

                }
            });
        }
    </script>
    <script>
        function storeCheckoutData(e) {
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#checkout-form").attr("action");

            $.ajax({
                url: url,
                type: 'post',
                data: $("#checkout-form").serialize(),
                success: function(data) {
                    if (data.alertmsg) {
                        Swal.fire(data.alertmsg);
                    } else if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {
                        $("#checkout-form")[0].reset();
                        toastr.success(data.msg);
                        location.href = data.redirectRoute;
                        // window.location.reload();

                    }

                }
            });
        }
    </script>
@endsection
