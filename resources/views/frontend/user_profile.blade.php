@extends('frontend.layouts.app')
@section('page-name', 'Profile')
@section('profile', 'active')
@section('content')
    <section class="user_profile pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-2">
                            <h4 class="profile">Profile</h4>
                        </div>
                        <div class="col-md-10">
                            <div class="">
                                <p class="mb-0">Logged in as <span class="fw-bold text-dark">@auth
                                            {{ auth()->user()->name }}/ {{ auth()->user()->email }}
                                        @endauth
                                    </span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-lg-4 col-md-6">
                    <div class="user_information">
                        <div class="card rounded-0">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="user_image">
                                        <img src="{{ asset('frontend/images/personicon.jpg') }}" alt="">
                                    </div>
                                    <div class="user_name">
                                        @auth
                                            <h3>{{ auth()->user()->name }}</h3>
                                        @endauth
                                        <form action="{{ route('logout') }}" method="POST">
                                            @csrf
                                            <button type="submit" class="logout_button"><i
                                                    class="fa fa-power-off me-3"></i>Logout</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="user_tabs pt-4">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                        aria-orientation="vertical">
                                        <a class="nav-link active" id="v-pills-mydetail-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-mydetail" type="button" role="tab"
                                            aria-controls="v-pills-mydetail" aria-selected="true"><i
                                                class="fa fa-user-circle me-3"></i>My Details</a>
                                        <a class="nav-link" id="v-pills-order-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-order" type="button" role="tab"
                                            aria-controls="v-pills-order" aria-selected="false"><i
                                                class="fa fa-shopping-bag me-3"></i> Order History</a>
                                        <a class="nav-link" id="v-pills-payment-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-payment" type="button" role="tab"
                                            aria-controls="v-pills-payment" aria-selected="false"><i
                                                class="fa fa-credit-card-alt me-3"></i>Payment Methods</a>
                                        <a class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-settings" type="button" role="tab"
                                            aria-controls="v-pills-settings" aria-selected="false"><i
                                                class="fa fa-home me-3"></i>Addresses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-8 user_profile_form">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-mydetail" role="tabpanel"
                                    aria-labelledby="v-pills-mydetail-tab">
                                    <h5 class="text-uppercase color-detail"><i class="fa fa-user-circle me-3"></i>My Details
                                    </h5>
                                    <div class="row mt-5">
                                        @auth
                                            @if (auth()->user()->name != null)
                                                <div class="col-md-4">
                                                    <div class="user_detail">
                                                        <h5>Full Name</h5>

                                                        <h6>{{ auth()->user()->name }}</h6>


                                                    </div>
                                                </div>
                                            @endif
                                        @endauth
                                        @auth
                                            @if (auth()->user()->phone != null)
                                                <div class="col-md-4">
                                                    <div class="user_detail">
                                                        <h5>Phone Number</h5>
                                                        <h6>{{ auth()->user()->phone }}</h6>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-4">
                                                    <div class="user_detail">
                                                        <h5>Phone Number</h5>
                                                        <h6>Please enter your phone number</h6>
                                                    </div>
                                                </div>
                                            @endif
                                        @endauth
                                        @auth
                                            @if (auth()->user()->email != null)
                                                <div class="col-md-4">
                                                    <div class="user_detail">
                                                        <h5>Email</h5>
                                                        <h6>{{ auth()->user()->email }}</h6>
                                                    </div>
                                                </div>
                                            @endif
                                        @endauth

                                    </div>
                                    <div class="mt-5">
                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade" id="pills-email">
                                                <div class="form_data_user p-4 col-md-8">
                                                    <form action="{{ route('frontend.changeUserEmail') }}" method="POST"
                                                        id="changeUserEmailForm">
                                                        @csrf
                                                        <div class="form-group mb-3">
                                                            <label for="email">Email <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="email" placeholder="Please enter your email"
                                                                class="form-control" name="email"
                                                                value="{{ $user->email }}" readonly="readonly">
                                                        </div>

                                                        <div class="form-group mb-3">
                                                            <label for="email">New Email<span
                                                                    class="text-danger">*</span></label>
                                                            <input type="email" placeholder="Please enter your email"
                                                                class="form-control" name="email" value="">
                                                            @error('email')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <button type="submit" class="btn btn-login rounded-0">Save
                                                            Changes</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="pills-password">

                                                <div class="form_data_user p-4 col-md-8">
                                                    <form action="{{ route('frontend.userNewPassword') }}" method="POST"
                                                        id="userpasswordForm">
                                                        @csrf
                                                        <div class="form-group mb-3">
                                                            <label for="">Current Password <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="password" class="form-control"
                                                                placeholder="Please enter your current password"
                                                                name="current_password" value="">
                                                            <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                            @error('current_password')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>

                                                        <div class="form-group mb-3">
                                                            <label for="">New Password <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="password" id="password" class="form-control"
                                                                placeholder="Please enter your new password"
                                                                name="password">
                                                            <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                            @error('password')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-3">
                                                            <label for="">Confirm New Password <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="password" id="password_confirmation"
                                                                class="form-control"
                                                                placeholder="Please retype your password"
                                                                name="password_confirmation">
                                                            <i class="toggle-password fa fa-fw fa-eye-slash"></i>
                                                        </div>
                                                        <button type="submit" class="btn btn-login rounded-0">Save
                                                            Changes</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="pills-edit-profile">
                                                <div class="form_data_user p-4 col-md-8">
                                                    <form action="{{ route('frontend.changeUserProfile') }}"
                                                        method="POST" id="updateUserProfileForm">
                                                        @csrf
                                                        <div class="form-group mb-3">
                                                            <label for="name">Full Name <span
                                                                    class="text-danger">*</span></label>
                                                            <input type="text" placeholder="Please enter your name"
                                                                class="form-control" name="name" id="name"
                                                                value="{{ old('name', $user->name) }}">
                                                            @error('name')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>

                                                        <div class="form-group mb-3">
                                                            <label for="phone">Phone Number<span
                                                                    class="text-danger">*</span></label>
                                                            <input type="text" placeholder="Please enter your phone"
                                                                class="form-control" id="phone" name="phone"
                                                                value="{{ old('phone', $user->phone) }}">
                                                            @error('phone')
                                                                <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                        <button class="btn btn-login rounded-0">Save Changes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-email-tab" data-bs-toggle="pill"
                                                    data-bs-target="#pills-email">Change Email</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-password-tab" data-bs-toggle="pill"
                                                    data-bs-target="#pills-password">Change
                                                    Password</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-edit-profile-tab"
                                                    data-bs-toggle="pill" data-bs-target="#pills-edit-profile">Edit
                                                    Profile</button>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="v-pills-order" role="tabpanel"
                                    aria-labelledby="v-pills-order-tab">
                                    <h5 class="text-uppercase color-detail"><i class="fa fa-shopping-bag me-3"></i>Order
                                        History

                                    </h5>
                                   
                                    <div class="order_detail mt-4">
                                        <div class="table-responsive table-bordered">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        {{-- <th scope="col">S.N</th> --}}
                                                        <th scope="col">OrdrId</th>
                                                        <th scope="col">Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                
                                                    @foreach ($orders as $order)
                                                        <tr>
                                                            <th>#{{ $order->id }}</th>
                                                            <td>{{ $order->total_price }}</td>
                                                           <td>
                                                               <a href="{{ route('frontend.view_order_detail',$order->id) }}">View Details</a>
                                                           </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-payment" role="tabpanel"
                                    aria-labelledby="v-pills-payment-tab">Payment Methods</div>
                                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel"
                                    aria-labelledby="v-pills-settings-tab">Setting</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $("#pills-email-tab").on('click', function() {
            $("#pills-email").addClass('active');
            $("#pills-email").addClass('show');
            console.log('added active show');
        })
    </script>
    <script>
        $("#changeUserEmailForm").validate({
            rules: {
                email: {
                    required: {
                        depends: function() {
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    customemail: true,
                },
            },
            messages: {
                email: {
                    required: "Email is required",
                    customemail: "Please enter valid email address",
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        $("#userpasswordForm").validate({
            rules: {
                current_password: {
                    required: true,
                },
                password: {
                    required: true,
                    passwordCheck: true,
                    minlength: 10,
                },
                password_confirmation: {
                    equalTo: "#password",
                },
            },
            messages: {
                password: {
                    required: "New Password is required",
                    minlength: "Password must be of at least 10 characters.",
                    passwordCheck: "Password must contain at least one uppercase , lowercase, digit and special character",
                },
                password_confirmation: {
                    equalTo: "Password confirmation does not match.",
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        $("#updateUserProfileForm").validate({
            rules: {
                name: {
                    required: true,
                    lettersonly: true,
                },
                user_address: {
                    stringonly: true,
                },
                phone: {
                    digits: true,
                    minlength: 10,
                    maxlength: 13
                },
            },
            messages: {
                email: {
                    required: "Email is required",
                    customemail: "Please enter valid email address",
                },
                phone: {
                    digits: "Phone must contain only numeric value",
                    minlength: "Phone must have at least 10 digits",
                    maxlength: "The phone length must not be greater than 13",
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    </script>
@endsection
