@extends('layouts.admin.app')
@section('title', 'Product Rating')
@section('product-rating', 'active')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Product Ratings</span>
                                </a>

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {{-- <div class="table-responsive"> --}}
                            <table id="ProductRating" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Product Name</th>
                                        <th>Product Category</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Review</th>
                                        <th>Rating</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($product_ratings as $key => $product_rating)
                                        <tr>
                                            <td>{{ ++$id }}</td>
                                            <td>{{ $product_rating->product->name }}</td>
                                            @if ($product_rating->category->title)
                                                <td>{{ $product_rating->category->title }}</td>
                                            @endif
                                            <td>{{ $product_rating->name }}</td>
                                            <td>{{ $product_rating->email }}</td>
                                            <td>
                                                <textarea>
                                                {{ $product_rating->review }}
                                                </textarea>
                                            </td>
                                            <td>{{ $product_rating->rating }}</td>
                                            <td>
                                                <?php echo $product_rating->status ? '<span class="cur_sor badge badge-success" onclick="updateStatus(' . $product_rating->id . ',$(this))">Active</span>' : '<span class="badge badge-warning cur_sor" onclick="updateStatus(' . $product_rating->id . ',$(this))">Inactive</span>'; ?>
                                            </td>
                                            <td>
                                                <div class="d-inline-flex">
                                                    <form
                                                        action="{{ route('admin.productrating.delete', $product_rating->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method("DELETE")
                                                        <button class="btn btn-danger btn-sm ml-2" type="submit"><i
                                                                class="fa fa-trash iCheck"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- </div> --}}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

    <script>
        $(document).ready(function() {
            $("#ProductRating").DataTable({
                "responsive": false,
                "lengthChange": true,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'csv',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'excel',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],
                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });




        function updateStatus(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.productrating.updateStatus') }}",
                    type: 'POST',
                    data: {
                        'product_rating_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Inactive');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Active');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }
    </script>
@endsection
