@extends('layouts.admin.app')
@section('title', 'Product')
@section('product', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">Product Detail</span>
                                </a>
                                <a href="{{ route('admin.product.index') }}" class="float-right btn btn-primary">
                                    <i class="fa fa-arrow-left iCheck"></i>&nbsp;Back to List
                                </a>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="col-md-12 mb-5">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Product Category</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $product->category->title }}
                                            </div>
                                        </div>
                                    </div>
                                    @if ($product->producttype)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="">Product Type</label>
                                                </div>
                                                <div class="col-md-9">

                                                    {{ $product->producttype->title }}

                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="code">Product Code&nbsp;</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $product->code }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="name">Product Name&nbsp;</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $product->name }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Unit </label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $product->unit->title }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="price">Price </label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $product->price }}

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="qty">Quantity </label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $product->qty }}
                                            </div>
                                        </div>
                                    </div>
                                    @if ($product->dicount)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="discount">Discount</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {{ $product->discount }}
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($product->short_description)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="description">Description&nbsp;</label>
                                                </div>
                                                <div class="col-md-9">
                                                    {{ $product->short_description }}


                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="status">Status </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($product->status == 1)
                                                    Active
                                                @elseif($product->status == 0)
                                                    Inactive
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="is_featured">Is Featured </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($product->is_featured == 1)
                                                    Yes
                                                @elseif($product->is_featured == 0)
                                                    No
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="is_trending">Is Trending </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($product->is_trending == 1)
                                                    Yes
                                                @elseif($product->is_trending == 0)
                                                    No
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="is_latest">Is Latest </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($product->is_latest == 1)
                                                    Yes
                                                @elseif($product->is_latest == 0)
                                                   No
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="is_bestselling">Is Best Selling </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($product->is_bestselling == 1)
                                                    Yes
                                                @elseif($product->is_bestselling == 0)
                                                    No
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="image">Product Image</label>
                                            </div>
                                            <div class="col-md-9">

                                                <img src="{{ $product->getImg($product->image) }}" class="imgSize"
                                                    id="imgPreview">

                                            </div>
                                        </div>
                                    </div>
                                    @if ($product->description)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="description">Description&nbsp;</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" rows="4">{{ strip_tags(html_entity_decode($product->description)) }}</textarea>


                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            {{-- @if ($product->product_images == null) --}}
                            @if ($product->product_images)
                                <div class="col-md-12">
                                    <div class="mt-4 mb-5">
                                        <a>
                                            <span class="content-header"><strong>Product Images</strong></span>
                                        </a>
                                        <a href="{{ route('admin.productImage.create', $product->id) }}"
                                            class="float-right btn btn-primary">
                                            <i class="fa fa-plus iCheck"></i>&nbsp;Add Images
                                        </a>
                                    </div>
                                    <table id="ProductImage" class="table table-responsive-xl text-center">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Image</th>

                                                <th>Status</th>
                                                <th class="hidden">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $id = 0;
                                            ?>
                                            @foreach ($product->product_images as $key => $product_image)
                                                <tr>
                                                    <td>{{ ++$id }}</td>
                                                    <td>
                                                        <img src="{{ $product_image->getImg($product_image->image) }}"
                                                            class="imageSize" alt="">
                                                    </td>

                                                    <td>

                                                        <?php echo $product_image->status ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-warning">Inactive</span>'; ?>


                                                    </td>
                                                    <td>
                                                        <div class="d-inline-flex">
                                                            <a href="{{ route('admin.productImage.edit', $product_image->id) }}"
                                                                class="btn btn-sm btn-primary" title="Edit Product">
                                                                <i class="fa fa-edit iCheck"></i> Edit
                                                            </a>

                                                            <form
                                                                action="{{ route('admin.productImage.delete', $product_image->id) }}"
                                                                method="POST">
                                                                @csrf
                                                                @method("DELETE")
                                                                <button class="btn btn-danger btn-sm ml-2" type="submit"><i
                                                                        class="fa fa-trash iCheck"></i>&nbsp;Delete</button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            @endif
                            {{-- @endif --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')

    <script>
        $(document).ready(function() {
            $("#ProductImage").DataTable({
                "responsive": false,
                "lengthChange": true,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'csv',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'excel',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],
                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });
    </script>
@endsection
