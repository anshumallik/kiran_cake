@extends('layouts.admin.app')
@section('title', 'Product')
@section('product', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">Edit Product</span>
                                </a>
                                <a href="{{ route('admin.product.index') }}" class="float-right btn btn-primary">
                                    <i class="fa fa-arrow-left iCheck"></i>&nbsp;Back to List
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.product.update', $product->id) }}" method="POST"
                                enctype="multipart/form-data" id="form">
                                @csrf
                                @method('put')
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <p class="db_error"></p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="">Select Product Category</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="product_category_id" id="product_category_id"
                                                        class="form-control select2" onchange="getProductType();">
                                                        <option value="">Select Product Category</option>
                                                        @foreach ($product_categories->where('status', 1) as $product_category)
                                                            <option value="{{ $product_category->id }}"
                                                                {{ $product_category->id == $product->product_category_id ? 'selected' : '' }}>
                                                                {{ $product_category->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="require product_category_id text-danger"></span>
                                                    @error('product_category_id')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="">Select Product Type</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="product_type_id" id="product_type_id"
                                                        class="form-control select2">
                                                        <option value="">Select Product Type</option>
                                                        {{-- @foreach ($product_types as $product_type)
                                                            <option value="{{ $product_type->id }}"
                                                                {{ $product_type->id == $product->product_type_id ? 'selected' : '' }}>
                                                                {{ $product_type->title }}</option>
                                                        @endforeach --}}
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="title">Select Selling Type</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="product_selling_type" class="form-control select2"
                                                        id="product_selling_type">
                                                        <option value="">Select Selling Type</option>
                                                        <option value="featured_product"
                                                            {{ $product->product_selling_type == 'featured_product' ? 'selected' : '' }}>
                                                            Featured Product</option>
                                                        <option value="trending_product"
                                                            {{ $product->product_selling_type == 'trending_product' ? 'selected' : '' }}>
                                                            Trending Product</option>
                                                        <option value="best_selling_product"
                                                            {{ $product->product_selling_type == 'best_selling_product' ? 'selected' : '' }}>
                                                            Best Selling Product</option>
                                                        <option value="latest_collection"
                                                            {{ $product->product_selling_type == 'latest_collection' ? 'selected' : '' }}>
                                                            Latest Collection</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="code">Product Code&nbsp;<span
                                                            class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" name="code"
                                                        value="{{ old('code', $product->code) }}" class="form-control"
                                                        placeholder="Enter product code">
                                                    <span class="require code text-danger"></span>
                                                    @error('code')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="name">Product Name&nbsp;<span
                                                            class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" name="name"
                                                        value="{{ old('name', $product->name) }}" class="form-control"
                                                        placeholder="Enter product name">
                                                    <span class="require name text-danger"></span>
                                                    @error('name')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="short_description">Short Description&nbsp;</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <textarea name="short_description"
                                                        class="form-control">{{ old('short_description', $product->short_description) }}</textarea>
                                                    <span class="require short_description text-danger"></span>
                                                    @error('short_description')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="">Unit <span class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="unit_id" class="form-control">
                                                        <option value="">Select Unit</option>
                                                        @foreach ($units as $unit)
                                                            <option value="{{ $unit->id }}"
                                                                {{ $unit->id == $product->unit_id ? 'selected' : '' }}>
                                                                {{ $unit->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="require unit_id text-danger"></span>
                                                    @error('unit_id')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="price">Price <span class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <input type="hidden" name="currency" value="Rs">
                                                            <span class="input-group-text" id="basic-addon1">Rs</span>
                                                        </div>
                                                        <input type="text" class="form-control"
                                                            value="{{ $product->price }}" name="price" maxlength="10"
                                                            aria-label="price" aria-describedby="basic-addon1"
                                                            onkeypress="return onlynumbers(event);">
                                                        @error('price')
                                                            <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>

                                                    <span class="require price text-danger"></span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="qty">Quantity <span class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" name="qty" class="form-control"
                                                        value="{{ old('qty', $product->qty) }}">
                                                    <span class="require qty text-danger"></span>
                                                    @error('qty')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="discount">Discount&nbsp;(%)</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="text" name="discount" class="form-control"
                                                        value="{{ old('discount', $product->discount) }}">
                                                    <span class="require discount text-danger"></span>
                                                    @error('discount')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="description">Description&nbsp;</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <textarea name="description" class="form-control ckeditor"
                                                        id="ckeditor">{{ old('description', $product->description) }}</textarea>
                                                    <span class="require description text-danger"></span>
                                                    @error('description')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="status">Status <span class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <select name="status" class="form-control select2">
                                                        <option value="1"
                                                            {{ old('status', $product->status) == '1' ? 'selected' : '' }}>
                                                            Active</option>
                                                        <option value="0"
                                                            {{ old('status', $product->status) == '0' ? 'selected' : '' }}>
                                                            Inactive</option>
                                                    </select>
                                                    <span class="require status text-danger"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="image">Upload Product Image (2MB)&nbsp;<span
                                                            class="req">*</span></label>
                                                </div>
                                                <div class="col-md-9">
                                                    <input type="file" class="form-control" name="image"
                                                        onchange="showImg(this, 'imgPreview')">
                                                    <img src="{{ $product->getImg($product->image) }}"
                                                        class="imgSize" id="imgPreview">
                                                    <span class="require image text-danger"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label for="is_featured">Is Featured </label>
                                                    <select name="is_featured" class="form-control select2">
                                                        <option value="1"
                                                            {{ old('is_featured', $product->is_featured) == '1' ? 'selected' : '' }}>
                                                            Yes</option>
                                                        <option value="0"
                                                            {{ old('is_featured', $product->is_featured) == '0' ? 'selected' : '' }}>
                                                            No</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="is_trending">Is Trending </label>
                                                    <select name="is_trending" class="form-control select2">
                                                        <option value="1"
                                                            {{ old('is_trending', $product->is_trending) == '1' ? 'selected' : '' }}>
                                                            Yes</option>
                                                        <option value="0"
                                                            {{ old('is_trending', $product->is_trending) == '0' ? 'selected' : '' }}>
                                                            No</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="is_latest">Is Latest </label>
                                                    <select name="is_latest" class="form-control select2">
                                                        <option value="1"
                                                            {{ old('is_latest', $product->is_latest) == '1' ? 'selected' : '' }}>
                                                            Yes</option>
                                                        <option value="0"
                                                            {{ old('is_latest', $product->is_latest) == '0' ? 'selected' : '' }}>
                                                            No</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label for="is_bestselling">Is Best Selling </label>
                                                    <select name="is_bestselling" class="form-control select2">
                                                        <option value="1"
                                                            {{ old('is_bestselling', $product->is_bestselling) == '1' ? 'selected' : '' }}>
                                                            Yes</option>
                                                        <option value="0"
                                                            {{ old('is_bestselling', $product->is_bestselling) == '0' ? 'selected' : '' }}>
                                                            No</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" class="btn btn-primary"
                                                onclick="submitForm(event);">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $(".alert-warning").css('display', 'none');
        });

        function submitForm(e) {
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#form").attr("action");
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $.ajax({
                url: url,
                type: 'post',
                _method: "put",
                data: new FormData(this.form),
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {
                        location.href = data.redirectRoute;
                        toastr.success(data.msg);
                    }

                }
            });
        }
    </script>
    <script>
        $(document).ready(function(){
            getProductType();
        })
        function getProductType() {
            var id = $("#product_category_id").val();
            // id = id.split("-");
            // id = id[0];
            console.log(id);
            $.ajax({
                url: "{{ route('admin.getproducttype') }}",
                data: {
                    'category_id': id
                },
                type: "POST",
                dataType: "json",
                success: function(res) {
                    $("#product_type_id").empty();
                    if (res) {

                        var options = "";
                        options += "<option value=''>Select Product Type</option>";

                        $.each(res, function(key, name) {
                            valueData = key;
                            var selectedData = "{{ $product->product_type_id }}";
                            var optionselected = selectedData == key ? "selected" : "";
                            options += "<option value=" + valueData + " " + optionselected + ">" +
                                name + "</option>";

                        });
                        $("#product_type_id").html(options);


                    } else {

                        $("#product_type_id").html("<option value=''>Select Product Type</option>");
                    }

                }
            });
        }
    </script>
@endsection
