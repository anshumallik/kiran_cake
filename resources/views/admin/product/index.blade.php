@extends('layouts.admin.app')
@section('title', 'Product')
@section('product', 'active')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Products</span>
                                </a>
                                <a href="{{ route('admin.product.create') }}" class="btn btn-primary float-right">
                                    <i class="fas fa-plus"></i> Add Product
                                </a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {{-- <div class="table-responsive"> --}}
                            <table id="Product" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Image</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th class="hidden" style="width:20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="sortedtable">
                                    @foreach ($products as $key => $product)
                                        <tr data-id="{{ $product->id }}" class="sortRow">
                                            <td style='cursor: pointer;'>{{ ++$id }}</td>
                                            <td style='cursor: pointer;'>
                                                <img src="{{ $product->getImg($product->image) }}" class="imageSize"
                                                    alt="">
                                            </td>
                                            <td style='cursor: pointer;'>{{ $product->code }}</td>
                                            <td style='cursor: pointer;'>{{ $product->name }}</td>
                                            <td style='cursor: pointer;'>{{ $product->category->title }}</td>
                                            <td style='cursor: pointer;'>{{ $product->qty }}</td>
                                            <td style='cursor: pointer;'>{{ $product->price }}</td>
                                            <td>
                                                <?php echo $product->status ? '<span class="cur_sor badge badge-success" onclick="updateStatus(' . $product->id . ',$(this))">Active</span>' : '<span class="badge badge-warning cur_sor" onclick="updateStatus(' . $product->id . ',$(this))">Inactive</span>'; ?>
                                            </td>
                                            <td>
                                                <div class="d-inline-flex">
                                                    <a href="{{ route('admin.product.edit', $product->id) }}"
                                                        class="btn btn-sm btn-primary" title="Edit Product">
                                                        <i class="fa fa-edit iCheck"></i> 
                                                    </a>
                                                    <a href="{{ route('admin.product.show', $product->id) }}"
                                                        class="btn btn-sm btn-primary ml-2" title="View Product">
                                                        <i class="fa fa-eye iCheck"></i> 
                                                    </a>
                                                    <a href="{{ route('admin.productImage.create', $product->id) }}"
                                                        class="btn btn-sm btn-success ml-2" title="Add Product Image">
                                                        <i class="fa fa-plus iCheck"></i> 
                                                    </a>
                                                    <form action="{{ route('admin.product.delete', $product->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method("DELETE")
                                                        <button class="btn btn-danger btn-sm ml-2" type="submit"><i
                                                                class="fa fa-trash iCheck"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- </div> --}}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

    <script>
        $(document).ready(function() {
            $("#Product").DataTable({
                "responsive": false,
                "lengthChange": true,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'csv',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'excel',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],
                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });
        $("#sortedtable").sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                console.log('updated');
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {
            var id = $('tr.sortRow').attr('data-id');
            console.log(id);
            var order = [];
            $('tr.sortRow').each(function(index, element) {
                order.push({
                    id: $(this).attr('data-id'),
                    position: index + 1
                });
            });
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ route('admin.product.updateOrder') }}",
                data: {
                    order: order,
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == "Success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });
        }

        function updateStatus(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.product.updateStatus') }}",
                    type: 'POST',
                    data: {
                        'product_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Inactive');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Active');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }
    </script>
@endsection
