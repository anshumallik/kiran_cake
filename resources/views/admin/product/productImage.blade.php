@extends('layouts.admin.app')
@section('title', 'Product')
@section('product', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">Product Image</span>
                                </a>
                                <a href="{{ route('admin.product.index') }}" class="float-right btn btn-primary">
                                    <i class="fa fa-arrow-left iCheck"></i>&nbsp;Back to List
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.productImage.store') }}" method="POST"
                                enctype="multipart/form-data" id="form">
                                @csrf
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <p class="db_error"></p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <div class="input-field">
                                            <label class="active">Add Product Images</label>
                                            <div class="input-images" style="padding-top: .5rem;"></div>
                                        </div>
                                        <span class="require images text-danger"></span>

                                    </div>
                                    <div class="form-group text-center">
                                        <button type="button" class="btn btn-primary"
                                            onclick="submitForm(event);">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            // console.log('hey')
            $('.input-images').imageUploader();

        });
        $(document).ready(function() {
            $(".alert-warning").css('display', 'none');
        });

        function submitForm(e) {
            
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#form").attr("action");
            
            console.log(url);
            // return 1;
            $.ajax({
                url: url,
                type: 'post',
                data: new FormData(this.form),
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    // console.log(data);
                    // return 1;
                    if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {
                        location.href = data.redirectRoute;
                        toastr.success(data.msg);
                    }

                }
            });
            
        }
    </script>
@endsection
