@extends('layouts.admin.app')
@section('title', 'Brand')
@section('brand', 'active')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Brands</span>
                                </a>
                                <a href="{{ route('admin.brand.create') }}" class="btn btn-primary float-right">
                                    <i class="fas fa-plus"></i> Add Brand
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="Brand" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Image</th>
                                        <th>Url</th>

                                        <th>Status</th>
                                        <th class="hidden" style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="sortedtable">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#Brand').DataTable({
                'createdRow': function(row, data, dataIndex) {
                    $(row).addClass('sortRow');
                    $(row).attr('data-id', data.i);
                },
                "processing": true,
                "serverSide": true,
                "responsive": false,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],

                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                },
                "ajax": {
                    "url": "{{ route('admin.brand.allBrands') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{ csrf_token() }}"
                    }
                },
                "columns": [{
                        "data": "id"
                    },
                    {
                        "data": "logo"
                    },
                    {
                        "data": "url"
                    },

                    {
                        "data": "status"
                    },
                    {
                        "data": "action"
                    },
                ]

            });
            dataTablePosition();
        });


        $("#sortedtable").sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                console.log('updated');
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {
            var id = $('tr.sortRow').attr('data-id');
            console.log(id);
            var order = [];
            $('tr.sortRow').each(function(index, element) {
                order.push({
                    id: $(this).attr('data-id'),
                    position: index + 1
                });
            });
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ route('admin.brand.updateOrder') }}",
                data: {
                    order: order,
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    $("#Brand").DataTable().ajax.reload();
                    console.log(response);
                    if (response.status == "Success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });
        }

        function updateStatus(id) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.brand.updateStatus') }}",
                    type: 'POST',
                    data: {
                        'brand_id': id
                    },
                    success: function(data) {
                        $("#Brand").DataTable().ajax.reload();
                        toastr.success(data.msg);
                    }
                });
            }
        }

        function deleteBrand(id) {
            if (id) {
                $.ajax({
                    url: '{{ route('admin.brand.deleteBrand') }}',
                    type: 'POST',
                    data: {
                        'brand_id': id
                    },
                    success: function(data) {
                        $("#Brand").DataTable().ajax.reload();
                        toastr.error(data.msg);
                    }
                });
            }
        }
    </script>
@endsection
