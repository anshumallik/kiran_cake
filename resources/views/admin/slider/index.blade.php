@extends('layouts.admin.app')
@section('title', 'Slider')
@section('slider', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Sliders</span>
                                </a>
                                <a href="{{ route('admin.slider.create') }}" class="btn btn-primary float-right">
                                    <i class="fas fa-plus"></i> Add Slider
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="Slider" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Image</th>
                                        <th>Caption</th>
                                        <th>Link</th>
                                        <th>Status</th>
                                        <th class="hidden">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sliders as $key => $slider)
                                        <tr>
                                            <td>{{ ++$id }}</td>
                                            <td>
                                                <img src="{{ $slider->getImg($slider->image) }}" class="imageSize"
                                                    alt="">
                                            </td>
                                            <td>
                                                @if ($slider->caption)
                                                    {{ $slider->caption }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>{{ $slider->link }}</td>

                                            <td>

                                                <?php echo $slider->status ? '<span class="cur_sor badge badge-success" onclick="updateStatus(' . $slider->id . ',$(this))">Active</span>' : '<span class="badge badge-warning cur_sor" onclick="updateStatus(' . $slider->id . ',$(this))">Inactive</span>'; ?>


                                            </td>
                                            <td>
                                                <div class="d-inline-flex">

                                                    <a href="{{ route('admin.slider.edit', $slider->id) }}"
                                                        class="btn btn-primary btn-sm" title="Edit Slider">
                                                        <i class="fa fa-edit iCheck"></i>
                                                    </a>

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#Slider").DataTable({
                "responsive": false,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],

                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });

        function updateStatus(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.slider.updateStatus') }}",
                    type: 'POST',
                    data: {
                        'slider_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Inactive');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Active');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }
    </script>
@endsection
