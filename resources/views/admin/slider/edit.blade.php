@extends('layouts.admin.app')
@section('title', 'Slider')
@section('slider', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">Edit Slider</span>
                                </a>
                                <a href="{{ route('admin.slider.index') }}" class="btn btn-primary float-right">
                                    <i class="fa fa-arrow-left"></i> Back to List
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.slider.update', $slider->id) }}" method="POST"
                                enctype="multipart/form-data" id="form">
                                @csrf
                                @method('put')
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <p class="db_error"></p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label for="caption">Caption</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" name="caption"
                                                            class="form-control @error('caption') is-invalid @enderror"
                                                            placeholder="Enter caption" id=""
                                                            value="{{ $slider->caption }}">
                                                        <span class="require caption text-danger"></span>
                                                        @error('caption')
                                                            <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label for="link">Link</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" name="link"
                                                            class="form-control @error('link') is-invalid @enderror"
                                                            placeholder="Enter link" id=""
                                                            value="{{ $slider->link }}">
                                                        <span class="require link text-danger"></span>
                                                        @error('link')
                                                            <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label for="status">Status &nbsp;<span
                                                                class="req">*</span></label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <select name="status"
                                                            class="form-control select2 @error('status') is-invalid @enderror"
                                                            id="status" style="width: 100%">
                                                            <option value="1"
                                                                {{ $slider->status == '1' ? 'selected' : '' }}>
                                                                Active</option>
                                                            <option value="0"
                                                                {{ $slider->status == '0' ? 'selected' : '' }}>
                                                                Inactive</option>
                                                        </select>
                                                        <span class="require status text-danger"></span>
                                                        @error('status')
                                                            <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label for="image">Upload Image (1600*600)</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="file" name="image" id="img"
                                                            onchange="showImg(this, 'imgPreview')"
                                                            class="form-control @error('image') is-invalid @enderror">
                                                        <img src="{{ $slider->getImg($slider->image) }}" id="imgPreview"
                                                            class="imgSize" alt="">
                                                        <span class="require image text-danger"></span>
                                                        @error('image')
                                                            <span class="text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="form-group text-center">
                                    <button type="button" onclick="submitForm(event);"
                                        class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $(".alert-warning").css('display', 'none');
        });

        function submitForm(e) {
            e.preventDefault();
            $('.require').css('display', 'none');
            let url = $("#form").attr("action");
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            $.ajax({
                url: url,
                type: 'post',
                _method: "put",
                data: new FormData(this.form),
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    if (data.db_error) {
                        $(".alert-warning").css('display', 'block');
                        $(".db_error").html(data.db_error);
                    } else if (data.errors) {
                        var error_html = "";
                        $.each(data.errors, function(key, value) {
                            error_html = '<div>' + value + '</div>';
                            $('.' + key).css('display', 'block').html(error_html);
                        });
                    } else if (!data.errors && !data.db_error) {
                        location.href = data.redirectRoute;
                        toastr.success(data.msg);
                    }

                }
            });
        }
    </script>
@endsection
