@extends('layouts.admin.app')
@section('title', 'Dashboard')
@section('dashboard', 'active')
@section('content')
    <style>
        table td,
        th {
            padding: 15px 15px !important;
        }

        .color {
            background-color: red;
            color: #fff;
        }


        .bgcolor {
            background-color: #d00000;
            color: #fff;
            padding: 8px;
        }

        .bgcolor1 {
            background-color: #00b70c;
            color: #fff;
            padding: 8px;
        }

        .bstyle h5 {
            margin-bottom: 0;
            text-align: center;
            font-size: 16px;
            line-height: 1;
        }

        .paddingcol .col-md-2 {
            padding-right: 5px;
            padding-left: 5px;
        }

        .design {
            height: 30px;
            width: 30px;
        }

        .main {
            padding: 30px 40px 30px 30px;
        }

        .main1 {
            padding: 30px;
        }

    </style>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card create-card">
                        <div class="card-body">
                            <button class="btn btn-primary">Dashboard</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="content content-style">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header bg-transparent mb-3"><i class="fas fa-chart-bar mr-3"
                                    style="color: #10c12e"></i>Weekly
                                Visits
                            </div>
                            <div style="height: 100%; width: 100%">
                                <canvas id="visitChart" width="200" height="82"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    @if (!empty($totalVisit))
                        <div class="info-box rounded-0 bg-yellow">
                            <span class="info-box-icon elevation-1"><i class="fas fa-chart-bar"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Visits</span>
                                <span class="info-box-number">{{ $totalVisit }}</span>
                            </div>
                        </div>
                    @endif
                    @if (!empty($totalProduct))
                        <a href="{{ route('admin.product.index') }}">
                            <div class="info-box rounded-0 bg-green">
                                <span class="info-box-icon elevation-1"><i class="fab fa-product-hunt"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Products</span>
                                    <span class="info-box-number">{{ $totalProduct }}</span>
                                </div>

                            </div>

                        </a>
                    @endif
                    <a href="#">
                        <div class="info-box rounded-0 bg-red">
                            <span class="info-box-icon elevation-1"><i class="fas fa-shopping-cart"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Order</span>
                                <span class="info-box-number">1</span>
                            </div>
                        </div>
                    </a>
                    @if (!empty($totalCategory))
                        <a href="{{ route('admin.productcategory.index') }}">
                            <div class="info-box rounded-0 bg-info">
                                <span class="info-box-icon elevation-1"><i class="fas fa-project-diagram"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Total Categories</span>
                                    <span class="info-box-number">{{ $totalCategory }}</span>
                                </div>
                            </div>
                        </a>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header bg-transparent mb-3"><i class="fas fa-chart-bar mr-3"
                                    style="color: #10c12e"></i>Monthly
                                Visits
                            </div>
                            <div style="height: 100%; width: 100%">
                                <canvas id="monthlyVisitChart" width="200" height="70"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            let url = "{{ route('weeklyvisits') }}";
            var Date = new Array();
            var Count = new Array();
            $.get(url, function(response) {
                $.each(response, function(key, value) {
                    console.log(key + '=' + value);
                    Date.push(key);
                    Count.push(value);
                });
                var ctx = document.getElementById('visitChart').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: Date,
                        datasets: [{
                            label: 'Total Visits',
                            data: Count,

                            backgroundColor: [
                                'rgba(255, 215, 0, 0.3)',

                            ],
                            borderColor: [
                                'rgba(253, 170, 53, 0.5)',
                            ],
                            borderWidth: 2
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            let url = "{{ route('monthlyvisits') }}";
            let year;
            let Datee = new Array();
            let monthName = new Array();
            let Count = new Array();
            $.get(url, function(response) {
                $.each(response, function(key, value) {
                    // console.log(key + '=' + value);
                    let date = new Date(key);
                    monthName.push(monthNames[date.getMonth()] + ' ' + date.getDate());
                    year = date.getFullYear();
                    Datee.push(key);
                    Count.push(value);
                });
                var ctx = document.getElementById('monthlyVisitChart');
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: monthName,
                        datasets: [{
                            label: 'Total Visits',
                            data: Count,
                            backgroundColor: [
                                'rgba(0, 181, 204, 0.3)',

                            ],
                            borderColor: [
                                'rgba(0, 181, 204, 0.5)',

                            ],
                            borderWidth: 2
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            })
        });
    </script>
@endsection
