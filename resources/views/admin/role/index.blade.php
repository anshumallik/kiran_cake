@extends('layouts.admin.app')
@section('title', 'Role')
@section('role', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Roles</span>
                                </a>
                                <a href="{{ route('admin.role.create') }}" class="btn btn-primary float-right">
                                    <i class="fas fa-plus"></i> Add Role
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="Role" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Permissions</th>
                                        <th class="hidden">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $role)
                                        <tr>
                                            <td>{{ ++$id }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>{{ $role->slug }}</td>
                                            <td>
                                                @foreach ($role->permissions as $permission)
                                                    {{-- {{ $loop->first ? '' : ',' }} --}}
                                                    <span class="badge badge-success">{{ $permission->name }}</span>
                                                @endforeach
                                            </td>
                                            <td>
                                                {{-- @if ($role->name != 'Superuser') --}}
                                                    <div class="d-inline-flex">
                                                        <a href="{{ route('admin.role.edit', $role->id) }}"
                                                            class="btn btn-primary btn-sm" title="Edit Role">
                                                           <i class="fa fa-edit iCheck"></i>
                                                        </a>
                                                    </div>
                                                {{-- @endif --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#Role").DataTable({
                "responsive": false,
                "autoWidth": false,

                "processing": true,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],

                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });
    </script>
@endsection
