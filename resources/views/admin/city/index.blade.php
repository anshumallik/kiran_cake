@extends('layouts.admin.app')
@section('title', 'City')
@section('city', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Cities</span>
                                </a>
                                <a href="{{ route('admin.city.create') }}" class="btn btn-primary float-right">
                                    <i class="fas fa-plus"></i> Add City
                                </a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">

                            <table id="City" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Province</th>
                                        <th>City</th>
                                        <th>Status</th>
                                        <th class="hidden">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="sortedtable">
                                    @foreach ($cities as $key => $city)
                                        <tr data-id="{{ $city->id }}" class="sortRow">
                                            <td style='cursor: pointer;'>{{ ++$id }}</td>
                                            <td style='cursor: pointer;'>{{ $city->province->title }}</td>
                                            <td style='cursor: pointer;'>{{ $city->title }}</td>
                                            <td>
                                                <?php echo $city->status ? '<span class="cur_sor badge badge-success" onclick="updateStatus(' . $city->id . ',$(this))">Active</span>' : '<span class="badge badge-warning cur_sor" onclick="updateStatus(' . $city->id . ',$(this))">Inactive</span>'; ?>
                                            </td>
                                            <td>
                                                <div class="d-inline-flex">
                                                    <a href="{{ route('admin.city.edit', $city->id) }}"
                                                        class="btn btn-sm btn-primary" title="Edit Province">
                                                        <i class="fa fa-edit iCheck"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

    <script>
        $(document).ready(function() {
            $("#City").DataTable({
                "responsive": false,
                "lengthChange": true,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'csv',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'excel',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],
                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });

        $("#sortedtable").sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                console.log('updated');
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {
            var id = $('tr.sortRow').attr('data-id');
            console.log(id);
            var order = [];
            $('tr.sortRow').each(function(index, element) {
                order.push({
                    id: $(this).attr('data-id'),
                    position: index + 1
                });
            });
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ route('admin.city.updateOrder') }}",
                data: {
                    order: order,
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == "Success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });
        }

        function updateStatus(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.city.updateStatus') }}",
                    type: 'POST',
                    data: {
                        'city_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Inactive');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Active');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }
    </script>
@endsection
