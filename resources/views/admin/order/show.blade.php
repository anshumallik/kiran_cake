@extends('layouts.admin.app')
@section('title', 'Order')
@section('order', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">Order Detail</span>
                                </a>
                                <a href="{{ route('admin.order.index') }}" class="float-right btn btn-primary">
                                    <i class="fa fa-arrow-left iCheck"></i>&nbsp;Back to List
                                </a>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="col-md-12 mb-5">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $order->name }}
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="code">Phone&nbsp;</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $order->phone }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="name">Province&nbsp;</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $order->province->title }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="name">City&nbsp;</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $order->city->title }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="name">Address&nbsp;</label>
                                            </div>
                                            <div class="col-md-9">
                                                {{ $order->address }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="status">Status </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($order->status == 1)
                                                    Active
                                                @elseif($order->status == 0)
                                                    Inactive
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="verify_status">Is Vefified </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($order->verify_status == 1)
                                                    Yes
                                                @elseif($order->verify_status == 0)
                                                    No
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="shipping_status">Is Shipped </label>
                                            </div>
                                            <div class="col-md-9">
                                                @if ($order->shipping_status == 1)
                                                    Yes
                                                @elseif($order->shipping_status == 0)
                                                    No
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            @if ($order->order_items)
                                <div class="col-md-12">
                                    <div class="mt-4 mb-5">
                                        <a>
                                            <span class="content-header"><strong>Order Items</strong></span>
                                        </a>

                                    </div>
                                    <table id="ProductImage" class="table table-responsive-xl text-center">
                                        <thead>
                                            <tr>
                                                <th>S.N</th>
                                                <th>Product Name</th>
                                                <th>Image</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Subtotal</th>
                                                <th class="hidden">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $id = 0;
                                            ?>
                                            @foreach ($order->order_items as $key => $order_item)
                                                <tr>
                                                    <td>{{ ++$id }}</td>
                                                    <td>{{ $order_item->product->name }}</td>
                                                    <td>
                                                        <img src="{{ $order_item->getImg($order_item->product->image) }}"
                                                            class="imageSize" alt="">
                                                    </td>

                                                    <td>

                                                        {{ $order_item->price }}


                                                    </td>
                                                    <td>

                                                        {{ $order_item->quantity }}


                                                    </td>
                                                    <td>
                                                        <div class="d-inline-flex">



                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            @endif
                            {{-- @endif --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')

    <script>
        $(document).ready(function() {
            $("#ProductImage").DataTable({
                "responsive": false,
                "lengthChange": true,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'csv',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'excel',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],
                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });
    </script>
@endsection
