@extends('layouts.admin.app')
@section('title', 'Order')
@section('order', 'active')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Orders</span>
                                </a>

                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {{-- <div class="table-responsive"> --}}
                            <table id="Order" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>City</th>
                                        <th>Address</th>
                                        <th>Order Time</th>
                                        <th>Verification Status</th>
                                        <th>Shipping Status</th>
                                        <th>Status</th>
                                        <th class="hidden" style="width:1%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="sortedtable">
                                    @foreach ($orders as $key => $order)
                                        <tr data-id="{{ $order->id }}" class="sortRow">
                                            <td style='cursor: pointer;'>{{ ++$id }}</td>
                                            <td style='cursor: pointer;'>{{ $order->name }}</td>
                                            <td style='cursor: pointer;'>{{ $order->phone }}</td>
                                            <td style='cursor: pointer;'>{{ $order->city->title }}</td>
                                            <td style='cursor: pointer;'>{{ $order->address }}</td>
                                            <td>
                                                <?php echo $order->verify_status ? '<span class="cur_sor badge badge-success" onclick="verifyOrder(' . $order->id . ',$(this))">Verified</span>' : '<span class="badge badge-warning cur_sor" onclick="verifyOrder(' . $order->id . ',$(this))">Inverified</span>'; ?>
                                            </td>
                                            <td>
                                                <?php echo $order->shipping_status ? '<span class="cur_sor badge badge-success" onclick="verifyShipping(' . $order->id . ',$(this))">Shipped</span>' : '<span class="badge badge-warning cur_sor" onclick="verifyShipping(' . $order->id . ',$(this))">Unshipped</span>'; ?>
                                            </td>

                                            <td>
                                                <?php echo $order->status ? '<span class="cur_sor badge badge-success" onclick="updateStatus(' . $order->id . ',$(this))">Active</span>' : '<span class="badge badge-warning cur_sor" onclick="updateStatus(' . $order->id . ',$(this))">Inactive</span>'; ?>
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($order['created_at'])->diffForHumans(null, null, null, 2) }}
                                            </td>
                                            <td>
                                                <div class="d-inline-flex">

                                                    <a href="{{ route('admin.order.show', $order->id) }}"
                                                        class="btn btn-sm btn-primary ml-2" title="View order">
                                                        <i class="fa fa-eye iCheck"></i>
                                                    </a>

                                                    <form action="{{ route('admin.order.delete', $order->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method("DELETE")
                                                        <button class="btn btn-danger btn-sm ml-2" type="submit"><i
                                                                class="fa fa-trash iCheck"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- </div> --}}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

    <script>
        $(document).ready(function() {
            $("#Order").DataTable({
                "responsive": false,
                "lengthChange": true,
                "autoWidth": false,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'csv',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'excel',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],
                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });
        $("#sortedtable").sortable({
            items: "tr",
            cursor: 'move',
            opacity: 0.6,
            update: function() {
                console.log('updated');
                sendOrderToServer();
            }
        });

        function sendOrderToServer() {
            var id = $('tr.sortRow').attr('data-id');
            console.log(id);
            var order = [];
            $('tr.sortRow').each(function(index, element) {
                order.push({
                    id: $(this).attr('data-id'),
                    position: index + 1
                });
            });
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ route('admin.product.updateOrder') }}",
                data: {
                    order: order,
                    _token: '{{ csrf_token() }}'
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == "Success") {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            });
        }

        function updateStatus(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.order.updateStatus') }}",
                    type: 'POST',
                    data: {
                        'order_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Inactive');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Active');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }

        function verifyOrder(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.order.verifyOrder') }}",
                    type: 'POST',
                    data: {
                        'order_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Unverified');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Verified');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }

        function verifyShipping(id, el) {
            if (id) {
                $.ajax({
                    url: "{{ route('admin.order.verifyShipping') }}",
                    type: 'POST',
                    data: {
                        'order_id': id
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            el.text('Unshipped');
                            el.removeClass('badge-success').addClass('badge-warning');
                        } else if (data.status == 1) {
                            el.text('Shipped');
                            el.removeClass('badge-warning').addClass('badge-success');
                        }
                        toastr.success(data.msg);
                    }
                });
            }
        }
    </script>
@endsection
