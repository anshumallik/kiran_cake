@extends('layouts.admin.app')
@section('title', 'Cart')
@section('cart', 'active')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a>
                                    <span class="content-header">All Cart Items</span>
                                </a>

                            </div>
                        </div>
                        <div class="card-body">
                            <table id="Cart" class="table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>User</th>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th class="hidden">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cart_items as $key => $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            
                                                <td>
                                                    {{ $data->user_id }}
                                                </td>
                                            
                                            <td>{{ $data->products->name }}</td>
                                            <td>{{ $data->qty }}</td>
                                            <td>{{ $data->price }}</td>
                                            <td>
                                                <div class="d-inline-flex">
                                                    <form action="{{ route('admin.cart.destroy', $data->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-sm btn-danger btn-sm" type="submit"><i
                                                                class="fa fa-trash"></i></button>
                                                    </form>


                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $("#Cart").DataTable({
                "responsive": false,
                "autoWidth": false,
                "processing": true,
                "dom": 'lBfrtip',
                "buttons": [{
                        extend: 'collection',
                        text: "<i class='fa fa-ellipsis-v'></i>",
                        buttons: [{
                                extend: 'copy',
                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'pdf',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                }
                            },
                            {
                                extend: 'print',

                                exportOptions: {
                                    columns: 'th:not(:last-child)'
                                },

                            },
                        ],

                    },
                    {
                        extend: 'colvis',
                        columns: ':not(.hidden)'
                    }
                ],

                "language": {
                    "infoEmpty": "No entries to show",
                    "emptyTable": "No data available",
                    "zeroRecords": "No records to display",
                }
            });
            dataTablePosition();
        });
    </script>
@endsection
