<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block mr-5">
            <a class="nav-link">@yield('title')</a>
        </li>
        <li class="nav-item">
            <span class="nav-link" id="display_time"></span>
        </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

       <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false" v-pre>
                <img class="img-circle elevation-2 img-size-32 mr-1 ml-3"
                    src="{{ asset('images/admin/avatar/' . getUser()->avatar) }}" alt="Admin">
                {{ getUser()->name }}<span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <div class="profile-sidebar">
                    <div class="profile-userpic justify-content-center text-center">
                        <img class="img-responsive" src="{{ asset('images/admin/avatar/' . getUser()->avatar) }}"
                            alt="">
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{ getUser()->name }}
                        </div>
                        <div class="profile-usertitle-job">
                            <i class="fa fa-map-marker mr-2"> {{ getUser()->address }}</i>
                        </div>
                    </div>

                    <div class="profile-userbuttons d-flex">
                        <button type="button" class="btn btn-success btn-sm rounded-0"><a
                                href="{{ route('admin.user.profile') }}">
                                User Profile
                            </a></button>
                        <button type="button" class="btn btn-danger btn-sm rounded-0">
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>
                                Logout
                            </a>
                        </button>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>

                </div>
            </div>
             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a href="{{ route('admin.user.profile') }}" class="dropdown-item">
                    <i class="far fa-user-circle nav-icon"></i> User Profile
                </a>
                <a class="dropdown-item" href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i>
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div> 
         </li>  
    </ul>
</nav>
