<li class="nav-item">
    <a href="{{ route('admin.user.index') }}" class="nav-link @yield('user')">
        <i class="nav-icon fas fa-user-plus iCheck"></i>
        <p>Users</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.permission.index') }}" class="nav-link @yield(" permission")">
        <i class="nav-icon fa fa-circle iCheck"></i>
        <p>Permissions</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.role.index') }}" class="nav-link @yield(" role")">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>Roles</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('admin.unit.index') }}" class="nav-link @yield(" unit")">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>Product Unit</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.brand.index') }}" class="nav-link @yield(" brand")">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>Our Brands</p>
    </a>
</li>
<li class="nav-item {{ $province || $city ? $menu : '' }}">
    <a href="#" class="nav-link {{ $province || $city ? $active : '' }}">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>
            Address
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('admin.province.index') }}" class="nav-link @yield('province')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Province</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.city.index') }}" class="nav-link @yield('city')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>City</p>
            </a>
        </li>

    </ul>
</li>

<li class="nav-item {{ $order || $vorder ? $menu : '' }}">
    <a href="#" class="nav-link {{ $order || $vorder ? $active : '' }}">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>
            Manage Order
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('admin.order.index') }}" class="nav-link @yield('order')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>All Orders</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="" class="nav-link @yield('verified-order')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Verified Orders</p>
            </a>
        </li>

    </ul>
</li>
<li class="nav-item {{ $productcategory || $product || $productType || $productRating ? $menu : '' }}">
    <a href="#" class="nav-link {{ $productcategory || $product || $productType || $productRating ? $active : '' }}">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>
            Product
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('admin.productcategory.index') }}" class="nav-link @yield('product-category')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Product Category</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.product-type.index') }}" class="nav-link @yield('product-type')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Product Type</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.product.index') }}" class="nav-link @yield('product')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Product</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.productrating.index') }}" class="nav-link @yield('product-rating')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Product Rating</p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="{{ route('admin.contact.index') }}" class="nav-link @yield(" contact")">
        <i class="nav-icon fa fa-hand-point-right iCheck"></i>
        <p>User's Query</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.service.index') }}" class="nav-link @yield(" service")">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>Services</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.cart.index') }}" class="nav-link @yield(" cart")">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>Cart Items</p>
    </a>
</li>
<li class="nav-item {{ $about || $general || $socialNav || $sliderNav ? $menu : '' }}">
    <a href="#" class="nav-link {{ $about || $general || $socialNav || $sliderNav ? $active : '' }}">
        <i class="nav-icon fa fa-th-list iCheck"></i>
        <p>
            General Settings
            <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="{{ route('admin.privacypolicy.index') }}" class="nav-link @yield(" privacy-policy")">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>Privacy Policy</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.about.index') }}" class="nav-link @yield('about')">
                <i class="nav-icon far fa-circle iCheck"></i>
                <p>About</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.general.index') }}" class="nav-link @yield('general-setting')">
                <i class="nav-icon fa fa-building iCheck"></i>
                <p>Company Information</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.social.index') }}" class="nav-link @yield('social')">
                <i class="nav-icon fa fa-share-alt iCheck"></i>
                <p>Socials</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.slider.index') }}" class="nav-link @yield('slider')">
                <i class="nav-icon fa fa-image iCheck"></i>
                <p>slider</p>
            </a>
        </li>
    </ul>
</li>
