<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
$menu = 'menu-open';
$active = 'active menu-open';
$client = Request::is('admin/client*');
$province = Request::is('admin/province*');
$city = Request::is('admin/city*');
$order = Request::is('admin/order*');
$vorder = Request::is('admin/verifiedorders*');
$productcategory = Request::is('admin/productcategory*');
$productRating = Request::is('admin/productrating*');
$productType = Request::is('admin/product-type*');
$product = Request::is('admin/product*');
$about = Request::is('admin/about*');
$general = Request::is('admin/company-information*');
$socialNav = Request::is('admin/social*');
$sliderNav = Request::is('admin/slider*');
$company_data = App\Models\CompanyData::first();
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="text-center">
        <a href="{{ route('admin.dashboard') }}" class="brand-link">
            <img src="{{ $company_data->companyImage($company_data->company_logo) }}" alt="AdminLTE Logo"
                class="brand-image">
        </a>
    </div>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image1 my-auto">
                <img src="{{ asset('images/admin/avatar/' . getUser()->avatar) }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('admin.user.profile') }}" class="d-block"> {{ getUser()->name }} </a>

                <div>
                    <small class="designation text-muted">
                        {{ getUser()->roles()->first()->name }}
                        <i class="fa fa-circle text-success"></i>
                    </small>

                </div>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link @yield('dashboard')">
                        <i class="nav-icon fas fa-tachometer-alt iCheck"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @include('layouts.admin.sidebar.allnav')
            </ul>
        </nav>
    <!-- /.sidebar-menu -->
    </div>
</aside>
