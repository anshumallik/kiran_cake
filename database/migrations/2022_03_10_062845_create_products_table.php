<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_type_id')->nullable();
            $table->unsignedBigInteger('product_category_id')->nullable();
            $table->unsignedBigInteger('unit_id')->nullable();
            $table->string('name')->unique();
            $table->string('slug')->nullable();
            $table->string('code')->unique();
            $table->integer('qty')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->string('discount')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->tinyInteger('is_trending')->default(0)->nullable();
            $table->tinyInteger('is_latest')->default(0)->nullable();
            $table->tinyInteger('is_featured')->default(0)->nullable();
            $table->tinyInteger('is_bestselling')->default(0)->nullable();
            $table->tinyInteger('order')->nullable();
            $table->string('currency')->default('Rs.');
            $table->string('image')->nullable()->default('default.png');
            $table->longText('description')->nullable();
            $table->longText('short_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
