<?php

use App\Http\Controllers\Admin\AboutController as AdminAboutController;
use App\Http\Controllers\Admin\BrandController as AdminBrandController;
use App\Http\Controllers\Admin\CartController as AdminCartController;
use App\Http\Controllers\Admin\CityController as AdminCityController;
use App\Http\Controllers\Admin\CompanyDataController as AdminCompanyDataController;
use App\Http\Controllers\Admin\ContactController as AdminContactController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\PermissionController as AdminPermissionController;
use App\Http\Controllers\Admin\PrivacyPolicyController as AdminPrivacyPolicyController;
use App\Http\Controllers\Admin\ProductCategoryController as AdminProductCategoryController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\ProductImageController as AdminProductImageController;
use App\Http\Controllers\Admin\ProductRatingController as AdminProductRatingController;
use App\Http\Controllers\Admin\ProductTypeController as AdminProductTypeController;
use App\Http\Controllers\Admin\ProvinceController as AdminProvinceController;
use App\Http\Controllers\Admin\RoleController as AdminRoleController;
use App\Http\Controllers\Admin\ServiceController as AdminServiceController;
use App\Http\Controllers\Admin\SliderController as AdminSliderController;
use App\Http\Controllers\Admin\SocialController as AdminSocialController;
use App\Http\Controllers\Admin\UnitController as AdminUnitController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Frontend\FrontendController as FrontendController;
use App\Http\Controllers\Frontend\OrderController as OrderController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes(['register' => true]);

Route::group(["middleware" => ["auth", 'checkPermission']], function () {
    Route::group(["prefix" => "admin/", "as" => "admin."], function () {
        Route::get("dashboard", [AdminDashboardController::class, "dashboard"])->name("dashboard");

        Route::group(["prefix" => "permission/", "as" => "permission."], function () {
            Route::get("", [AdminPermissionController::class, "index"])->name("index");
            Route::get('create', [AdminPermissionController::class, "create"])->name('create');
            Route::post("store", [AdminPermissionController::class, "store"])->name("store");
        });
        Route::group(["prefix" => "role/", "as" => "role."], function () {
            Route::get("", [AdminRoleController::class, 'index'])->name("index");
            Route::get("create", [AdminRoleController::class, 'create'])->name("create");
            Route::post("store", [AdminRoleController::class, 'store'])->name('store');
            Route::get("edit/{id}", [AdminRoleController::class, 'edit'])->name('edit');
            Route::match(["put", "patch"], "update/{id}", [AdminRoleController::class, "update"])->name('update');
        });
        Route::group(["prefix" => "user/", "as" => "user."], function () {
            Route::get("", [AdminUserController::class, 'index'])->name('index');
            Route::get("create", [AdminUserController::class, 'create'])->name('create');
            Route::post("store", [AdminUserController::class, 'store'])->name('store');
            Route::get("edit/{id}", [AdminUserController::class, 'edit'])->name('edit');
            Route::match(['put', "patch"], "update/{id}", [AdminUserController::class, 'update'])->name('update');
            Route::post("update-status", [AdminUserController::class, "updateStatus"])->name("updateStatus");

            Route::get("profile/", [AdminUserController::class, "profile"])->name("profile");
            Route::post("change-admin-password", [AdminUserController::class, "adminNewPassword"])->name("adminNewPassword");
            Route::post("change-admin-email", [AdminUserController::class, "changeAdminEmail"])->name("changeAdminEmail");
            Route::post("change-admin-profile", [AdminUserController::class, "changeAdminAvatar"])->name("changeAdminAvatar");

        });
        Route::group(['prefix' => 'company-information/', 'as' => 'general.'], function () {
            Route::get('', [AdminCompanyDataController::class, 'index'])->name('index');
            Route::post('store', [AdminCompanyDataController::class, 'store'])->name('store');
            Route::post('update/{id}', [AdminCompanyDataController::class, 'update'])->name('update');
            Route::delete('delete/{id}', [AdminCompanyDataController::class, 'destroy'])->name('destroy');
        });
        Route::group(["prefix" => "productcategory/", "as" => "productcategory."], function () {
            Route::get("", [AdminProductCategoryController::class, "index"])->name("index");
            Route::get("create", [AdminProductCategoryController::class, "create"])->name("create");
            Route::post("store", [AdminProductCategoryController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminProductCategoryController::class, "edit"])->name("edit");
            Route::put("update/{id}", [AdminProductCategoryController::class, "update"])->name("update");
            Route::post('productcategories/sortedDatatable', [AdminProductCategoryController::class, "updateOrder"])->name('updateOrder');
            Route::post("update-status", [AdminProductCategoryController::class, "updateStatus"])->name("updateStatus");

        });

        Route::group(["prefix" => "product-type/", "as" => "product-type."], function () {
            Route::get("", [AdminProductTypeController::class, "index"])->name("index");
            Route::get("create", [AdminProductTypeController::class, "create"])->name("create");
            Route::post("store", [AdminProductTypeController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminProductTypeController::class, "edit"])->name("edit");
            Route::put("update/{id}", [AdminProductTypeController::class, "update"])->name("update");
            Route::post('product-types/sortedDatatable', [AdminProductTypeController::class, "updateOrder"])->name('updateOrder');
            Route::post("update-status", [AdminProductTypeController::class, "updateStatus"])->name("updateStatus");

        });
        Route::group(["prefix" => "social/", "as" => "social."], function () {
            Route::get("", [AdminSocialController::class, "index"])->name("index");
            Route::get("create/", [AdminSocialController::class, "create"])->name("create");
            Route::post("store/", [AdminSocialController::class, "store"])->name("store");
            Route::get("edit/", [AdminSocialController::class, "edit"])->name("edit");
            Route::put("update/", [AdminSocialController::class, "update"])->name("update");
        });
        Route::group(["prefix" => "slider/", "as" => "slider."], function () {
            Route::get("", [AdminSliderController::class, "index"])->name("index");
            Route::get("create", [AdminSliderController::class, "create"])->name("create");
            Route::post("store", [AdminSliderController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminSliderController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminSliderController::class, "update"])->name("update");
            Route::post("update-status", [AdminSliderController::class, "updateStatus"])->name("updateStatus");
        });
        Route::group(["prefix" => "unit/", "as" => "unit."], function () {
            Route::get("", [AdminUnitController::class, "index"])->name("index");
            Route::get("create", [AdminUnitController::class, "create"])->name("create");
            Route::post("store", [AdminUnitController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminUnitController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminUnitController::class, "update"])->name("update");
            Route::post("update-status", [AdminUnitController::class, "updateStatus"])->name("updateStatus");
        });
        Route::group(["prefix" => "product/", "as" => "product."], function () {
            Route::get("", [AdminProductController::class, "index"])->name("index");
            Route::get("create", [AdminProductController::class, "create"])->name("create");
            Route::post("store", [AdminProductController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminProductController::class, "edit"])->name("edit");
            Route::get("show/{id}", [AdminProductController::class, "show"])->name("show");
            Route::put("update/{id}", [AdminProductController::class, "update"])->name("update");
            Route::post('products/sortedDatatable', [AdminProductController::class, "updateOrder"])->name('updateOrder');
            Route::post("update-status", [AdminProductController::class, "updateStatus"])->name("updateStatus");
            Route::delete("delete/{id}", [AdminProductController::class, "destroy"])->name("delete");
            Route::get('get-product-type', [AdminProductController::class, 'childCatBasedOnParent'])->name('get-product-type');

        });
        Route::post('getproducttype', [AdminProductController::class, 'fetchProductType'])->name("getproducttype");

        Route::group(["prefix" => "productImage/", "as" => "productImage."], function () {
            Route::get("", [AdminProductImageController::class, "index"])->name("index");
            Route::get("create/{id}", [AdminProductImageController::class, "create"])->name("create");
            Route::post("store", [AdminProductImageController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminProductImageController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminProductImageController::class, "update"])->name("update");
            Route::delete("delete/{id}", [AdminProductImageController::class, "destroy"])->name("delete");

        });

        Route::group(["prefix" => "about/", "as" => "about."], function () {
            Route::get("", [AdminAboutController::class, "index"])->name("index");
            Route::post("all-abouts", [AdminAboutController::class, "allAbouts"])->name('allAbouts');
            Route::get("create", [AdminAboutController::class, "create"])->name("create");
            Route::post("store", [AdminAboutController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminAboutController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminAboutController::class, "update"])->name("update");
            Route::post("update-status", [AdminAboutController::class, "updateStatus"])->name("updateStatus");
            Route::post('about/delete', [AdminAboutController::class, "delete"])->name('deleteAbout');
            Route::post('abouts/sortedDatatable', [AdminAboutController::class, "updateOrder"])->name('updateOrder');
        });

        Route::group(["prefix" => "brand/", "as" => "brand."], function () {
            Route::get("", [AdminBrandController::class, "index"])->name("index");
            Route::post("all-brands", [AdminBrandController::class, "allBrands"])->name('allBrands');
            Route::get("create", [AdminBrandController::class, "create"])->name("create");
            Route::post("store", [AdminBrandController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminBrandController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminBrandController::class, "update"])->name("update");
            Route::post("update-status", [AdminBrandController::class, "updateStatus"])->name("updateStatus");
            Route::post('brands/sortedDatatable', [AdminBrandController::class, "updateOrder"])->name('updateOrder');
            Route::post('brands/delete', [AdminBrandController::class, "delete"])->name('deleteBrand');

        });
        Route::group(["prefix" => "privacypolicy/", "as" => "privacypolicy."], function () {
            Route::get("", [AdminPrivacyPolicyController::class, "index"])->name("index");
            Route::get("create", [AdminPrivacyPolicyController::class, "create"])->name("create");
            Route::post("store", [AdminPrivacyPolicyController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminPrivacyPolicyController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminPrivacyPolicyController::class, "update"])->name("update");
            Route::post("update-status", [AdminPrivacyPolicyController::class, "updateStatus"])->name("updateStatus");

        });
        Route::group(["prefix" => "contact/", "as" => "contact."], function () {
            Route::get("", [AdminContactController::class, "index"])->name("index");
            Route::delete("delete/{id}", [AdminContactController::class, "destroy"])->name("destroy");
        });

        Route::group(["prefix" => "cart/", "as" => "cart."], function () {
            Route::get("", [AdminCartController::class, "index"])->name("index");
            Route::delete("delete/{id}", [AdminCartController::class, "destroy"])->name("destroy");
        });

        Route::group(["prefix" => "productrating/", "as" => "productrating."], function () {
            Route::get("", [AdminProductRatingController::class, "index"])->name("index");
            Route::post("update-status", [AdminProductRatingController::class, "updateStatus"])->name("updateStatus");
            Route::delete("delete/{id}", [AdminProductRatingController::class, "destroy"])->name("delete");

        });
        Route::group(["prefix" => "service/", "as" => "service."], function () {
            Route::get("", [AdminServiceController::class, "index"])->name("index");
            Route::get("create", [AdminServiceController::class, "create"])->name("create");
            Route::post("store", [AdminServiceController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminServiceController::class, "edit"])->name("edit");
            Route::match(["put", "patch"], "update/{id}", [AdminServiceController::class, "update"])->name("update");
            Route::post("update-status", [AdminServiceController::class, "updateStatus"])->name("updateStatus");
            Route::post('services/sortedDatatable', [AdminServiceController::class, "updateOrder"])->name('updateOrder');
            Route::delete("delete/{id}", [AdminServiceController::class, "destroy"])->name("delete");

        });

        Route::group(["prefix" => "province/", "as" => "province."], function () {
            Route::get("", [AdminProvinceController::class, "index"])->name("index");
            Route::get("create", [AdminProvinceController::class, "create"])->name("create");
            Route::post("store", [AdminProvinceController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminProvinceController::class, "edit"])->name("edit");
            Route::put("update/{id}", [AdminProvinceController::class, "update"])->name("update");
            Route::post('provinces/sortedDatatable', [AdminProvinceController::class, "updateOrder"])->name('updateOrder');
            Route::post("update-status", [AdminProvinceController::class, "updateStatus"])->name("updateStatus");

        });
        Route::group(["prefix" => "city/", "as" => "city."], function () {
            Route::get("", [AdminCityController::class, "index"])->name("index");
            Route::get("create", [AdminCityController::class, "create"])->name("create");
            Route::post("store", [AdminCityController::class, "store"])->name("store");
            Route::get("edit/{id}", [AdminCityController::class, "edit"])->name("edit");
            Route::put("update/{id}", [AdminCityController::class, "update"])->name("update");
            Route::post('cities/sortedDatatable', [AdminCityController::class, "updateOrder"])->name('updateOrder');
            Route::post("update-status", [AdminCityController::class, "updateStatus"])->name("updateStatus");

        });

        Route::group(["prefix" => "order/", "as" => "order."], function () {
            Route::get("", [AdminOrderController::class, "index"])->name("index");
            Route::post("verify-status", [AdminOrderController::class, "verifyOrder"])->name("verifyOrder");
            Route::post("shipping-status", [AdminOrderController::class, "verifyShipping"])->name("verifyShipping");
            Route::post("update-status", [AdminOrderController::class, "updateStatus"])->name("updateStatus");
            Route::delete("delete/{id}", [AdminOrderController::class, "destroy"])->name("delete");
            Route::get("show/{id}", [AdminOrderController::class, "show"])->name("show");

        });

    });

});

Route::get("weekly-visit/", [AdminDashboardController::class, "weeklyvisits"])->name("weeklyvisits");
Route::get("monthly-visit/", [AdminDashboardController::class, "monthlyvisits"])->name("monthlyvisits");

Route::group(['as' => 'frontend.'], function () {
    Route::get('', [FrontendController::class, 'index'])->name('index');
    Route::get('about-us', [FrontendController::class, 'about'])->name('about');
    Route::get('products/{category_slug}/{type_slug?}', [FrontendController::class, 'product'])->name('product');
    Route::get('product-detail/{slug}', [FrontendController::class, 'product_detail'])->name('product_detail');
    Route::get('service-detail/{slug}', [FrontendController::class, 'service_detail'])->name('service_detail');
    Route::get('shop', [FrontendController::class, 'shop'])->name('shop');
    Route::get('contact-us', [FrontendController::class, 'contact'])->name('contact');
    Route::get('cart', [FrontendController::class, 'cart'])->name('cart')->middleware('auth');
    Route::get('checkout', [FrontendController::class, 'checkout'])->name('checkout');
    Route::get('login-page', [FrontendController::class, 'login'])->name('login');
    Route::get('user/register', [FrontendController::class, 'register'])->name('register');
    Route::get('user-profile', [FrontendController::class, 'user_profile'])->name('user_profile')->middleware('auth');
    Route::get('view_order_detail/{id}', [FrontendController::class, 'view_order_detail'])->name('view_order_detail')->middleware('auth');
    Route::get('forget-password', [FrontendController::class, 'forget_password'])->name('forget_password');
    Route::get('privacy_policy', [FrontendController::class, 'privacy_policy'])->name('privacy_policy');
    Route::post("store-contact", [FrontendController::class, "storeContact"])->name("storecontact");
    Route::post("store-product-rating", [FrontendController::class, "storeProductRating"])->name("storeProductRating");
    Route::get("search", [FrontendController::class, "search"])->name("search");
    Route::post("new-visit", [FrontendController::class, "storeVisit"])->name("storeVisit");

    // add items to cart
    Route::post("add-item-to-cart", [FrontendController::class, "addItemsToCart"])->name("addItemsToCart");
    Route::post("delete-cart-item", [FrontendController::class, "deleteCartItems"])->name("deleteCartItems");
    Route::post("update-cart-item", [FrontendController::class, "updateCartItems"])->name("updateCartItems");

// user profile
    Route::post("change-user-email", [FrontendController::class, "changeUserEmail"])->name("changeUserEmail");
    Route::post("change-user-password", [FrontendController::class, "userNewPassword"])->name("userNewPassword");
    Route::post("change-user-profile", [FrontendController::class, "changeUserProfile"])->name("changeUserProfile");

    // forgot password
    Route::post('update/user/password', [FrontendController::class, 'updateUserForgetPassword'])->name('updateUserForgetPassword');

    // login with facebook
    Route::get('auth/facebook', [FrontendController::class, 'redirectToFacebook'])->name('redirectToFacebook');
    Route::get('auth/facebook/callback', [FrontendController::class, 'facebookSignin']);

    // login with google
    Route::get('auth/google', [FrontendController::class, 'redirectToGoogle'])->name('redirectToGoogle');
    Route::get('auth/google/callback', [FrontendController::class, 'googleSignin']);

    // order controller
    Route::post('getcity', [OrderController::class, 'fetchCity'])->name("getcity");
    Route::post("store-order", [OrderController::class, "storeOrder"])->name("storeOrder");

    Route::post('get-product-details-modal', [FrontendController::class, 'get_product_details'])->name('get_product_modal');

});
