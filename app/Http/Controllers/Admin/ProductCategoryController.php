<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    private $page = 'admin.productcategory.';
    private $redirectTo = 'admin.productcategory.index';

    public function index()
    {
        $product_categories = ProductCategory::orderBy('order', 'ASC')->get();
        return view($this->page . 'index', compact('product_categories'))->with('id');
    }
    public function create()
    {
        return view($this->page . 'create');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => ["required", "unique:product_categories,title"],
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                ProductCategory::create($input);
                DB::commit();
                return response()->json(["msg" => "Product category created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }

    }
    public function edit($id)
    {
        $product_category = ProductCategory::findOrFail($id);
        return view($this->page . "edit", compact("product_category"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', "unique:product_categories,title," . $id],
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $product_category = ProductCategory::findOrFail($id);
                $input = $request->except("_token");
                $input["slug"] = getSlug($request->title);

                $product_category->update($input);
                DB::commit();
                return response()->json(["msg" => "Product Category updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateOrder(Request $request)
    {
        $product_categories = ProductCategory::all();
        foreach ($product_categories as $product_category) {
            $product_category->timestamps = false;
            $id = $product_category->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $product_category->update(['order' => $order['position']]);
                }
            }

        }
        return response('Updated successfully.', 200);
    }
    public function updateStatus(Request $request)
    {
        $product_category = ProductCategory::where("id", $request->product_category_id)->first();
        if ($product_category->status == 0) {
            $product_category->update(["status" => 1]);
            $msg = "Product Category is active";
        } else {
            $product_category->update(["status" => 0]);
            $msg = "Product Category is inactive";
        }
        $status = $product_category->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
}
