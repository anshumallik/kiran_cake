<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    private $page = "admin.city.";
    private $redirectTo = "admin.city.index";

    public function index()
    {
        $cities = City::orderBy('order', 'ASC')->get();
        return view($this->page . "index", compact("cities"))->with("id");
    }

    public function create()
    {
        $provinces = Province::where('status', 1)->get();
        return view($this->page . "create", compact('provinces'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "province_id" => ["required"],
            "title" => ["required", "unique:cities,title"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                City::create($input);
                DB::commit();
                return response()->json(["msg" => "City created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $city = City::findOrFail($id);
        $provinces = Province::all();
        return view($this->page . "edit", compact("city", 'provinces'));
    }
    

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "province_id" => ["required"],
            "title" => ["required", "unique:cities,title," . $id],

        ], [
            "province_id.required" => "Province is required",
            "Title.required" => "Title is required",
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $city = City::findOrFail($id);
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                $city->update($input);
                DB::commit();

                return response()->json(["msg" => "City updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateStatus(Request $request)
    {
        $city = City::where("id", $request->city_id)->first();
        if ($city->status == 0) {
            $city->update(["status" => 1]);
            $msg = "City is active";
        } else {
            $city->update(["status" => 0]);
            $msg = "City is inactive";
        }
        $status = $city->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
    public function updateOrder(Request $request)
    {
        $cities = City::all();
        foreach ($cities as $city) {
            $city->timestamps = false;
            $id = $city->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $city->update(['order' => $order['position']]);
                }
            }
        }
        return response('Updated successfully.', 200);
    }
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        FileUnlink($this->destination, $city->image);
        $city->delete();
        return redirect()->route('admin.city.index')->with(notify("error", "city deleted successfully"));
    }

}
