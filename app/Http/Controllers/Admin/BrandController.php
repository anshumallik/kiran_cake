<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    private $page = "admin.brand.";
    private $destination = "images/brand/";
    private $redirectTo = "admin.brand.index";

    public function allBrands(Request $request)
    {
        $columns = array(
            0 => 'order',
            1 => 'url',
            2 => 'logo',
            3 => 'status',
            4 => 'order',
        );

        $totalData = Brand::orderBy('order', 'ASC')->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $brands = Brand::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $brands = Brand::where('id', 'LIKE', "%{$search}%")
                ->orWhere('url', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Brand::where('id', 'LIKE', "%{$search}%")
                ->orWhere('url', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($brands)) {
            $i = 1;
            foreach ($brands as $brand) {

                $edit = route('admin.brand.edit', $brand->id);
                if ($brand->logo != null) {
                    $image = asset('images/brand/' . $brand->logo);
                } else {
                    $image = asset('images/noimage.jpg');
                }
                $status = $brand->status ? 'Active' : 'Inactive';
                $buttonClass = $brand->status ? 'badge-success' : 'badge-warning';
                $plus = $i++;
                $nestedData['i'] = $brand->id;
                $nestedData['id'] = "<div style='cursor: pointer;' title='change display order'>{$plus}</div>";
                $nestedData['url'] = "<div style='cursor: pointer;' title='change display order'>{$brand->url}</div>";

                $nestedData['logo'] = "<img style='height: 50px; width: 50px;' src='{$image}'>";
                $nestedData['status'] = "<span class='cur_sor badge {$buttonClass}' onclick='updateStatus($brand->id)'>
                                                {$status}
                                            </span>";
                $nestedData['action'] = "<div style='display: inline-flex;'>
                                    <div>
                                        <a href = '{$edit}' class='btn btn-primary btn-sm mr-2' title='Edit Brand'><i class='fa fa-edit iCheck'></i></a>
                                     </div>
                                     <button class='btn btn-danger btn-sm' title='Delete' onclick='deleteBrand($brand->id)'><i class='fa fa-trash iCheck'></i></button>
                                </div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function index()
    {
        $brands = Brand::orderBy('order','ASC')->get();
        return view($this->page . "index", compact("brands"))->with("id");
    }

    public function create()
    {
        return view($this->page . "create");
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "url" => ["nullable", "url"],
            "logo" => ["required", "image", "mimes:jpeg,jpg,png", "max:4096"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $image = Upload::image($request, 'logo', $this->destination, null);
                $imageName = $input["logo"] = $image["imageName"];
                Brand::create($input);
                DB::commit();
                if ($request->hasFile('logo')) {
                    $image["image"]->move($this->destination, $imageName);
                }

                return response()->json(["msg" => "Brand created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $brand = Brand::findOrFail($id);
        return view($this->page . "edit", compact("brand"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            "url" => ["nullable","url"],
            "logo" => ["nullable", "image", "mimes:jpeg,jpg,png", "max:4096"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $brand = Brand::findOrFail($id);
                $oldImage = $brand->image;
                $input = $request->except("_token");

                if ($request->hasFile("logo")) {
                    $image = Upload::image($request, "logo", $this->destination);
                    $imageName = $input["logo"] = $image["imageName"];
                }

                $brand->update($input);
                DB::commit();
                if ($request->hasFile("logo")) {
                    FileUnlink($this->destination, $oldImage);
                    $image["image"]->move($this->destination, $imageName);

                }
                return response()->json(["msg" => "Brand updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }
    
    public function delete(Request $request)
    {
        $brand = Brand::find($request->brand_id);
        FileUnlink($this->destination, $brand->image);
        $brand->delete();
        return response()->json(['msg' => 'Logo deleted']);
    }

    public function updateStatus(Request $request)
    {
        $brand = Brand::where("id", $request->brand_id)->first();
        if ($brand->status == 0) {
            $brand->update(["status" => 1]);
            $msg = "Brand is active";
        } else {
            $brand->update(["status" => 0]);
            $msg = "Brand is inactive";
        }
        $status = $brand->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }

    public function updateOrder(Request $request)
    {
        $brands = Brand::all();
        foreach ($brands as $brand) {
            $brand->timestamps = false;
            $id = $brand->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $brand->update(['order' => $order['position']]);
                }
            }
        }
        return response('Updated successfully.', 200);
    }
}
