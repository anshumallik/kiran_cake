<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $page = "admin.order.";
    public function index()
    {
        $orders = Order::latest()->get();
        return view($this->page . "index", compact('orders'))->with('id');
    }

    public function destroy($id)
    {
        Order::destroy($id);
        return redirect()->back()->with(notify("error", "Order deleted successfully"));

    }

    public function verifyOrder(Request $request)
    {
        $order = Order::where("id", $request->order_id)->first();
        if ($order->verify_status == 0) {
            $order->update(["verify_status" => 1]);
            $msg = "Order is verified";
        } else {
            $order->update(["verify_status" => 0]);
            $msg = "Order is unverified";
        }
        $status = $order->verify_status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
    public function verifyShipping(Request $request)
    {
        $order = Order::where("id", $request->order_id)->first();
        if ($order->shipping_status == 0) {
            $order->update(["shipping_status" => 1]);
            $msg = "Order is shipped";
        } else {
            $order->update(["shipping_status" => 0]);
            $msg = "Order is unshipped";
        }
        $status = $order->shipping_status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
    public function updateStatus(Request $request)
    {
        $order = Order::where("id", $request->order_id)->first();
        if ($order->status == 0) {
            $order->update(["status" => 1]);
            $msg = "Order is active";
        } else {
            $order->update(["status" => 0]);
            $msg = "Order is inactive";
        }
        $status = $order->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }

    public function show($id)
    {
        $order = Order::where('id', $id)->with('order_items')->first();
        return view($this->page . "show", compact("order"));
    }
}
