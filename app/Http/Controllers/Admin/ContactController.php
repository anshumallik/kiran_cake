<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    private $page = "admin.contact.";
    public function index()
    {
        $contacts = Contact::latest()->get();
        return view($this->page . "index", compact('contacts'));
    }

    public function destroy($id)
    {
        Contact::destroy($id);
        return redirect()->back()->with(notify("error", "Contact deleted successfully"));

    }
}
