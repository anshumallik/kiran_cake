<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    private $page = "admin.slider.";
    private $destination = "images/slider/";
    private $redirectTo = "admin.slider.index";

    public function index()
    {
        $sliders = Slider::latest()->get();
        return view($this->page . "index", compact("sliders"))->with("id");
    }

    public function create()
    {
        return view($this->page . "create");
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "caption" => ["nullable"],
            "link" => ["nullable"],
            "image" => ["required", "image", "mimes:jpeg,jpg,png", "max:4096"],
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $image = Upload::image($request, "image", $this->destination);
                $imageName = $input["image"] = $image["imageName"];
                $image["image"]->move($this->destination, $imageName);
                Slider::create($input);
                DB::commit();
                return response()->json(["msg" => "Slider created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view($this->page . "edit", compact("slider"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "caption" => ["nullable"],
            "link" => ["nullable"],
            "image" => ["nullable", "image", "mimes:jpeg,jpg,png", "max:4096"],
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $slider = Slider::findOrFail($id);
                $oldImage = $slider->image;
                $input = $request->except("_token");
                if ($request->hasFile("image")) {
                    $image = Upload::image($request, "image", $this->destination);
                    $imageName = $input["image"] = $image["imageName"];
                }
                $slider->update($input);
                DB::commit();
                if ($request->hasFile("image")) {
                    FileUnlink($this->destination, $oldImage);
                    $image["image"]->move($this->destination, $imageName);

                }
                return response()->json(["msg" => "Slider updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateStatus(Request $request)
    {
        $slider = Slider::where("id", $request->slider_id)->first();
        if ($slider->status == 0) {
            $slider->update(["status" => 1]);
            $msg = "Slider is active";
        } else {
            $slider->update(["status" => 0]);
            $msg = "Slider is inactive";
        }
        $status = $slider->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
}
