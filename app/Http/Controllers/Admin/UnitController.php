<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
    private $page = "admin.unit.";
    private $redirectTo = "admin.unit.index";

    public function index()
    {
        $units = Unit::all();
        return view($this->page . 'index', compact('units'))->with('id');
    }

    public function create()
    {
        return view($this->page . 'create');
    }
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "title" => ["required", "unique:units,title"],
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                Unit::create($input);
                DB::commit();
                return response()->json(["msg" => "Unit created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }
    public function edit($id)
    {
        $unit = Unit::findOrFail($id);
        return view($this->page . "edit", compact("unit"));
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [

            "title" => ["required", "unique:units,title," . $id],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $unit = Unit::findOrFail($id);
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                $unit->update($input);
                DB::commit();
                return response()->json(["msg" => "Unit updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }

    }

    public function updateStatus(Request $request)
    {
        $unit = Unit::where("id", $request->unit_id)->first();
        if ($unit->status == 0) {
            $unit->update(["status" => 1]);
            $msg = "Unit is active";
        } else {
            $unit->update(["status" => 0]);
            $msg = "Unit is inactive";
        }
        $status = $unit->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
}
