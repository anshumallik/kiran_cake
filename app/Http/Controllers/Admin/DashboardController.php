<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Visit;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $totalCategory = ProductCategory::where('status',1)->count();
        $totalVisit = Visit::count();
        $totalProduct = Product::where('status',1)->count();
        return view("admin.dashboard",compact('totalProduct','totalVisit','totalCategory'));
    }
    public function weeklyvisits()
    {
        $visitCharts = Visit::select([
            DB::raw('DATE_FORMAT(visit_date,"%a") AS datetime'),
            DB::raw('COUNT(*) AS count'),
        ])
            ->whereBetween('visit_date', [Carbon::now()->subDays(7), Carbon::now()])
            ->groupBy('datetime')
            ->orderBy('datetime', 'ASC')
            ->get()
            ->toArray();
        $visitChartByDay = array();
        $currentSevenDays = CarbonPeriod::create(Carbon::now()->subDays(6), Carbon::now());
        foreach ($currentSevenDays as $data) {
            $visitChartByDay[$data->format('D')] = 0;
        }
        foreach ($visitCharts as $visitChart) {
            $visitChartByDay[$visitChart['datetime']] = $visitChart['count'];
        }
        return response()->json($visitChartByDay);

    }

    public function monthlyvisits()
    {
        $visits = Visit::select([
            DB::raw('DATE(visit_date) AS date'),
            DB::raw('COUNT(*) AS count'),
        ])->whereBetween('visit_date', [Carbon::now()->subDays(30), Carbon::now()])
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get()
            ->toArray();
        $lastThirtyDays = CarbonPeriod::create(Carbon::now()->subDays(29), Carbon::now());
        $dateCount = array();
        foreach ($lastThirtyDays as $date) {
            $dateCount[$date->format("Y-m-d")] = 0;
        }

        foreach ($visits as $countPerDay) {
            $dateCount[$countPerDay['date']] = $countPerDay['count'];
        }
        return response()->json($dateCount);
    }
}
