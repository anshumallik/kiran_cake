<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductType;
use App\Models\Unit;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    private $page = "admin.product.";
    private $destination = "images/product/";
    private $redirectTo = "admin.product.index";

    public function index()
    {
        $products = Product::orderBy('order', 'ASC')->get();
        return view($this->page . "index", compact("products"))->with("id");
    }

    public function create()
    {
        $product_types = ProductType::all();
        $product_categories = ProductCategory::where('status',1)->get();
        $units = Unit::all();
        return view($this->page . "create", compact('units', 'product_types', 'product_categories'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "product_category_id" => ["required"],
            "product_type_id" => ["nullable"],
            "unit_id" => ["required"],
            "name" => ["required", "unique:products,name"],
            "qty" => ["required"],
            "code" => ["required", "unique:products,code"],
            "price" => ["required"],
            "image" => ["required", "image", "mimes:jpeg,jpg,png", "max:4096"],
            "description" => ["nullable"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $image = Upload::image($request, "image", $this->destination);
                $imageName = $input["image"] = $image["imageName"];
                $image["image"]->move($this->destination, $imageName);
                $input['slug'] = getSlug($request->name);

                Product::create($input);
                DB::commit();
                return response()->json(["msg" => "Product created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $units = Unit::all();
        $product_types = ProductType::all();
        $product_categories = ProductCategory::all();

        return view($this->page . "edit", compact("product", 'units', 'product_types', 'product_categories'));
    }
    public function show($id)
    {
        $product = Product::where('id', $id)->with('product_images')->first();
        return view($this->page . "show", compact("product"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "product_category_id" => ["required"],
            "product_type_id" => ["nullable"],
            "unit_id" => ["required"],
            "name" => ["required", "unique:products,name," . $id],
            "qty" => ["required"],
            "code" => ["required"],
            "price" => ["required"],
            "image" => ["nullable", "image", "mimes:jpeg,jpg,png", "max:4096"],
            "description" => ["nullable"],
        ], [
            "product_category_id.required" => "Category is required",
            "unit_id.required" => "Unit is required",
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $product = Product::findOrFail($id);
                $oldImage = $product->image;
                $input = $request->except("_token");

                if ($request->hasFile("image")) {
                    $image = Upload::image($request, "image", $this->destination);
                    $imageName = $input["image"] = $image["imageName"];
                }
                $input['slug'] = getSlug($request->name);

                $product->update($input);
                DB::commit();
                if ($request->hasFile("image")) {
                    FileUnlink($this->destination, $oldImage);
                    $image["image"]->move($this->destination, $imageName);

                }
                return response()->json(["msg" => "Product updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateStatus(Request $request)
    {
        $product = Product::where("id", $request->product_id)->first();
        if ($product->status == 0) {
            $product->update(["status" => 1]);
            $msg = "Product is active";
        } else {
            $product->update(["status" => 0]);
            $msg = "Product is inactive";
        }
        $status = $product->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
    public function updateOrder(Request $request)
    {
        $products = Product::all();
        foreach ($products as $product) {
            $product->timestamps = false;
            $id = $product->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $product->update(['order' => $order['position']]);
                }
            }
        }
        return response('Updated successfully.', 200);
    }
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        FileUnlink($this->destination, $product->image);
        $product->delete();
        return redirect()->route('admin.product.index')->with(notify("error", "Product deleted successfully"));
    }

    public function fetchProductType(Request $request)
    {
        
        // $product_types = DB::table('category_types')->where('product_category_id', $request->category_id)->pluck('product_type_id')->toArray();
        // dd($product_types);
        $category = ProductCategory::where('id', $request->category_id)->first();
        if($category != null){
            $productTypes = $category->product_types->pluck('title','id');
        } else {
            $productTypes = null;
        }
       
        // dd( $productTypes);
        return response()->json($productTypes);
    }
}
