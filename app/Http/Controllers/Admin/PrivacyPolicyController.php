<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PrivacyPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PrivacyPolicyController extends Controller
{
    private $page = "admin.privacypolicy.";
    private $redirectTo = "admin.privacypolicy.index";

    public function index()
    {
        $privacy_policies = PrivacyPolicy::get();
        return view($this->page . "index", compact("privacy_policies"))->with("id");
    }

    public function create()
    {
        return view($this->page . "create");
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            "title" => ["required"],
            "description" => ["required"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);

                PrivacyPolicy::create($input);
                DB::commit();

                return response()->json(["msg" => "Created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $privacy_policy = PrivacyPolicy::findOrFail($id);
        return view($this->page . "edit", compact("privacy_policy"));
    }

    public function update(Request $request, $id)
    {
        $validator = $this->__validation($request->all());
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $privacy_policy = PrivacyPolicy::findOrFail($id);
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                $privacy_policy->update($input);
                DB::commit();
                return response()->json(["msg" => "Updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    private function __validation(array $data)
    {
        $validator = Validator::make($data, [
            "title" => ["required"],
            "description" => ["required"],

        ]);
        return $validator;
    }

    public function updateStatus(Request $request)
    {
        $privacy_policy = PrivacyPolicy::where("id", $request->privacy_policy_id)->first();
        if ($privacy_policy->status == 0) {
            $privacy_policy->update(["status" => 1]);
            $msg = "Privacy policy is active";
        } else {
            $privacy_policy->update(["status" => 0]);
            $msg = "Privacy policy is inactive";
        }
        $status = $privacy_policy->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
}
