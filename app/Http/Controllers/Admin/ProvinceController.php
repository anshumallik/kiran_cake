<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Province;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProvinceController extends Controller
{
    private $page = 'admin.province.';
    private $redirectTo = 'admin.province.index';

    public function index()
    {
        $provinces = Province::orderBy('order', 'ASC')->get();
        return view($this->page . 'index', compact('provinces'))->with('id');
    }
    public function create()
    {
        return view($this->page . 'create');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => ["required", "unique:provinces,title"],
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                Province::create($input);
                DB::commit();
                return response()->json(["msg" => "Province created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }

    }
    public function edit($id)
    {
        $province = Province::findOrFail($id);
        return view($this->page . "edit", compact("province"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', "unique:provinces,title," . $id],
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $province = Province::findOrFail($id);
                $input = $request->except("_token");
                $input["slug"] = getSlug($request->title);

                $province->update($input);
                DB::commit();
                return response()->json(["msg" => "Province updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateOrder(Request $request)
    {
        $provinces = Province::all();
        foreach ($provinces as $province) {
            $province->timestamps = false;
            $id = $province->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $province->update(['order' => $order['position']]);
                }
            }

        }
        return response('Updated successfully.', 200);
    }
    public function updateStatus(Request $request)
    {
        $province = Province::where("id", $request->province_id)->first();
        if ($province->status == 0) {
            $province->update(["status" => 1]);
            $msg = "Province is active";
        } else {
            $province->update(["status" => 0]);
            $msg = "Province is inactive";
        }
        $status = $province->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
}
