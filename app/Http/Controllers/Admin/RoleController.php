<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    private $page = "admin.role.";
    private $redirectTo = "admin.role.index";

    public function index()
    {
        $roles = Role::all();
        return view($this->page . 'index', compact('roles'))->with("id");
    }

    public function create()
    {
        $permissions = Permission::where('name', '<>', 'All')->get();
        return view($this->page . 'create', compact('permissions'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => ["required", "unique:roles,name"],
            "permissions" => ["required"],
        ], [
            "permissions.required" => "Please select permission",
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $role = new Role();
                $role->name = $request->name;
                $role->slug = getSlug($request->name);
                $permissions = $request->permissions;
                $role->save();
                $role->permissions()->sync($permissions);
                DB::commit();
                return response()->json(["msg" => "Role Created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::where('name', '<>', 'All')->get();
        return view($this->page . 'edit', compact('role', 'permissions'));
    }
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'unique:roles,name,' . $id],
            'permissions' => ['required'],
        ]);
        if ($validator->fails()) {
            return response()->json(['errors', $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $role = Role::find($id);
                $role->name = $request->name;
                $role->slug = getslug($request->name);
                $permissions = $request->permissions;
                $role->save();
                $role->permissions()->sync($permissions);
                DB::commit();
                return response()->json(["msg" => 'Role Updated successfully', 'redirectRoute' => route($this->redirectTo)]);

            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }
}
