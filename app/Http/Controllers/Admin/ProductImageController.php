<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductImageController extends Controller
{
    private $page = "admin.productImage.";
    private $destination = "images/productImage/";
    private $redirectTo = "admin.productImage.index";

    public function index()
    {
        $product_images = ProductImage::latest()->get();
        return view('admin.product.index', compact("product_images"))->with("id");
    }

    public function create($id)
    {
        $product = Product::find($id);
        return view('admin.product.productImage', compact('product'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'images' => 'required',
            'image' => 'mimes:jpeg,png,jpg,svg|max:10000',
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $files = $request->file('images');
                foreach ($files as $file) {
                    $fileName = time() . "." . $file->getClientOriginalName();
                    $product_image = new ProductImage();
                    $product_image->product_id = $request->product_id;
                    $product_image->image = $fileName;
                    $product_image->save();
                    DB::commit();
                    $file->move($this->destination, $fileName);
                }

                return response()->json(["msg" => "Product Image created successfully", "redirectRoute" => route('admin.product.show',$request->product_id)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }

    }

    public function edit($id)
    {
        $product_image = ProductImage::findOrFail($id);
        return view($this->page . "edit", compact("product_image"));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,png,jpg,svg|max:10000',
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            // dd($request->all());
            try {
                DB::beginTransaction();
                $product_image = ProductImage::findOrFail($id);
                $oldImage = $product_image->image;
                $input = $request->except("_token");

                if ($request->hasFile("image")) {
                    $image = Upload::image($request, "image", $this->destination);
                    $imageName = $input["image"] = $image["imageName"];
                }

                $product_image->update($input);
                DB::commit();
                if ($request->hasFile("image")) {
                    FileUnlink($this->destination, $oldImage);
                    $image["image"]->move($this->destination, $imageName);

                }
                return response()->json(["msg" => "Product Image updated successfully", "redirectRoute" => route("admin.product.show", $product_image->product_id)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }

        }
    }

    public function destroy($id)
    {
        $product_image = ProductImage::findOrFail($id);
        FileUnlink($this->destination, $product_image->image);
        $product_image->delete();
        return redirect()->back()->with(notify("error", "Product Image deleted successfully"));
    }

}
