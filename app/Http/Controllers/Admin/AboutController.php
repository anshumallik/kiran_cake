<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    private $page = "admin.about.";
    private $destination = "images/about/";
    private $redirectTo = "admin.about.index";

    public function allAbouts(Request $request)
    {
        $columns = array(
            0 => 'order',
            1 => 'title',
            2 => 'description',
            3 => 'image',
            4 => 'status',
            5 => 'order',
        );

        $totalData = About::orderBy('order', 'ASC')->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $abouts = About::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $abouts = About::where('id', 'LIKE', "%{$search}%")
                ->orWhere('title', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = About::where('id', 'LIKE', "%{$search}%")
                ->orWhere('title', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($abouts)) {
            $i = 1;
            foreach ($abouts as $about) {

                $edit = route('admin.about.edit', $about->id);
                // $delete = route('deleteAbout');
                if ($about->image != null) {
                    $image = asset('images/about/' . $about->image);
                } else {
                    $image = asset('images/noimage.jpg');
                }
                $status = $about->status ? 'Active' : 'Inactive';
                $buttonClass = $about->status ? 'badge-success' : 'badge-warning';
                $plus = $i++;
                $nestedData['i'] = $about->id;
                $nestedData['id'] = "<div style='cursor: pointer;' title='change display order'>{$plus}</div>";
                $nestedData['title'] = "<div style='cursor: pointer;' title='change display order'>{$about->title}</div>";
                $description = html_entity_decode($about->description);
                $finalDescription = strip_tags($description);
                $nestedData['description'] = "<textarea style='width: 100%;' cols='10' rows='5'>{$finalDescription}</textarea>";
                $nestedData['image'] = "<img style='height: 50px; width: 50px;' src='{$image}'>";
                $nestedData['status'] = "<span class='cur_sor badge {$buttonClass}' onclick='updateStatus($about->id)'>
                                                {$status}
                                            </span>";
                $nestedData['action'] = "<div style='display: inline-flex;'>
                                    <div>
                                        <a href = '{$edit}' class='btn btn-primary btn-sm' title='Edit About'><i class='fa fa-edit iCheck'></i>&nbsp; Edit About</a>
                                    <button class='btn btn-danger btn-sm' title='Delete About' onclick='deleteAbout($about->id)'><i class='fa fa-trash iCheck'></i>&nbsp; Delete</a>
                                        </div>
                                </div>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        echo json_encode($json_data);
    }

    public function index()
    {
        $abouts = About::latest()->get();
        return view($this->page . "index", compact("abouts"))->with("id");
    }

    public function create()
    {
        return view($this->page . "create");
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "title" => ["required", "unique:abouts,title"],
            "image" => ["nullable", "image", "mimes:jpeg,jpg,png", "max:4096"],
            "description" => ["nullable"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                if ($request->hasFile('image')) {
                    $image = Upload::image($request, "image", $this->destination);
                    $imageName = $input["image"] = $image["imageName"];
                    $image["image"]->move($this->destination, $imageName);

                }
                $input['slug'] = getSlug($request->title);

                About::create($input);
                DB::commit();
                return response()->json(["msg" => "About created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $about = About::findOrFail($id);

        return view($this->page . "edit", compact("about"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            "title" => ["required", "unique:abouts,title," . $id],
            "image" => ["nullable", "mimes:jpeg,jpg,png,pdf", "max:4096"],
            "description" => ["nullable"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $about = About::findOrFail($id);
                $oldImage = $about->image;
                $input = $request->except("_token");

                if ($request->hasFile("image")) {
                    $image = Upload::image($request, "image", $this->destination);
                    $imageName = $input["image"] = $image["imageName"];
                }
                $input['slug'] = getSlug($request->title);

                $about->update($input);
                DB::commit();
                if ($request->hasFile("image")) {
                    FileUnlink($this->destination, $oldImage);
                    $image["image"]->move($this->destination, $imageName);

                }
                return response()->json(["msg" => "About updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateStatus(Request $request)
    {
        $about = About::where("id", $request->about_id)->first();
        if ($about->status == 0) {
            $about->update(["status" => 1]);
            $msg = "About is active";
        } else {
            $about->update(["status" => 0]);
            $msg = "About is inactive";
        }
        $status = $about->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }

    public function updateOrder(Request $request)
    {
        $abouts = About::all();
        foreach ($abouts as $about) {
            $about->timestamps = false;
            $id = $about->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $about->update(['order' => $order['position']]);
                }
            }

        }
        return response('Update successfully.', 200);
    }
    public function delete(Request $request)
    {
        $about = About::find($request->about_id);
        FileUnlink($this->destination, $about->image);
        $about->delete();
        return response()->json(['msg' => 'About deleted']);
    }
}
