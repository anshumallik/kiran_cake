<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    private $page = "admin.cart.";
    public function index()
    {
        $cart_items = Cart::latest()->get();
        // dd($cart_items);
        return view($this->page . "index", compact('cart_items'));
    }

    public function destroy($id)
    {
        Cart::destroy($id);
        return redirect()->back()->with(notify("error", "Cart item deleted successfully"));

    }
}
