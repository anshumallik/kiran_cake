<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Service;
use App\Models\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    private $page = "admin.service.";
    private $destination = "images/service/";
    private $redirectTo = "admin.service.index";

    public function index()
    {
        $services = Service::latest()->get();
        return view($this->page . "index", compact("services"))->with("id");
    }

    public function create()
    {
        return view($this->page . "create");
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "title" => ["required", "unique:services,title"],
            "image" => ["required", "image", "mimes:jpeg,jpg,png", "max:4096"],
            "description" => ["nullable"],
            "date" => ["date"],

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $image = Upload::image($request, "image", $this->destination);
                $imageName = $input["image"] = $image["imageName"];
                $image["image"]->move($this->destination, $imageName);
                $input['slug'] = getSlug($request->title);

                Service::create($input);
                DB::commit();
                return response()->json(["msg" => "Service created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function edit($id)
    {
        $service = Service::findOrFail($id);

        return view($this->page . "edit", compact("service"));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            "title" => ["required", "unique:services,title," . $id],
            "image" => ["nullable", "mimes:jpeg,jpg,png,pdf", "max:4096"],
            "description" => ["nullable"],
            "date" => ["date"],
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $service = Service::findOrFail($id);
                $oldImage = $service->image;
                $input = $request->except("_token");

                if ($request->hasFile("image")) {
                    $image = Upload::image($request, "image", $this->destination);
                    $imageName = $input["image"] = $image["imageName"];
                }
                $input['slug'] = getSlug($request->title);

                $service->update($input);
                DB::commit();
                if ($request->hasFile("image")) {
                    FileUnlink($this->destination, $oldImage);
                    $image["image"]->move($this->destination, $imageName);

                }
                return response()->json(["msg" => "Service updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateStatus(Request $request)
    {
        $service = Service::where("id", $request->service_id)->first();
        if ($service->status == 0) {
            $service->update(["status" => 1]);
            $msg = "Service is active";
        } else {
            $service->update(["status" => 0]);
            $msg = "Service is inactive";
        }
        $status = $service->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
     public function updateOrder(Request $request)
    {
        $services = Service::all();
        foreach ($services as $service) {
            $service->timestamps = false;
            $id = $service->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $service->update(['order' => $order['position']]);
                }
            }
        }
        return response('Updated successfully.', 200);
    }
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        FileUnlink($this->destination, $service->image);
        $service->delete();
        return redirect()->route('admin.service.index')->with(notify("error", "Service deleted successfully"));
    }

}
