<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductRating;
use Illuminate\Http\Request;

class ProductRatingController extends Controller
{
    private $page = "admin.productrating.";
    private $redirectTo = "admin.productrating.index";

    public function index()
    {
        $product_ratings = ProductRating::latest()->get();
        return view($this->page . "index", compact("product_ratings"))->with("id");
    }

    public function updateStatus(Request $request)
    {
        $product_rating = ProductRating::where("id", $request->product_rating_id)->first();
        if ($product_rating->status == 0) {
            $product_rating->update(["status" => 1]);
            $msg = "Rating is active";
        } else {
            $product_rating->update(["status" => 0]);
            $msg = "Rating is inactive";
        }
        $status = $product_rating->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }

    public function destroy($id)
    {
        $product_rating = ProductRating::findOrFail($id);
        $product_rating->delete();
        return redirect()->route($this->redirectTo)->with(notify("error", "Product Rating deleted successfully"));
    }

}
