<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductTypeController extends Controller
{
    private $page = 'admin.product-type.';
    private $redirectTo = 'admin.product-type.index';

    public function index()
    {

        $product_types = ProductType::orderBy('order', 'ASC')->get();
        // dd($product_types);
        return view($this->page . 'index', compact('product_types'))->with('id');
    }
    public function create()
    {
        $product_categories = ProductCategory::where('status', 1)->get();
        return view($this->page . 'create', compact('product_categories'));
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // "product_category_id" => ["required"],
            "title" => ["required", "unique:product_types,title"],

        ], [
            "product_category_id.required" => "Category is required",
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }

        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $input['slug'] = getSlug($request->title);
                $product_type = ProductType::create($input);
                $product_type->categories()->attach($request->category);
                DB::commit();
                return response()->json(["msg" => "Product type created successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }

    }
    public function edit($id)
    {
        $product_categories = ProductCategory::where('status', 1)->get();
        $product_type = ProductType::findOrFail($id);
        return view($this->page . "edit", compact("product_type", 'product_categories'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            // "product_category_id" => ["required"],
            "title" => ["required", "unique:product_types,title," . $id],

        ], [
            "product_category_id.required" => "Category is required",
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $product_type = ProductType::findOrFail($id);
                $input = $request->except("_token");
                $input["slug"] = getSlug($request->title);
                $product_type->update($input);
                $product_type->categories()->sync($request->category);
                DB::commit();
                return response()->json(["msg" => "Product type updated successfully", "redirectRoute" => route($this->redirectTo)]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(["db_error" => $e->getMessage()]);
            }
        }
    }

    public function updateOrder(Request $request)
    {
        $product_types = ProductType::all();
        foreach ($product_types as $product_type) {
            $product_type->timestamps = false;
            $id = $product_type->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $product_type->update(['order' => $order['position']]);
                }
            }

        }
        return response('Updated successfully.', 200);
    }
    public function updateStatus(Request $request)
    {
        $product_type = ProductType::where("id", $request->product_type_id)->first();
        if ($product_type->status == 0) {
            $product_type->update(["status" => 1]);
            $msg = "Product type is active";
        } else {
            $product_type->update(["status" => 0]);
            $msg = "Product type is inactive";
        }
        $status = $product_type->status;
        return response()->json(["msg" => $msg, "status" => $status]);

    }
}
