<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    public function fetchCity(Request $request)
    {

        $province = Province::where('id', $request->province_id)->first();
        if ($province != null) {
            $cities = $province->cities->pluck('title', 'id');
        } else {
            $cities = null;
        }
        return response()->json($cities);
    }

    public function storeOrder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "name" => ["required"],
            "phone" => ['required'],
            "province_id" => ['required'],
            "city_id" => ['required'],
            "address" => ['required'],
        ], [
            "name.required" => "Please enter your name",
            "phone.required" => "Please enter your phone",
            "province_id.required" => "Please enter your province",
            "city_id.required" => "Please enter your city",
            "address.required" => "Please enter your address",

        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                $order = Order::create($input);
                $cartitems = Cart::where('user_id', Auth::id())->get();
                foreach ($cartitems as $cartitem) {
                    OrderItem::create([
                        'order_id' => $order->id,
                        'product_id' => $cartitem->product_id,
                        'quantity' => $cartitem->qty,
                        'price' => $cartitem->products->price,
                        'subtotal' => $cartitem->products->price * $cartitem->qty,
                    ]);
                    $product = Product::where('id', $cartitem->product_id)->first();
                    $product->qty = $product->qty - $cartitem->qty;
                    $product->update();
                }
                $cartitems = Cart::where('user_id', Auth::id())->get();
                Cart::destroy($cartitems);
                DB::commit();
                return response()->json(["msg" => "Your order has been placed successfully", "redirectRoute" => route('frontend.cart')]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(["db_error" => $e->getMessage()]);
            }

        }

    }
}
