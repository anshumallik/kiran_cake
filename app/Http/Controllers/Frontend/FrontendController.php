<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\CompanyData;
use App\Models\Contact;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\PrivacyPolicy;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductRating;
use App\Models\ProductType;
use App\Models\Province;
use App\Models\Service;
use App\Models\Slider;
use App\Models\User;
use App\Models\Visit;
use App\Rules\MatchPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Socialite;
use Spatie\Searchable\Search;

class FrontendController extends Controller
{
    public function index()
    {
        $company_data = CompanyData::first();
        $services = Service::where('status', 1)->latest()->take(3)->get();
        $product_images = ProductImage::where('status', 1);
        $featured_products = Product::where('status', 1)
            ->where('is_featured', 1)->orderBy('order', 'ASC')->take(5)->get();
        $trending_products = Product::where('status', 1)
            ->where('is_trending', 1)->orderBy('order', 'ASC')->take(5)->get();
        $best_selling_products = Product::where('status', 1)
            ->where('is_bestselling', 1)->orderBy('order', 'ASC')->take(5)->get();
        $latest_products = Product::where('status', 1)
            ->where('is_latest', 1)->orderBy('order', 'ASC')->take(5)->get();

        $sliders = Slider::where('status', 1)->latest()->get();
        $brands = Brand::where('status', 1)->latest()->get();

        return view('frontend.index', compact('brands', 'company_data', 'services', 'sliders', 'latest_products', 'best_selling_products', 'featured_products', 'trending_products'));
    }

    public function product($category_slug, $type_slug = null)
    {
        $products = Product::query();
        $category = ProductCategory::where('slug', $category_slug)->first();
        $types = $category->product_types;
        if ($category != null) {
            $category_id = $category->id;
            $products = $products->where('product_category_id', $category_id);
        }
        if ($type_slug != null) {
            $type = ProductType::where('slug', $type_slug)->first();
            if ($type != null) {
                $type_id = $type->id;
                $products = $products->where('product_type_id', $type_id);
            }
        }
        $products = $products;

        // if sort option is selected
        if (isset($_GET['sort']) && !empty($_GET['sort'])) {
            if ($_GET['sort'] == "name_a_to_z") {
                $products->orderBy('name', 'ASC');

            } else if ($_GET['sort'] == "name_z_to_a") {
                $products->orderBy('name', 'DESC');

            } else if ($_GET['sort'] == "price_low_to_high") {
                $products->orderBy('price', 'ASC');
                // $products->orderByRaw('CONVERT(price, int) ASC');

            } else if ($_GET['sort'] == "price_high_to_low") {
               $products->orderBy('price', 'DESC');
            //    $products->orderByRaw('CONVERT(price, int) DESC');


            }

        } if(isset($_GET['price_range'])){
            $price = explode('-', $_GET['price_range']);
            $products->whereBetween('price',[$price[0], $price[1]]);
        } else {
            $products->orderBy('order');

        }
        $products = $products->paginate(18);
        $ids = $products->pluck('id')->toArray();
        $totalProductReview = ProductRating::where('product_ratings.product_id', (array) $ids)->where('status', 1)->count();
        if ($totalProductReview > 0) {
            $totalFiveRating = ProductRating::where('product_ratings.product_id', (array) $ids)->where('rating', 5)->where('status', 1)->count();
            $fivePercent = round(($totalFiveRating * 100) / ($totalProductReview), 1);
            $totalFourRating = ProductRating::where('product_ratings.product_id', (array) $ids)->where('rating', 4)->where('status', 1)->count();
            $fourPercent = round(($totalFourRating * 100) / ($totalProductReview), 1);
            $totalThreeRating = ProductRating::where('product_ratings.product_id', (array) $ids)->where('rating', 3)->where('status', 1)->count();
            $threePercent = round(($totalThreeRating * 100) / ($totalProductReview), 1);
            $totalTwoRating = ProductRating::where('product_ratings.product_id', (array) $ids)->where('rating', 2)->where('status', 1)->count();
            $twoPercent = round(($totalTwoRating * 100) / ($totalProductReview), 1);
            $totalOneRating = ProductRating::where('product_ratings.product_id', (array) $ids)->where('rating', 1)->where('status', 1)->count();
            $onePercent = round(($totalOneRating * 100) / ($totalProductReview), 1);
        } else {
            $fivePercent = 0;
            $fourPercent = 0;
            $threePercent = 0;
            $twoPercent = 0;
            $onePercent = 0;
        }

        // dd($products);
        return view('frontend.product', compact('products', 'types', 'totalProductReview',
            'fivePercent', 'fourPercent', 'threePercent', 'twoPercent', 'onePercent'));
    }
    public function product_detail($slug, Request $request)
    {
        $product = Product::with('product_images')->where('slug', $slug)->where('status', 1)->firstOrFail();
        $more_products = Product::where('slug', '!=', $product->slug)->where('status', 1)->latest()->get();
        // dd($request->category);

        $products_from_similar_categories = Product::where('product_category_id', $product->product_category_id)
            ->where('slug', '<>', $product->slug)
            ->where('status', 1)->get();
        // dd($products_from_similar_categories);
        $totalProductReview = ProductRating::where('product_ratings.product_id', $product->id)->where('status', 1)->count();

        if ($totalProductReview > 0) {

            $totalFiveRating = ProductRating::where('product_ratings.product_id', $product->id)->where('rating', 5)->where('status', 1)->count();
            // dd($totalFiveRating);
            $fivePercent = round(($totalFiveRating * 100) / ($totalProductReview), 1);
            $totalFourRating = ProductRating::where('product_ratings.product_id', $product->id)->where('rating', 4)->where('status', 1)->count();
            $fourPercent = round(($totalFourRating * 100) / ($totalProductReview), 1);
            $totalThreeRating = ProductRating::where('product_ratings.product_id', $product->id)->where('rating', 3)->where('status', 1)->count();
            $threePercent = round(($totalThreeRating * 100) / ($totalProductReview), 1);
            $totalTwoRating = ProductRating::where('product_ratings.product_id', $product->id)->where('rating', 2)->where('status', 1)->count();
            $twoPercent = round(($totalTwoRating * 100) / ($totalProductReview), 1);
            $totalOneRating = ProductRating::where('product_ratings.product_id', $product->id)->where('rating', 1)->where('status', 1)->count();
            $onePercent = round(($totalOneRating * 100) / ($totalProductReview), 1);
        } else {
            $fivePercent = 0;
            $fourPercent = 0;
            $threePercent = 0;
            $twoPercent = 0;
            $onePercent = 0;
        }
        $product_ratings = $product->product_ratings()->where('status', 1)->paginate(4);
        if ($request->ajax()) {
            $view = view('frontend.loadComment', compact('product', 'fivePercent', 'onePercent', 'twoPercent', 'fourPercent', 'threePercent', 'product_ratings', 'totalProductReview', 'more_products', 'products_from_similar_categories'))->render();
            return response()->json(['html' => $view]);
        }

        return view('frontend.product_detail', compact('product', 'fivePercent', 'onePercent', 'twoPercent', 'fourPercent', 'threePercent', 'product_ratings', 'totalProductReview', 'more_products', 'products_from_similar_categories'));
    }

    public function shop()
    {

        $trending_products = Product::where('status', 1)
            ->where('is_trending', 1)->orderBy('order', 'ASC')->get();

        $best_selling_products = Product::where('status', 1)
            ->where('is_bestselling', 1)->orderBy('order', 'ASC')->take(9)->get();

        $latest_products = Product::where('status', 1)
            ->where('is_latest', 1)->orderBy('order', 'ASC')->get();

        $featured_products = Product::where('status', 1)
            ->where('is_featured', 1)->orderBy('order', 'ASC')->get();
        $sliders = Slider::where('status', 1)->latest()->get();
        return view('frontend.shop', compact('sliders', 'trending_products', 'featured_products', 'latest_products', 'best_selling_products'));
    }
    public function cart()
    {
        $cart_items = Cart::where('user_id', auth()->user()->id)->latest()->get();
        return view('frontend.cart', compact('cart_items'));
    }
    public function checkout()
    {
        $provinces = Province::where('status', 1)->get();
        $old_cart_items = Cart::where('user_id', auth()->user()->id)->latest()->get();
        foreach ($old_cart_items as $old_cart_item) {
            if (!Product::where('id', $old_cart_item->product_id)->where('qty', '>=', $old_cart_item->qty)->exists()) {
                $removeItem = Cart::where('user_id', Auth::id())->where('product_id', $old_cart_item->product_id)->first();
                $removeItem->delete();
            }

        }
        $cart_items = Cart::where('user_id', auth()->user()->id)->latest()->get();
        return view('frontend.checkout', compact('cart_items', 'provinces'));
    }
    public function login()
    {
        $company_data = CompanyData::first();
        return view('frontend.login', compact('company_data'));
    }
    public function forget_password()
    {
        return view('frontend.forget_password');
    }
    public function register()
    {
        return view('frontend.register');
    }
    public function user_profile()
    {
        $orders = Order::where('user_id', Auth::id())->with('order_items')->get();

        $user = Auth::user();
        return view('frontend.user_profile', compact('user', 'orders'));
    }
    public function view_order_detail($id)
    {
        $order_items = OrderItem::where('order_id', $id)->get();
        return view('frontend.view_order_detail', compact('order_items'));
    }
    public function about()
    {
        $about = About::where('status', 1)->first();
        return view('frontend.about', compact('about'));
    }

    public function contact()
    {
        $company_data = CompanyData::first();
        return view('frontend.contact', compact('company_data'));
    }
    public function privacy_policy()
    {
        $privacy_policy = PrivacyPolicy::where('status', 1)->first();
        return view('frontend.privacy_policy', compact('privacy_policy'));
    }
    public function service_detail($slug)
    {
        $service = Service::where('slug', $slug)->firstOrFail();
        return view('frontend.service_detail', compact('service'));
    }
    public function storeContact(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            "name" => ["required"],
            "email" => ["required", "email:dns"],
            "phone" => ['required'],
            "message" => ['required'],
            'g-recaptcha-response' => ['required'],
        ], [
            "name.required" => "Please enter your name",
            "email.required" => "Please enter your email",
            "phone.required" => "Please enter your phone",
            "message.required" => "Please enter your query",
            "g-recaptcha-response.required" => "Please check the recaptcha",
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                Contact::create($input);
                DB::commit();
                return response()->json(["msg" => "Your message has been submitted successfully"]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(["db_error" => $e->getMessage()]);
            }

        }

    }
    public function storeProductRating(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "product_id" => ["required"],
            "name" => ["required"],
            "email" => ["required", "email:dns"],
            "review" => ['required'],
            "rating" => ['required'],
        ],
            ["product_id.required" => "Product is required",
                "name.required" => "Please enter your name",
                "email.required" => "Please enter your email",
                "rating.required" => "Please add rating",
                "review.required" => "Please enter your review",
            ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()]);
        }
        if ($validator->passes()) {
            try {
                DB::beginTransaction();
                $input = $request->except("_token");
                ProductRating::create($input);
                DB::commit();
                return response()->json(["msg" => "Your review has been submitted successfully"]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(["db_error" => $e->getMessage()]);
            }

        }

    }

    public function addItemsToCart(Request $request)
    {
        $product_id = $request->input('product_id');
        // dd($product_id);

        // if (!Auth::check()) {
        //     return response()->json(['loginRoute' => route('frontend.login')]);
        // }
        // dd($request->all());
        if (Auth::check()) {
            $prod_check = Product::where('id', $product_id)->first();

            if ($prod_check) {
                if (Cart::where('product_id', $product_id)->where('user_id', Auth::id())->exists()) {
                    $cart_this = Cart::where('product_id', $product_id)->where('user_id', Auth::user()->id)->first();
                    $cart_this->update(['qty' => $cart_this->qty + $request->qty]);
                    return response()->json(["alertmsg" => "Product added to cart successfully."]);

                } else {
                    $validator = Validator::make($request->all(), [
                        "product_id" => ["required"],
                        "user_id" => ["nullable"],
                        "price" => ['required'],
                        "qty" => ['required'],
                    ]);
                    if ($validator->fails()) {
                        return response()->json(["errors" => $validator->errors()]);
                    }
                    if ($validator->passes()) {
                        try {
                            DB::beginTransaction();
                            $input = $request->except("_token");
                            Cart::create($input);
                            $cart_count = Cart::where('user_id', Auth::user()->id)->count();
                            DB::commit();
                            return response()->json(["msg" => "Product added to cart successfully", "cart_count" => $cart_count]);
                        } catch (\Exception $e) {
                            DB::rollback();
                            return response()->json(["db_error" => $e->getMessage()]);
                        }

                    }

                }

            }

        }
        // else {
        //     return response()->json(["alertmsg" => "Login to continue"]);

        // }

    }
    public function updateCartItems(Request $request)
    {
        $prod_id = $request->input('prod_id');
        $prod_qty = $request->input('prod_qty');
        if (Auth::check()) {
            if (Cart::where('product_id', $prod_id)->where('user_id', Auth::id())->exists()) {
                $cart = Cart::where('product_id', $prod_id)->where('user_id', Auth::id())->first();
                $cart->qty = $prod_qty;
                $cart->update();
                return response()->json(["msg" => "Cart updated successfully"]);

            }

        }

    }

    public function deleteCartItems(Request $request)
    {
        if (Auth::check()) {
            $prod_id = $request->input('prod_id');
            if (Cart::where('product_id', $prod_id)->where('user_id', Auth::id())->exists()) {
                $cartItem = Cart::where('product_id', $prod_id)->where('user_id', Auth::id())->first();
                $cartItem->delete();
                return response()->json(["alertmsg" => "Product deleted successfully"]);

            }
        } else {
            return response()->json(["alertmsg" => "Login to continue"]);

        }
    }

    public function search(Request $request)
    {
        $searchResults = (new Search())
            ->registerModel(Product::class, 'name', 'description', 'price', 'image')

            ->perform($request->input('query'));

        return view('frontend.search', compact('searchResults'));
    }
    public function changeUserEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email:dns',
        ]);
        User::find(getUser()->id)->update(['email' => $request->email]);
        $notification = array(
            'alert-type' => 'success',
            'message' => 'Email changed successfully.',
        );
        return redirect()->back()->with($notification);
    }
    public function userNewPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new MatchPassword()],
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@&$#%(){}^*+-]).*$/',
        ], [
            'password.regex' => 'Password must contain at least one uppercase , lowercase, digit and special character',
            'password.required' => 'Password is required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($validator->passes()) {
            try {
                User::find(getUser()->id)->update(['password' => Hash::make($request->password)]);
                Auth::logout();
                Session::flush();
                return redirect()->route('frontend.login')->with(notify('success', 'Password updated successfully'));
            } catch (\Exception $e) {
                return redirect()->back()->with(notify('warning', $e->getMessage()));
            }
        }

    }
    public function changeUserProfile(Request $request)
    {
        $this->validate($request, [
            'phone' => "nullable|numeric|digits_between:10,13",

        ], [
            'phone.min' => 'Phone must have at least 10 digits',
            'phone.numeric' => "Phone must contain only numeric value",
            'phone.digits_between' => "The Phone length must be 10 to 13",
        ]);
        $input = $request->except('_token');

        getUser()->update($input);

        $notification = array(
            'alert-type' => 'success',
            'message' => 'Profile changed successfully.',
        );
        return redirect()->back()->with($notification);
    }
    public function updateUserForgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|confirmed|min:10|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
        ], [
            'email.required' => 'Email is required',
            'email.email' => 'Please enter valid email address.',
            'password.required' => 'Password field is required',
            'password.confirmed' => 'Password confirmation does not match.',
            'password.min' => 'Password must be of at least 10 characters.',
            'password.regex' => 'Password must contain at least one uppercase , lowercase, digit and special character',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($validator->passes()) {
            $user = User::where('email', $request->email)->first();
            if ($user) {
                $input = $request->except(['_token', 'email']);
                $input['password'] = Hash::make($request->password);
                $user->update($input);
                $notification = array(
                    'alert-type' => 'success',
                    'message' => 'Your password updated successfully',
                );
                Auth::login($user);
                return redirect()->route('frontend.login')->with($notification);
            } else {
                Session::flash('msg', 'Email doesn\'t exists in our records');
                return redirect()->back();
            }
        }
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
    public function facebookSignin(Request $request)
    {
        try {

            $user = Socialite::driver('facebook')->user();

            $user = User::where('email', $user->getEmail())->first();
            // $facebookId = User::where('facebook_id', $user->id)->first();

            if ($user) {
                Auth::login($user);
                return redirect('/');
            } else {
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'facebook_id' => $user->id,
                    'password' => 123456,
                ]);

                Auth::login($createUser);
                return redirect('/');
            }

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function googleSignin(Request $request)
    {
        try {
            $user = Socialite::driver('google')->user();
            // $googleId = User::where('google_id', $user->id)->first();
            $user = User::where('email', $user->getEmail())->first();

            if ($user) {
                Auth::login($user);
                return redirect('/');
            } else {
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'password' => 123456,
                ]);

                Auth::login($createUser);
                return redirect('/');
            }

        } catch (\Exception $exception) {
            dd($exception->getMessage());

        }
    }

    public function storeVisit(Request $request)
    {
        Visit::new_visit($request);
        return response()->json(['message' => 'Visit created']);
    }

    public function get_product_details(Request $request)
    {
        $product = Product::find($request->product_id);
        return view('frontend.partial.add_to_cart', compact('product'));
    }
}
