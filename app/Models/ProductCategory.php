<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'slug', 'status', 'order'];

    public function product_types()
    {
        return $this->belongsToMany("App\Models\ProductType", "category_types");
    }
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'product_category_id', 'id');
    }

    public function product_ratings()
    {
        return $this->hasMany('App\Models\ProjectRating', 'product_category_id', 'id');
    }
}
