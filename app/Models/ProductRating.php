<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{
    use HasFactory;
    protected $fillable = ['product_id', 'product_category_id', 'name', 'email', 'review', 'rating', 'status'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'product_category_id', 'id');

    }

}
