<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;
    
    protected $fillable = ['title', 'slug', 'status', 'order'];

    public function cities()
    {
        return $this->hasMany('App\Models\City', 'province_id', 'id');
    }

}
