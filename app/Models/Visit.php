<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    use HasFactory;
    protected $fillable = ['fingerprint', 'useragent', 'browser', 'br_ver', 'os', 'os_ver', 'visit_date', 'visit_time', 'timezone'];

    public static function new_visit($request)
    {
        $date = date('Y-m-d');
        $time = date('h:i:s');
        self::firstOrCreate([
            'fingerprint' => $request->fingerprint,
            'visit_date' => $date,
        ], [
            'fingerprint' => $request->fingerprint,
            'useragent' => $request->useragent,
            'browser' => $request->browser,
            'br_ver' => $request->br_ver,
            'os' => $request->os,
            'os_ver' => $request->os_ver,
            'timezone' => $request->timezone,
            'visit_date' => $date,
            'visit_time' => $time,
        ]);
    }
}
