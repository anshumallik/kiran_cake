<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
     protected $fillable = ["logo", "url", "status",'order'];


   public function getImg($img)
    {
        if ($img != null && $img != "default.png") {
            return asset("images/brand/" . $img);
        }
        return asset("images/default.png");
    }
}
