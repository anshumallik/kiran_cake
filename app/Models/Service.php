<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    protected $fillable = ["title", "image", "slug", "status", "description", "order"];

    public function getImg($img)
    {
        if ($img != null && $img != "default.png") {
            return asset("images/service/" . $img);
        }
        return asset("images/default.png");
    }
}
