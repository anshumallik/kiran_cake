<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;
    protected $fillable = ['caption', 'link', 'image', 'status'];

    public function getImg($img)
    {
        if ($img != null && $img != "default.png") {
            return asset("images/slider/" . $img);
        }
        return asset("images/default.png");
    }
}
