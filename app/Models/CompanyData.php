<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyData extends Model
{
    use HasFactory;
    protected $fillable = ['company_name', 'company_phone', 'company_phone1',
        'company_address', 'company_email', 'company_logo', 'company_slogan','company_banner'];

    public function companyImage($img)
    {
        if ($img != null) {
            return asset('images/general/' . $img);
        } else {
            return asset('images/default.png');
        }
    }
    public function companyBanner($img)
    {
        if ($img != null) {
            return asset('images/general/banner/' . $img);
        } else {
            return asset('images/default.png');
        }
    }

}
