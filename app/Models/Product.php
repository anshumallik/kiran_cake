<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Product extends Model implements Searchable
{
    use HasFactory;

    protected $fillable = ['name', 'status', 'order', 'image', 'price',
        'product_category_id', 'product_type_id', 'unit_id', 'code', 'slug', 'qty',
        'discount', 'product_selling_type', 'is_bestselling', 'is_latest', 'is_trending', 'is_featured',
        'description', 'short_description'];

    public function order_items()
    {
        return $this->hasMany('App\Models\OrderItem', 'product_id', 'id');
    }
    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }
    public function product_images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory', 'product_category_id', 'id');
    }
    public function cart()
    {
        return $this->belongsTo('App\Models\Cart', 'product_id', 'id');
    }

    public function producttype()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id', 'id');
    }
    public function product_ratings()
    {
        return $this->hasMany('App\Models\ProductRating', 'product_id', 'id');
    }

    public function avgRating()
    {
        return $this->product_ratings->where('status', 1)->avg('rating');
    }
    public function getImg($img)
    {
        if ($img != null && $img != "default.png") {
            return asset("images/product/" . $img);
        }
        return asset("images/default.png");
    }
    public function getSearchResult(): SearchResult
    {
        $url = route('frontend.product_detail', ['slug' => $this->slug]);

        return new SearchResult(
            $this,
            $this->name,
            $url
        );
    }

}
