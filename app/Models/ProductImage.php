<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'product_id', 'image','status',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    public function getImg($img)
    {
        if ($img != null && $img != "default.png") {
            return asset("images/productImage/" . $img);
        }
        return asset("images/default.png");
    }

}
